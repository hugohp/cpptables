// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "../include/cpptables/table.hh"
#include "../include/cpptables/splay_table.hh"
#include "../include/cpptables/types.hh"

using namespace tables;
using namespace std;

struct year_t { static constexpr std::string_view name = "year"; };
struct month_t { static constexpr std::string_view name = "month"; };
struct v0_t { static constexpr std::string_view name = "v0"; };
struct v1_t { static constexpr std::string_view name = "v1"; };

TEST_CASE( "splay_table", "[test0]" ) {

  SECTION( "upsert" ) {

    using my_table_t = table<
      std::pair<year_t,unsigned int>,
      std::pair<month_t,unsigned int>,
      std::pair<v0_t,double>,
      std::pair<v1_t,unsigned int>
    >;

    const my_table_t t(
      {2020,2020,2021,2021},
      {1,2,1,2},
      {1.0,2.0,3.0,4.0},
      {100,200,300,400}
    );

    const std::filesystem::path path = 
      std::filesystem::temp_directory_path().append("splay");

    using splay_table_t = 
      splay_table<my_table_t,std::tuple<year_t,month_t>>;

    const splay_table_t splay(path);
    splay.update(t);
    const my_table_t t1 = splay.get<v0_t,v1_t>( splay.td() );
    REQUIRE( t1.eq(t).all().all() );

    splay.upsert(t);
    const my_table_t t2 = splay.get<v0_t,v1_t>( splay.td() );
    const my_table_t expected_t2 = t.uj(t).sort_by<year_t,month_t>();
    REQUIRE( t2.eq(expected_t2).all().all() );
  }

  /*
  SECTION( "speed_test" ) {
	 
    using my_table_t = table<
      std::pair<year_t,unsigned long>,
      std::pair<v0_t,unsigned long>,
      std::pair<v1_t,unsigned long>
    >;

    const std::filesystem::path path = 
      std::filesystem::temp_directory_path().append("splay_speed_test");

    using splay_table_t = 
      splay_table<my_table_t,std::tuple<year_t>>;

    const splay_table_t splay(path);

    constexpr std::size_t n = 1000000;
    std::vector<unsigned long> year(n);
    std::vector<unsigned long> v0s(n);
    std::vector<unsigned long> v1s(n);
    for ( unsigned int run = 0 ; run < 100 ; ++run )
    {
      std::cout << "run " << run << "\n";
      std::fill(year.begin(),year.end(),run % 10);
      std::iota(v0s.begin(),v0s.end(),run*n);
      std::iota(v1s.begin(),v1s.end(),run*n);
      column<unsigned long> cyear(year);
      column<unsigned long> c0(v0s);
      column<unsigned long> c1(v1s);
      const my_table_t t(std::move(cyear),std::move(c0),std::move(c1));
      splay.upsert(t);
    }
  }
  */
}
