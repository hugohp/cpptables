// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"

using namespace tables;
using namespace std;

struct c0{};
struct c1{};
struct c2{};

TEST_CASE( "fby", "[group]" ) {

  SECTION( "fby" ) {

    using my_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const my_table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );
   
    // n_groups();
    using ftable_t = my_table_t::fby_t<std::tuple<c0>>;

    const ftable_t ft = t.fby<c0>();
    REQUIRE ( ft.n_groups() == 3);

    // t();
    const my_table_t t1 = ft.t();
    REQUIRE ( t1.eq(t).all().all() );

    // m()
    const std::map<ftable_t::g_row_t,vixs_t> gixs = ft.m();
    using g_row_t = typename ftable_t::g_row_t;

    // grouped by abc
    auto it = gixs.find(g_row_t(std::string_view("abc")));
    REQUIRE( it != gixs.end() );
    vixs_t ixs = it->second;
    
    const my_table_t expected_t_abc(
      {"abc","abc"},
      {10.0, 40.0},
      {100, 400}
    );
    REQUIRE( t.at(ixs).eq(expected_t_abc).all().all() );

    // grouped by def
    it = gixs.find(g_row_t(std::string_view("def")));
    REQUIRE( it != gixs.end() );
    ixs = it->second;
    
    const my_table_t expected_t_def(
      {"def","def"},
      {20.0, 50.0},
      {200, 500}
    );
    REQUIRE( t.at(ixs).eq(expected_t_def).all().all() );
 
    // grouped by ghi
    it = gixs.find(g_row_t(std::string_view("ghi")));
    REQUIRE( it != gixs.end() );
    ixs = it->second;
    
    const my_table_t expected_t_ghi(
      {"ghi","ghi"},
      {30.0, 60.0},
      {300, 600}
    );
    REQUIRE( t.at(ixs).eq(expected_t_ghi).all().all() );
 
  }

  SECTION( "agg" ) {

    using my_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned int>
    >;

    const my_table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );
   
    using r_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,double>
    >;

    auto favg = [] (const column<double>& xs) { return xs.avg(); };
    auto fnorm = [] (const column<unsigned>& xs) { return xs.norm(); };
    const r_table_t res = t.fby<c0>().agg<c1,c2>(favg,fnorm);

    const r_table_t expected_res(
      {"abc","def","ghi"},
      {25.0, 35.0, 45.0},
      {412.311,538.516, 670.82}
    );

    REQUIRE ( res.col<c0>().eq(expected_res.col<c0>()).all() );
    REQUIRE ( (res.col<c1>() - expected_res.col<c1>()).norm() <= 1e-10 );
    REQUIRE ( (res.col<c2>() - expected_res.col<c2>()).norm() <= 0.01 );
  }

  SECTION( "fupdate (one transf)" ) {

    using my_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const my_table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );
   
    auto freverse = [] (const column<unsigned>& xs) { return xs.reverse(); };
    const my_table_t res = t.fby<c0>().fupdate<c2>(freverse);

    const my_table_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {400, 500, 600, 100, 200, 300}
    );

    REQUIRE ( res.eq(expected_res).all().all() );
  }

  SECTION( "fupdate (two trasnf)" ) {

    using my_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const my_table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );
   
    using r_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    auto fcast = [] (const column<double>& xs) { return xs.cast_to<unsigned>(); };
    auto freverse = [] (const column<unsigned>& xs) { return xs.reverse(); };
    const r_table_t res = t.fby<c0>().fupdate<c1,c2>(fcast,freverse);

    const r_table_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {10, 20, 30, 40, 50, 60},
      {400, 500, 600, 100, 200, 300}
    );

    REQUIRE ( res.eq(expected_res).all().all() );
  }

}
