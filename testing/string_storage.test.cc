// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include "common.test.hh"
#include "../include/cpptables/strings/string_storage.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

TEST_CASE( "string_storage", "[test0]" ) {

  SECTION( "test0 " ) {

    const std::vector<std::string_view> svs = { "foo", "bar", "abc", "def" };

    const std::vector<std::string_view> rsvs = impl::string_storage::instance().add_strings(svs);
    const std::vector<std::string_view> rsvs1 = impl::string_storage::instance().add_strings(svs);

    REQUIRE( svs == rsvs );
    REQUIRE( svs == rsvs1 );
  }
}
