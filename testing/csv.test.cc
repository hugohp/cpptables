// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/csv.hh"

using namespace tables;

struct c0_t { constexpr static char const * const name = "c0"; };
struct c1_t { constexpr static char const * const name = "c1"; };
struct c2_t { constexpr static char const * const name = "c2"; };
struct c3_t { constexpr static char const * const name = "c3"; };

struct f0 
{
   bool operator()(const std::string_view sv) const
   {
     return ( sv == "foo" ) ? true : false;
   } 
};


TEST_CASE( "from_csv", "[from_csv]" ) {

  SECTION( "simple_test" ) {

    using table_t = table<
      std::pair<c0_t,std::string_view>,
      std::pair<c1_t,double>,
      std::pair<c2_t,unsigned int>,
      std::pair<c3_t,bool>
    >;

    using fs_t = tmap<std::pair<c3_t,f0>>;
 
    const table_t t = from_csv<table_t,fs_t>::run("/home/john/src/cpptables/testing/data/data.csv");

    const table_t expected_t (
      {"secid0", "secid1","secid2"},
      {10.,11.,12.},
      {999,888,777},
      {true, false, true}
    );
    REQUIRE ( t.eq(expected_t).all().all() );
  }
}
