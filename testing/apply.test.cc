// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/where.hh"

using namespace tables;

const auto dbl = [] ( const double x ) { return 2.0*x; };
const auto triple = [] ( const unsigned x ) { return 3*x; };

TEST_CASE( "apply", "[test0]" ) {

  SECTION( "t.apply" ) {

    const px_vol_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500 , 600 }
    );

   const px_vol_t res = t.apply<price_t,volume_t>( dbl, triple );

   const px_vol_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {20.0, 40.0, 60.0, 80.0, 100.0, 120.0},
      {300, 600, 900, 1200, 1500 , 1800 }
   );
  }

  SECTION( "t.apply_at" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   const auto dbl = [] ( const double x ) { return 2.0*x; };
   const px_t res = t.apply_at<price_t>(
     where( t.col<security_id_t>() == std::string_view("abc" ) ),
     dbl
   );

   const px_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {20.0, 20.0, 30.0, 80.0, 50.0, 60.0}
   );

   REQUIRE( res.eq(expected_res).all().all() );
  }
}
