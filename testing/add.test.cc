// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

TEST_CASE( "add", "[test0]" ) {

  SECTION( "add column" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   const column<unsigned int> vs ( { 1,2,3,4,5,6 } );
   const auto t0 = t.add<volume_t>(vs);

   const px_vol_t expected_t0(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {1, 2, 3, 4, 5, 6}
    );

   REQUIRE( t0.eq(expected_t0).all().all() );
  }

  SECTION( "add table" ) {

    const table<
      std::pair<price_t,double>
    > tp( {10.0, 20.0, 30.0, 40.0, 50.0, 60.0});

    const table<
      std::pair<volume_t,unsigned>
    > tv( {1, 2, 3, 4, 5, 6});

   using table_t = 
    const table<
      std::pair<price_t,double>,
      std::pair<volume_t,unsigned>
    >;

   const table_t res = tp.add(tv);

   const table_t expected_res(
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {1, 2, 3, 4, 5, 6}
   );

   REQUIRE( res.eq(expected_res).all().all() );
  }

  SECTION( "addu column" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   const column<double> pxs(
      {100.0, 200.0, 300.0, 400.0, 500.0, 600.0}
    );

   const px_t res = t.addu<price_t>(pxs);

   const px_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {100.0, 200.0, 300.0, 400.0, 500.0, 600.0}
    );

   REQUIRE( res.eq(expected_res).all().all() );
  }

  SECTION( "addu table" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

    const table<std::pair<price_t,double>> t1(
      {100.0, 200.0, 300.0, 400.0, 500.0, 600.0}
    );

    const px_t res = t.addu(t1);

    const px_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {100.0, 200.0, 300.0, 400.0, 500.0, 600.0}
    );

   REQUIRE( res.eq(expected_res).all().all() );
  }
}
