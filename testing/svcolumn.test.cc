// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

TEST_CASE( "svcolumns", "[test0]" ) {

  SECTION( "apply" ) {

    const column<std::string_view> c ( { "foo", "bar" } );
    const auto f = [] ( const std::string_view sv ) { return (sv == "foo") ? 1U : 0U; };

    const column<unsigned> rs = c.apply(f);
    const column<unsigned> expected_rs ({ 1,0});

    REQUIRE( rs.eq(expected_rs).all() );
  }

  SECTION( "reverse" ) {

    const column<std::string_view> c ( { "foo", "bar" } );
    const column<std::string_view> rc = c.reverse();

    const column<std::string_view> expected_rc ( { "bar", "foo" } );

    REQUIRE( rc.eq(expected_rc).all() );
  }

  SECTION( "at/where" ) {

    const column<std::string_view> c ( { "foo", "bar", "zoo" } );

    const column<bool> bc ( { true, true, false } );
    const column<std::string_view> res = c.at( bc.where() );
    const column<std::string_view> expected_res ( { "foo", "bar" } );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "all/any" ) {
  
    const column<std::string_view> c0 ( { "foo", "bar", "zoo" } );
    const column<std::string_view> c1 ( { "foo", "", "zoo" } );

    REQUIRE( c0.all() );
    REQUIRE( !c1.all() );

    REQUIRE( c0.any() );
    REQUIRE( c1.any() );
  }

  SECTION( "operator<" ) {
  
    const column<std::string_view> c0 ( { "abc", "def", "ghi" } );
    const column<std::string_view> c1 ( { "abc", "zoo", "def" } );

    const column<bool> res = c0 < c1;
    const column<bool> expected_res( { false, true, false} );
 
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator<=" ) {
  
    const column<std::string_view> c0 ( { "abc", "def", "ghi" } );
    const column<std::string_view> c1 ( { "abc", "zoo", "def" } );

    const column<bool> res = c0 <= c1;
    const column<bool> expected_res( { true, true, false} );
 
    REQUIRE( res.eq(expected_res).all() );
 
  }

  SECTION( "operator>=" ) {
  
    const column<std::string_view> c0 ( { "abc", "def", "ghi" } );
    const column<std::string_view> c1 ( { "abc", "zoo", "def" } );

    const column<bool> res = c0 >= c1;
    const column<bool> expected_res( { true, false, true} );
 
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator>" ) {
  
    const column<std::string_view> c0 ( { "abc", "def", "ghi" } );
    const column<std::string_view> c1 ( { "abc", "zoo", "def" } );

    const column<bool> res = c0 > c1;
    const column<bool> expected_res( { false, false, true} );
 
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "distinct" ) {

    const column<std::string_view> c0 ( { "abc", "def", "abc" } );

    const column<std::string_view> res = c0.distinct();
    const column<std::string_view> expected_res( { "abc", "def" } );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "differ" ) {

    const column<std::string_view> c0 ( { "abc", "abc", "def", "def" } );

    const column<bool> res = c0.differ();
    const column<bool> expected_res ({ true, false, true, false });
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "except" ) {

    const column<std::string_view> c0 ( { "abc", "abc", "def", "def" } );
    const column<std::string_view> c1 ( { "abc" } );

    const column<std::string_view> res = c0.except(c1);
    const column<std::string_view> expected_res ({ "def", "def" });
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "sort/rsort" ) {

    const column<std::string_view> c0 ( { "abc", "zoo", "ghi", "def" } );

    const column<std::string_view> s0 = c0.sort();
    const column<std::string_view> expected_s ( { "abc", "def", "ghi", "zoo" } );
    REQUIRE( s0.eq(expected_s).all() );

    const column<std::string_view> r0 = c0.rsort();
    const column<std::string_view> expected_rs ( { "zoo", "ghi", "def", "abc" } );
    REQUIRE( r0.eq(expected_rs).all() );

  }

  SECTION( "mins" ) {

    const column<std::string_view> c0 ( { "zoo", "def", "abc", "def" } );

    const column<std::string_view> res = c0.mins();
    const column<std::string_view> expected_res ( { "zoo", "def", "abc", "abc" } );
    REQUIRE( res.eq(expected_res).all() );
  }
 
  SECTION( "maxs" ) {

    const column<std::string_view> c0 ( { "abc", "def", "zoo", "def" } );

    const column<std::string_view> res = c0.maxs();
    const column<std::string_view> expected_res ( { "abc", "def", "zoo", "zoo" } );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "mmin" ) {

    const column<std::string_view> c0 ( { "abc", "def", "zoo", "def" } );
    const column<std::string_view> res = c0.mmin(2);

    const column<std::string_view> expected_res ( { "abc", "abc", "def", "def" } );

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "prev/next" ) {

    using col_t = column<std::string_view>;

    const col_t col ( { "abc", "def", "ghi" });
    const col_t pcol = col.prev();
    const col_t ncol = col.next();

    const col_t expected_pcol ( { "", "abc", "def" });
    REQUIRE( pcol.eq(expected_pcol).all() );

    const col_t expected_ncol ( { "def", "ghi", "" });
    REQUIRE( ncol.eq(expected_ncol).all() );
  }
 
  SECTION( "mmax" ) {
    const column<std::string_view> c0 ( { "abc", "def", "zoo", "def" } );
    const column<std::string_view> res = c0.mmax(2);

    const column<std::string_view> expected_res ( { "abc", "def", "zoo", "zoo" } );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "is_none" ) {
  
    const column<std::string_view> c ( { "abc", "", "zoo", "def" } );
    const column<bool> bc = c.is_none();
    const column<bool> expected_bc({false,true,false,false});

    REQUIRE( (bc == expected_bc).all() );
  }

  SECTION( "cross" ) {

    const column<std::string_view> c0 ( { "foo", "bar" } );
    const column<unsigned> c1 ( { 1, 2});

    const std::pair<column<std::string_view>,column<unsigned>> res = c0.cross(c1);
    const std::pair<column<std::string_view>,column<unsigned>> expected_res = 
    {
        column<std::string_view>({"foo","foo","bar","bar"}),
        column<unsigned>({1,2,1,2})
    };

    REQUIRE( (res.first == expected_res.first).all() );
    REQUIRE( (res.second == expected_res.second).all() );
  }

  SECTION( "starts_with" ) {

    const column<std::string_view> col ( { "foo_abc", "foo_def", "bar_ghi" } );
    const column<std::string_view> prefix ( { "foo", "foo", "foo" } );

    const column<bool> r0 = col.starts_with(prefix);
    const column<bool> expected_r0({true,true,false});

    REQUIRE( r0.eq(expected_r0).all() );

    const column<bool> r1 = col.starts_with("bar");
    const column<bool> expected_r1({false,false,true});
    REQUIRE( r1.eq(expected_r1).all() );
  }

  SECTION( "ends_with" ) {

    const column<std::string_view> col ( { "abc_foo", "def_foo", "ghi_bar" } );
    const column<std::string_view> prefix ( { "foo", "foo", "foo" } );

    const column<bool> r0 = col.ends_with(prefix);
    const column<bool> expected_r0({true,true,false});

    REQUIRE( r0.eq(expected_r0).all() );

    const column<bool> r1 = col.ends_with("bar");
    const column<bool> expected_r1({false,false,true});
    REQUIRE( r1.eq(expected_r1).all() );
  }

  SECTION( "substr" ) {

    const column<std::string_view> col ( { "abcdef", "ghijkl", "mnopqr" } );
    const column<std::size_t> pos ( { 0, 1, 2} );
    const column<std::size_t> count ( { 1,2,3} );

    const column<std::string_view> r0 = col.substr(pos,count);
    const column<std::string_view> expected_r0( { "a", "hi", "opq" } );

    REQUIRE( r0.eq(expected_r0).all() );

    const column<std::string_view> r1 = col.substr(1,3);
    const column<std::string_view> expected_r1( { "bcd", "hij", "nop" } );

    REQUIRE( r1.eq(expected_r1).all() );
  }

  SECTION( "concat" ) {

    const column<std::string_view> c0 ( { "abc", "def", "ghi" } );
    const column<std::string_view> c1( { "foo", "bar", "zoo" } );

    const column<std::string_view> r0 = c0.concat(c1);
    const column<std::string_view> expected_r0( { "abcfoo", "defbar", "ghizoo" } );

    REQUIRE( r0.eq(expected_r0).all() );

    const column<std::string_view> r1 = c0.concat("foo");
    const column<std::string_view> expected_r1( { "abcfoo", "deffoo", "ghifoo" } );

    REQUIRE( r1.eq(expected_r1).all() );
  }

  SECTION( "to_string" ) {
    const column<std::string_view> col ( { "abc", "def", "ghi" } );
    const std::string r0 = col.to_string();
    const std::string_view expected_r0 = "abcdefghi";

    REQUIRE( r0 == expected_r0 );

    const std::string r1 = col.to_string(",");
    const std::string_view expected_r1 = "abc,def,ghi";
    REQUIRE( r1 == expected_r1 );
  }
}
