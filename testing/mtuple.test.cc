// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

struct c0 {};
struct c1 {};
struct c2 {};
struct c3 {};

TEST_CASE( "mtuple", "[test0]" ) {

  SECTION( "cast_to" ) {

    using mtuple0_t = const mtuple<
      std::pair<c0,unsigned>,
      std::pair<c1,double>
    >;

    using mtuple1_t = const mtuple<
      std::pair<c2,std::string_view>,
      std::pair<c3,double>
    >;

    const mtuple0_t m0(1,2.0);
    const mtuple1_t m1("abc",10.0);

    using r_mtuple_t = const mtuple<
      std::pair<c0,unsigned>,
      std::pair<c1,double>,
      std::pair<c2,std::string_view>,
      std::pair<c3,double>
    >;

    const r_mtuple_t rm = m0.add(m1);
    const r_mtuple_t expected_rm = r_mtuple_t(1,2.0,"abc",10.0);

    REQUIRE( rm == expected_rm );
  }
}

