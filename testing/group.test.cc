// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/gtable.hh"
#include "../include/cpptables/types.hh"

using namespace tables;
using namespace std;

struct c0 {};
struct c1 {};
struct c2 {};

TEST_CASE( "group", "[test0]" ) {

  SECTION( "group_by" )  {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );
  
    using gtable_t = px_t::group_by_t<tuple<security_id_t>>;

    const gtable_t gt = t.group_by<security_id_t>();

    const px_t expected_gt(
      {"abc","abc","def", "def","ghi","ghi"},
      {10.0, 40.0, 20.0, 50.0, 30.0, 60.0}
    );

    REQUIRE( gt.ungroup().eq(expected_gt).all().all() );
  }

  SECTION( "max min sum avg" )  {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );
  
    const auto rmax = t.group_by<security_id_t>().max();
    const px_t expected_rmax(
      {"abc","def","ghi"},
      {40.0, 50.0, 60.0}
    );
    REQUIRE( rmax.eq(expected_rmax).all().all() );

    const auto rmin = t.group_by<security_id_t>().min();
    const px_t expected_rmin(
      {"abc","def","ghi"},
      {10.0, 20.0, 30.0}
    );
    REQUIRE( rmin.eq(expected_rmin).all().all());

    const auto rsum = t.group_by<security_id_t>().sum();
    const px_t expected_rsum(
      {"abc","def","ghi"},
      {50.0, 70.0, 90.0}
    );
    REQUIRE( rsum.eq(expected_rsum).all().all());

    const auto ravg = t.group_by<security_id_t>().avg();
    const px_t expected_ravg(
      {"abc","def","ghi"},
      {25.0, 35.0, 45.0}
    );
    REQUIRE( ravg.eq(expected_ravg).all().all());
  }

  SECTION( "abs" )  {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,int>,
      std::pair<c2,double>
    >;

    const table_t t(
      {"abc","def","abc","def"},
      {-10,-20,10,20},
      {-1.1, -2.2, -3.3, -4.4}
    );

    const auto tf = t.group_by<c0>().abs().ungroup();

    const table_t expected_tf(
      {"abc","abc","def","def"},
      {10,10,20,20},
      {1.1, 3.3,2.2, 4.4}
    );
    REQUIRE( tf.eq(expected_tf).all().all());
  }

  SECTION( "abs" )  {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,int>,
      std::pair<c2,double>
    >;

    const table_t t(
      {"abc","def","abc","def"},
      {-10,-20,10,20},
      {-1.1, -2.2, -3.3, -4.4}
    );

    const auto tf = t.group_by<c0>().abs().ungroup();

    const table_t expected_tf(
      {"abc","abc","def","def"},
      {10,10,20,20},
      {1.1, 3.3,2.2, 4.4}
    );
    REQUIRE( tf.eq(expected_tf).all().all());
  }

  SECTION( "log/log2/log10/exp" )  {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,double>
    >;

    const table_t t(
      {"abc","def","abc","def"},
      {1,2,3,4},
      {10.0,20.0,30.0,40.0}
    );

    using r_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,double>
    >;
 
    // log
    const auto gt = t.group_by<c0>();
    const r_table_t t1 = gt.log().ungroup();

    REQUIRE( t1.col<c0>().eq(column<std::string_view>({"abc","abc","def","def"})).all() );
    REQUIRE( ( t1.col<c1>() - gt.ungroup().col<c1>().log()).abs().le(1e-10).all() );

    // log2
    const r_table_t t2 = gt.log2().ungroup();
    REQUIRE( t2.col<c0>().eq(column<std::string_view>({"abc","abc","def","def"})).all() );
    REQUIRE( ( t2.col<c1>() - gt.ungroup().col<c1>().log2()).abs().le(1e-10).all() );

    // log10
    const r_table_t t3 = gt.log10().ungroup();
    REQUIRE( t3.col<c0>().eq(column<std::string_view>({"abc","abc","def","def"})).all() );
    REQUIRE( ( t3.col<c1>() - gt.ungroup().col<c1>().log10()).abs().le(1e-10).all() );

    // exp
    const r_table_t t4 = gt.exp().ungroup();
    REQUIRE( t4.col<c0>().eq(column<std::string_view>({"abc","abc","def","def"})).all() );
    REQUIRE( ( t4.col<c1>() - gt.ungroup().col<c1>().exp()).abs().le(1e-10).all() );
  }
} 
