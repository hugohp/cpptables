// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

constexpr double none_dbl = prim_traits<double>::none;
constexpr int none_int = prim_traits<int>::none;
constexpr int none_uint = prim_traits<unsigned int>::none;

TEST_CASE( "columns", "[test0]" ) {

  SECTION( "cast_to" ) {

    const column<int> cint ( { none_int, -1, 0, 1, 2 } );
    const column<double> res = cint.cast_to<double>();

    const column<double> expected_res( { none_dbl , -1.0 ,0.0, 1.0, 2.0 } );
    
    REQUIRE( (res.zfill() == expected_res.zfill()).all() );
  }

  SECTION( "at/where" ) {

    const column<int> c ( { 10, 20 ,30 ,40 ,50} );
    const column<int> res = c.at( where( c <= 30 ) );
    const column<int> expected_res ({10,20,30});
    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "where(vector-bool)" ) {

    const column<int> c ( { 10, 20 ,30 ,40 ,50} );
    const column<int> res = c.at( where( c <= 30 ) );
    const column<int> expected_res ({10,20,30});
    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "apply" ) {

    const column<double> ds ( { 1.5, 2.5, 3.5 });
    const auto f = [] ( const double v ) { return (int)v; };

    const column<int> is = ds.apply(f);
    const column<int> expected_is ({ 1,2,3});

    REQUIRE( (is == expected_is).all() );
  }

 
  SECTION( "operators+" ) {
  
    using col_t = column<double>;
    const col_t c0  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );
    const col_t c1  ( { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } );

    const col_t res = c0 + c1;
    const col_t expected_res( { none_dbl , 22.0, none_dbl, 44.0, 55.0, none_dbl } );
    
    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "operators+ cte" ) {
  
    using col_t = column<double>;
    const col_t col  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );

    const col_t res0 = col + 10.0;
    const col_t res1 = 10.0 + col;
    const col_t expected_res( { none_dbl , 30.0, none_dbl, 50.0, 60.0, none_dbl } );
    
    REQUIRE( (res0.zfill() == expected_res.zfill() ).all() );
    REQUIRE( (res1.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "operators-" ) {
  
    using col_t = column<double>;
    const col_t c0  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );
    const col_t c1  ( { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } );

    const col_t res = c0 - c1;
    const col_t expected_res( { none_dbl , 18.0, none_dbl, 36.0, 45.0, none_dbl } );
    
    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "operators- cte" ) {
  
    using col_t = column<double>;
    const col_t col  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );

    const col_t res0 = col - 10.0;
    const col_t res1 = 10.0 - col;
    const col_t expected_res0( { none_dbl , 10.0, none_dbl, 30.0, 40.0, none_dbl } );
    const col_t expected_res1( { none_dbl , -10.0, none_dbl, -30.0, -40.0, none_dbl } );
    
    REQUIRE( (res0.zfill() == expected_res0.zfill()).all() );
    REQUIRE( (res1.zfill() == expected_res1.zfill()).all() );
  }

  SECTION( "operators*" ) {
  
    using col_t = column<double>;
    const col_t c0  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );
    const col_t c1  ( { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } );

    const col_t res = c0 * c1;
    const col_t expected_res( { none_dbl , 40.0, none_dbl, 160.0, 250.0, none_dbl } );
    
    REQUIRE( (res.zfill() == expected_res.zfill()).all() );
  }

  SECTION( "operators* cte" ) {
  
    using col_t = column<double>;
    const col_t col  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );

    const col_t res0 = col * 10.0;
    const col_t res1 = 10.0 * col;
    const col_t expected_res( { none_dbl , 200.0, none_dbl, 400.0, 500.0, none_dbl } );
    
    REQUIRE( (res0.zfill() == expected_res.zfill()).all() );
    REQUIRE( (res1.zfill() == expected_res.zfill()).all() );
  }

  SECTION( "operators/" ) {
  
    using col_t = column<double>;
    const col_t c0  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );
    const col_t c1  ( { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } );

    const col_t res = c0 / c1;
    const col_t expected_res( { none_dbl , 10.0, none_dbl, 10.0, 10.0, none_dbl } );
    
    REQUIRE( (res.zfill() == expected_res.zfill()).all() );
  }

  SECTION( "operators/ cte" ) {
  
    using col_t = column<double>;
    const col_t col  ( { none_dbl , 20.0, none_dbl, 40.0, 50.0, none_dbl } );

    const col_t res0 = col / 10.0;
    const col_t res1 = 10.0 / col;
    const col_t expected_res0( { none_dbl , 2.0, none_dbl, 4.0, 5.0, none_dbl } );
    const col_t expected_res1( { none_dbl , 0.5, none_dbl, 10.0/40.0, 10.0/50.0, none_dbl } );
    
    REQUIRE( (res0.zfill() == expected_res0.zfill()).all() );
    REQUIRE( (res1.zfill() == expected_res1.zfill()).all() );
  }

  SECTION( "all/any" ) {
  
    using col_t = column<int>;
    const col_t c0 ( { 1, 0, 1 } );
    const col_t c1  ( { 1, 1, 1 } );
    const col_t c2  ( { 0, 0, 0 } );

    REQUIRE( !c0.all() );
    REQUIRE( c1.all() );

    REQUIRE( c0.any() );
    REQUIRE( c1.any() );
    REQUIRE( ! c2.any() );
  }

  SECTION( "is_none" ) {
  
    const column<int> c ( { 0,1,2, none_int, 4,5,none_int} );
    const column<bool> bc = c.is_none();
    const column<bool> expected_bc({false,false,false,true,false,false,true});

    REQUIRE( (bc == expected_bc).all() );
  }


  SECTION( "operator<" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res0(c0 < c1);
    const column<bool> res1(c0 < 25);
    const column<bool> res2(25 < c0);

    const column<bool> expected_res0 ({false,false,false,true,true});
    const column<bool> expected_res1 ({true,true,false,false,false});
    const column<bool> expected_res2 ({false,false,true,true,true});
    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator<=" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res0(c0 <= c1);
    const column<bool> res1(c0 <= 25);
    const column<bool> res2(25 <= c0);

    const column<bool> expected_res0 ({true,false,false,true,true});
    const column<bool> expected_res1 ({true,true,false,false,false});
    const column<bool> expected_res2 ({false,false,true,true,true});
    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator>=" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res0(c0 >= c1);
    const column<bool> res1(c0 >= 25);
    const column<bool> res2(25 >= c0);

    const column<bool> expected_res0 ({true,true,true,false,false});
    const column<bool> expected_res1 ({false,false,true,true,true});
    const column<bool> expected_res2 ({true,true,false,false,false});
    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator>" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res0(c0 > c1);
    const column<bool> res1(c0 > 25);
    const column<bool> res2(25 > c0);

    const column<bool> expected_res0 ({false,true,true,false,false});
    const column<bool> expected_res1 ({false,false,true,true,true});
    const column<bool> expected_res2 ({true,true,false,false,false});
    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "fills" ) {

    using col_t = column<double>;

    const col_t col  ( { none_dbl , 20.0, none_dbl, 30.0, 40, none_dbl } );
    const col_t res = col.fills();

    const col_t expected_res( {none_dbl, 20.0, 20.0, 30.0, 40.0, 40.0 } );

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "replace" ) {

    using col_t = column<double>;

    const col_t col0  ( { 1.0, 2.0, 3.0, none_dbl});
    const col_t col1  ( { none_dbl , 20.0, 30.0, 40.0});

    const col_t res = col0.replace(col1);
    const col_t expected_res( {1.0, 20.0, 30.0, 40.0});

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "fill_with(scalar)" ) {

    using col_t = column<double>;

    const col_t col ( { none_dbl , 20.0, none_dbl, 30.0, 40, none_dbl } );
    const col_t res = col.fill_with(123.0);

    const col_t expected_res( {123.0, 20.0, 123.0, 30.0, 40.0, 123.0 } );

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "fill_with(column)" ) {

    using col_t = column<double>;

    const col_t col ( { none_dbl , 20.0, none_dbl, 30.0, 40, none_dbl } );
    const col_t colf ( { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } );
    const col_t res = col.fill_with(colf);

    const col_t expected_res( {1.0, 20.0, 3.0,30.0, 40.0, 6.0 } );

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "where" ) {

    const column<int> c ( { 1,2,3,4 } );
    const column<int> res = c.at( where ( c == 1 ) );

    const column<int> expected_res({1});
    REQUIRE( ((res == expected_res) ).all() );
  }

  SECTION( "operator>=" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res(c0 >= c1);

    const column<bool> expected_res ({true,true,true,false,false});
    REQUIRE( ((res == expected_res) ).all() );
  }

  SECTION( "operator>" ) {
  
    constexpr int none_int = prim_traits<int>::none;
    using col_t = column<int>;
    const col_t c0 ( { 10, 20, none_int, 30, 40 } );
    const col_t c1 ( { 10, 19, 10, none_int, 50 } );

    const column<bool> res(c0 > c1);

    const column<bool> expected_res ({false,true,true,false,false});
    REQUIRE( ((res == expected_res) ).all() );
  }

  SECTION( "fills" ) {

    using col_t = column<double>;

    const col_t col  ( { none_dbl , 20.0, none_dbl, 30.0, 40, none_dbl } );
    const col_t res = col.fills();

    const col_t expected_res( {none_dbl, 20.0, 20.0, 30.0, 40.0, 40.0 } );

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "fill_with" ) {

    using col_t = column<double>;

    const col_t col ( { none_dbl , 20.0, none_dbl, 30.0, 40, none_dbl } );
    const col_t res = col.fill_with(123.0);

    const col_t expected_res( {123.0, 20.0, 123.0, 30.0, 40.0, 123.0 } );

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "where" ) {

    const column<int> c ( { 1,2,3,4 } );
    const column<int> res = c.at( where ( c == 1 ) );

    const column<int> expected_res({1});
    REQUIRE( ((res == expected_res) ).all() );
  }

  SECTION( "deltas" ) {

    using col_t = column<double>;

    const col_t col ({10.0, none_dbl, 10.0, 30.0}) ;
    const col_t res = col.deltas();

    const col_t expected_res( {10.0, none_dbl, none_dbl, 20.0 } );

    REQUIRE( (res.zfill() == expected_res.zfill()).all() );
  }

  SECTION( "distinct" ) {

    using col_t = column<double>;

    const col_t col ( { none_dbl , 10.0, none_dbl, 10.0, 5.0, none_dbl } );
    const col_t res = col.distinct();

    const col_t expected_res( {10.0, 5.0 } );

    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "except" ) {

    using col_t = column<int>;

    const col_t col ( { none_int , 1, 2, 3 } );
    const col_t xs ( { 1, 2 } );
    const col_t res = col.except(xs);

    const col_t expected_res( {none_int, 3} );

    REQUIRE( (res.zfill() == expected_res.zfill() ).all() );
  }

  SECTION( "sort/rsort" ) {

    using col_t = column<double>;

    const col_t col ( { 3.0, 1.0, 2.0 } );
    const col_t sorted_col = col.sort();
    const col_t rsorted_col = col.rsort();

    const col_t expected_sorted_col( {1.0, 2.0, 3.0 } );
    const col_t expected_rsorted_col( {3.0, 2.0, 1.0} );

    REQUIRE( (sorted_col == expected_sorted_col ).all() );
    REQUIRE( (rsorted_col == expected_rsorted_col ).all() );
  }

  SECTION( "sums" ) {

    using col_t = column<double>;

    const col_t col ( { 3.0, none_dbl, 2.0 } );
    const col_t res = col.sums();

    const col_t expected_res( { 3.0, 3.0, 5.0 } );

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "msum" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 1, none_int, 3, 4, 5, 6 } );
    const col_t res = col.msum(2);

    const col_t expected_res( { 1, 1, 3, 7, 9, 11} );

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "mins" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 100, 10, 20, none_int, 5 });
    const col_t res = col.mins();

    const col_t expected_res ( { 100, 10, 10, 0, 0} );

    REQUIRE( (res == expected_res ).all() );
  }
 
  SECTION( "maxs" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 5, 10, 15, 20, none_int, 5 });
    const col_t res = col.maxs();

    const col_t expected_res ( { 5, 10, 15, 20, 20, 20 });

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "mmin" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 100, 10, 20, none_int, 5 });
    const col_t res = col.mmin(2);

    const col_t expected_res ( { 100, 10, 10, 0, 0} );

    REQUIRE( (res == expected_res ).all() );
  }
 
  SECTION( "prev/next" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 1, 2, 3, 4 ,5 } );
    const col_t pcol = col.prev();
    const col_t ncol = col.next();

    const col_t expected_pcol ( { none_uint, 1, 2, 3 ,4 } );
    REQUIRE( pcol.eq(expected_pcol).all() );

    const col_t expected_ncol ( { 2, 3, 4, 5, none_uint } );
    REQUIRE( ncol.eq(expected_ncol).all() );
  }
 
  SECTION( "mmax" ) {

    using col_t = column<unsigned int>;

    const col_t col ( { 5, 10, 15, 20, none_int, 5 });
    const col_t res = col.mmax(2);

    const col_t expected_res ( { 5, 10, 15, 20, 20, 5 });

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "log/log2/log10" ) {

    const column<unsigned> col ( { 1, 2, 3, none_int, 5 });

    const column<double> lcol = col.log();
    const column<double> expected_lcol ( { std::log(1), std::log(2), std::log(3), none_dbl, std::log(5) } );
    REQUIRE( (lcol == expected_lcol).all() );

    const column<double> l2col = col.log2();
    const column<double> expected_l2col( { std::log2(1), std::log2(2), std::log2(3), none_dbl, std::log2(5) } );
    REQUIRE( (l2col == expected_l2col ).all() );

    const column<double> l10col = col.log10();
    const column<double> expected_l10col ( { std::log10(1), std::log10(2), std::log10(3), none_dbl, std::log10(5) } );
    REQUIRE( (l10col == expected_l10col ).all() );
  }
 
  SECTION( "exp" ) {

    const column<unsigned> col ( { 1, 2, 3, none_int, 5 });

    const column<double> ecol = col.exp();
    const column<double> expected_ecol ( { std::exp(1), std::exp(2), std::exp(3), none_dbl, std::exp(5) } );
    REQUIRE( (ecol == expected_ecol).all() );
  }

  SECTION( "mod" ) {
    {
      const column<unsigned> col ( { 10, 20, 30, none_int, 50 });
  
      const column<double> mcol = col.mod(2);
      const column<double> expected_mcol ( { std::fmod(10,2), std::fmod(20,2), std::fmod(30,2), none_dbl, std::fmod(50,2) } );
      REQUIRE( (mcol == expected_mcol).all() );
    }

    {
      const column<double> col ( { 10.0, 20.0, 30.0, none_dbl, 50.0 });
  
      const column<double> mcol = col.mod(2);
      const column<double> expected_mcol ( { std::fmod(10.0,2), std::fmod(20.0,2), std::fmod(30.0,2), none_dbl, std::fmod(50.0,2) } );
      REQUIRE( (mcol == expected_mcol).all() );
    }
  }

  SECTION( "abs" ) {
    {
      const column<int> col ( { -1, -2 , 1, 2, none_int } );
      const column<int> acol = col.abs();
      const column<int> expected_acol ( { 1, 2 , 1, 2, none_int } );
  
      REQUIRE( acol.eq(expected_acol).all() );
    }

    {
      const column<unsigned> col ( { 1, 2 , 1, 2, none_uint } );
      const column<unsigned> acol = col.abs();
      const column<unsigned> expected_acol ( { 1, 2 , 1, 2, none_uint } );
  
      REQUIRE( acol.eq(expected_acol).all() );
    }
 
    {
      const column<double> col ( { -1.1, -2.1 , 0.0, 2.0, none_dbl } );
      const column<double> acol = col.abs();
      const column<double> expected_acol ({ 1.1, 2.1 , 0.0, 2.0, none_dbl });

      REQUIRE( acol.eq(expected_acol).all() );
    }
  }
 
  SECTION( "dot" ) {

    const column<int> c0({ 10, 20, none_int});
    const column<double> c1({ 1.1, 2.2, 3.3});

    const double res = c0.dot(c1);
    const double expected_res = 55;

    REQUIRE( res == expected_res);
  }
  SECTION( "xbar (int)" ) {

    const column<int> c0({ -3, -2, -1, 0, 1, 2, 3, none_int} );
    const column<int> expected_res({ -4, -2, -2, 0, 0, 2, 2, none_int});

    const column<int> res = c0.xbar(2);

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "xbar (double)" ) {

    const column<double> c0({ -5.0, -1.0, 0.0, 1.0, 5.0, none_dbl} );
    const column<double> expected_res({ -6.3, -2.1, 0.0, 0.0, 4.2, none_dbl} );

    const column<double> res = c0.xbar(2.1);

    REQUIRE( (res - expected_res).norm() < 1e-10 );
  }

  SECTION( "cross" ) {

    const column<double> c0({ 10.0, 20.0});
    const column<char>   c1({'a','b','c'});

    const std::pair<column<double>,column<char>> res = c0.cross(c1);
    const std::pair<column<double>,column<char>> expected_res = 
    {
        column<double>({10.0, 10.0, 10.0, 20.0, 20.0, 20.0}),
        column<char>({'a','b','c','a','b','c'})
    };

    REQUIRE( (res.first == expected_res.first).all() );
    REQUIRE( (res.second == expected_res.second).all() );
  }


  SECTION( "til" ) {

    const column<std::size_t> xs = til(10);
    const column<std::size_t> expected_xs({0,1,2,3,4,5,6,7,8,9});
    REQUIRE( xs.eq(expected_xs).all() );
  }

  SECTION( "in" ) {

    const column<int> xs({1,2,3,4,5,6,7,8,9,10});
    const column<int> ys({1,5,10});
    const column<bool> expected_r({true,false,false,false,true,false,false,false,false,true});
    REQUIRE( xs.in(ys).eq( expected_r ).all() );
  }
}
