// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/where.hh"

using namespace tables;

constexpr double dbl_none = prim_traits<double>::none;
constexpr unsigned int uint_none = prim_traits<unsigned int>::none;

struct c0 {};
struct c1 {};
struct c2 {};

using my_table_t = table<
  std::pair<c0,double>,
  std::pair<c1,unsigned int>
>;

TEST_CASE( "table", "[test0]" ) {

  SECTION( "empty table" ) {

   using empty_t = table<>;
   empty_t empty0;
   empty_t empty1;
   REQUIRE( empty0.nrows() == 0);
   REQUIRE( empty0.ncols() == 0);

   const column<unsigned> col0({1,2,3});
   const column<unsigned> col1({10,20,30});
   const auto res = empty0.template add<c0,c1>(col0,col1);

   using res_table_t = table<
     std::pair<c0,unsigned>,
     std::pair<c1,unsigned>
   >;

   const res_table_t expected_res({1,2,3},{10,20,30});

   REQUIRE( res.eq(expected_res).all().all() );
  }

  SECTION( "t.at" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   const px_t res = t.at ( where( t.col<security_id_t>() == std::string_view("abc") ) );

   const px_t expected_res(
      {"abc","abc"},
      {10.0, 40.0}
    );

   REQUIRE( res.eq(expected_res).all().all() );
  }

  SECTION( "take/drop" ) {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>
    >;

    const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10, 20, 30, 40, 50, 60}
    );

   const table_t t0 = t.take(2);

   const table_t expected_t0(
      {"abc","def"},
      {10, 20}
    );

   REQUIRE( t0.eq(expected_t0).all().all() );

   const table_t t1 = t.drop(2);

   const table_t expected_t1(
      {"ghi", "abc","def","ghi"},
      {30, 40, 50, 60}
    );

   REQUIRE( t1.eq(expected_t1).all().all() );
  }

  SECTION( "rtake/rdrop" ) {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>
    >;

    const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10, 20, 30, 40, 50, 60}
    );

   const table_t t0 = t.rtake(2);

   const table_t expected_t0(
      {"def","ghi"},
      {50 ,60 }
    );

   REQUIRE( t0.eq(expected_t0).all().all() );

   const table_t t1 = t.rdrop(2);

   const table_t expected_t1(
      {"abc","def","ghi", "abc"},
      {10, 20, 30, 40}
    );

   REQUIRE( t1.eq(expected_t1).all().all() );
  }

  SECTION( "t.sort_by" ) {

    const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );
  
   const auto t0 = t.sort_by<security_id_t>();

   const px_t expected_t0(
      {"abc","abc","def", "def","ghi","ghi"},
      {10.0, 40.0, 20.0, 50.0, 30.0, 60.0}
    );

   REQUIRE( t0.eq(expected_t0).all().all() );
  }

  SECTION( "t.sort" ) {

   const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );
  
   const auto t0 = t.sort();

   const px_t expected_t0(
      {"abc","abc","def", "def","ghi","ghi"},
      {10.0, 40.0, 20.0, 50.0, 30.0, 60.0}
    );

   REQUIRE( t0.eq(expected_t0).all().all() );
  }

  SECTION( "t.reverse" ) {

   const px_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );
  
   const auto t0 = t.reverse();

   const px_t expected_t0(
      {"ghi","def","abc", "ghi","def","abc"},
      {60.0, 50.0, 40.0, 30.0, 20.0, 10.0}
    );

   REQUIRE( t0.eq(expected_t0).all().all() );
  }

  SECTION( "t.append (row)" ) {

    const px_t t0(
      {"abc","def","ghi" },
      {10.0, 20.0, 30.0 }
    );

    const typename px_t::row_t row("zoo", 100.0);
  
    const auto res = t0.append(row);

    const px_t expected_res(
      {"abc","def","ghi", "zoo"},
      {10.0, 20.0, 30.0, 100.0 }
    );

    REQUIRE( res.eq(expected_res).all().all() );
  }


  SECTION( "t.append (table)" ) {

    const px_t t0(
      {"abc","def","ghi" },
      {10.0, 20.0, 30.0 }
    );

    const px_t t1(
      {"abc","def","ghi" },
      {40.0, 50.0, 60.0 }
    );
  
   const auto res = t0.append(t1);

   const px_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   REQUIRE( res.eq(expected_res).all().all() );
  }

  SECTION( "t.indices_by" ) {

    const px_vol_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );

    const auto gixs = t.indices_by<security_id_t>();

    auto it = gixs.find(std::make_tuple<std::string_view>("abc"));
    REQUIRE ( it != gixs.end() );

    const vixs_t expected_ixs0 = {0, 3};
    REQUIRE ( it != gixs.end());
    REQUIRE ( it->second == expected_ixs0);

    it = gixs.find(std::make_tuple<std::string_view>("def"));
    const vixs_t expected_ixs1 = {1, 4};
    REQUIRE ( it != gixs.end());
    REQUIRE ( it->second == expected_ixs1);

    it = gixs.find(std::make_tuple<std::string_view>("ghi"));
    const vixs_t expected_ixs2 = {2, 5};
    REQUIRE ( it != gixs.end());
    REQUIRE ( it->second == expected_ixs2);
  }

  SECTION( "fills" ) {


    const my_table_t t(
      {10.0, dbl_none, 20.0, 30.0},
      {1, uint_none, 2, 3 }
    );

    // fills
    const my_table_t tf = t.fills();

    const my_table_t expected_tf(
      {10.0, 10.0, 20.0, 30.0},
      {1, 1, 2, 3 }
    );
    REQUIRE ( tf.eq(expected_tf).all().all() );

    // fill_with
    const my_table_t tfw = t.fill_with({100.0, 100});

    const my_table_t expected_tfw(
      {10.0, 100.0, 20.0, 30.0},
      {1, 100, 2, 3 }
    );
    REQUIRE ( tfw.eq(expected_tfw).all().all() );

    // zfill
    const my_table_t tfz = t.zfill();

    const my_table_t expected_tfz(
      {10.0, 0.0, 20.0, 30.0},
      {1, 0, 2, 3 }
    );
    REQUIRE ( tfz.eq(expected_tfz).all().all() );
  }

  SECTION( "deltas" ) {

    const my_table_t t(
      {10.0, dbl_none, 10.0, 30.0},
      {1, uint_none, 1, 3 }
    );

    const my_table_t td = t.deltas();

    const my_table_t expected_td(
      {10.0, dbl_none, dbl_none, 20.0},
      {1, uint_none, uint_none, 2 }
    );
    REQUIRE ( td.eq(expected_td).all().all() );
  }

  SECTION( "differ" ) {

    const my_table_t t(
      {10.0, 10.0 ,dbl_none, dbl_none, 20.0, 20.0, 20.0},
      {1, 1, uint_none, uint_none, 2, 2, 3}
    );

    const column<bool> ds = t.differ();

    const column<bool> expected_ds({true, false, true, false, true, false, true});
    REQUIRE ( (ds == expected_ds).all() );
  }

  SECTION( "except" ) {

    const my_table_t t0(
      {1, 2, 3 },
      {1, 2, uint_none }
    );

    const my_table_t t1(
      {1, 3 },
      {1, uint_none }
    );

    const my_table_t rt = t0.except(t1);

    const my_table_t expected_rt(
      {2},
      {2}
    );
    REQUIRE ( rt.eq(expected_rt).all().all() );
  }

  SECTION( "next/prev") {

    using table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,std::string_view>
    >;

    const table_t t(
      {10, 20, 30},
      {"abc","def","ghi"}
    );

    const table_t nt = t.next();
    const table_t expected_nt(
      {20, 30, prim_traits<unsigned>::none},
      {"def","ghi", ""}
    );
    REQUIRE ( nt.eq(expected_nt).all().all() );

    const table_t pt = t.prev();
    const table_t expected_pt(
      {prim_traits<unsigned>::none,10,20},
      {"", "abc", "def"}
    );
    REQUIRE ( pt.eq(expected_pt).all().all() );
  }

  SECTION( "abs") {

    using table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,int>,
      std::pair<c2,double>
    >;

    const table_t t(
      {1, 2, 3},
      {-1, -2, 3},
      {-1.0, -2.0, -3.0}
    );

    const table_t t_abs = t.abs();

    const table_t expected_t_abs(
      {1, 2, 3},
      {1, 2, 3},
      {1.0, 2.0, 3.0}
    );

   REQUIRE( t_abs.eq(expected_t_abs).all().all() );
  } 

  SECTION( "log/log2/log10" ) {

    using table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,double>
    >;

    const table_t t(
      {1, 2, 3},
      {10.0, 20.0, 30.0}
    );
  
    using r_table_t = table<
      std::pair<c0,double>,
      std::pair<c1,double>
    >;

   // log
   const auto t1 = t.log();
   const r_table_t expected_t1(
      {std::log(1), std::log(2), std::log(3)},
      {std::log(10.0), std::log(20.), std::log(30.0)}
    );

   REQUIRE( t1.eq(expected_t1).all().all() );

   // log2
   const auto t2 = t.log2();
   const r_table_t expected_t2(
      {std::log2(1), std::log2(2), std::log2(3)},
      {std::log2(10.0), std::log2(20.), std::log2(30.0)}
    );

   REQUIRE( t2.eq(expected_t2).all().all() );
 
   // log10
   const auto t3 = t.log10();
   const r_table_t expected_t3(
      {std::log10(1), std::log10(2), std::log10(3)},
      {std::log10(10.0), std::log10(20.), std::log10(30.0)}
    );

   REQUIRE( t3.eq(expected_t3).all().all() );
 
  }

  SECTION( "exp" ) {

    using table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,double>
    >;

    const table_t t(
      {1, 2, 3},
      {10.0, 20.0, 30.0}
    );
  
    using r_table_t = table<
      std::pair<c0,double>,
      std::pair<c1,double>
    >;

    const auto t1 = t.exp();
    const r_table_t expected_t1(
      {std::exp(1), std::exp(2), std::exp(3)},
      {std::exp(10.0), std::exp(20.), std::exp(30.0)}
    );

    REQUIRE( t1.eq(expected_t1).all().all() );
  }

  SECTION( "mod" ) {

    using table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,double>
    >;

    using row_t = typename table_t::row_t;

    const table_t t(
      {1, 2, 3},
      {10.0, 20.0, 30.0}
    );
  
    using r_table_t = table<
      std::pair<c0,double>,
      std::pair<c1,double>
    >;

    const auto t1 = t.mod( row_t(2,3.0) );

    const r_table_t expected_t1(
      {std::fmod(1,2), std::fmod(2,2), std::fmod(3,2)},
      {std::fmod(10.0,3.0), std::fmod(20.,3.0), std::fmod(30.0,3.0)}
    );

    REQUIRE( t1.eq(expected_t1).all().all() );
  }


  SECTION( "max/min" ) {

    const my_table_t t(
      {10.0, dbl_none, 10.0, 30.0},
      {123, uint_none, 1, 3 }
    );

    using row_t = typename my_table_t::row_t;

    const row_t rmin = t.min();
    const row_t expected_rmin(0.0, 0);
    REQUIRE ( rmin == expected_rmin );

    const row_t rmax = t.max();
    const row_t expected_rmax(30.0,3);
    REQUIRE ( rmax == expected_rmax );
  }

  SECTION( "sum/avg" ) {

    const my_table_t t(
      {10.0, dbl_none, 20.0, 30.0},
      {1, uint_none, 2, 3 }
    );

    using row_t = typename my_table_t::row_t;
    const row_t rsum = t.sum();
    const row_t ravg = t.avg();

    const row_t expected_rsum(60.0,6);
    const row_t expected_ravg(20.0,2);
    REQUIRE ( rsum == expected_rsum );
    REQUIRE ( ravg == expected_ravg );
  }

  SECTION( "all/any" ) {

    using a_table_t = table<
      std::pair<c0,unsigned>,
      std::pair<c1,std::string_view>,
      std::pair<c2,bool>
    >;

    const a_table_t t(
      {1,2,3,uint_none},
      {"abc","def","","jkl"},
      {false, true, true, true}
    );

    using r_row_t = row<
      std::pair<c0,bool>,
      std::pair<c1,bool>,
      std::pair<c2,bool>
    >;

    // all
    const r_row_t r_all = t.all();
    const r_row_t r_expected_all(false, false, false);
    REQUIRE ( r_expected_all == r_all );

    // any
    const r_row_t r_any = t.any();
    const r_row_t r_expected_any(true, true, true);
    REQUIRE ( r_expected_any == r_any );
  }

  SECTION( "neg" ) {

    using a_table_t = table<
      std::pair<c0,bool>,
      std::pair<c1,bool>
    >;

    const a_table_t t(
      {false, true, false, true},
      {true, true, true, true}
    );
    const a_table_t t_neg = t.neg();

    const a_table_t expected_neg(
      {true, false, true, false},
      {false, false, false, false}
    );

    REQUIRE ( t_neg.eq(expected_neg).all().all() );
    REQUIRE ( t.neg().neg().eq(t).all().all() );
  }


  SECTION( "eq/ne" ) {

    const my_table_t t0(
      {10.0, dbl_none, 20.0, 30.0},
      {1, uint_none, 2, 3 }
    );

    const my_table_t t1(
      {11.0, 5.0, 20.0, 30.0},
      {1, uint_none, 20, 3 }
    );

    using r_table_t = table<
      std::pair<c0,bool>,
      std::pair<c1,bool>
    >;

    // eq
    const r_table_t t_eq = t0.eq(t1);     

    const r_table_t expected_t_eq(
      column<bool>({false, false, true, true}),
      column<bool>({true, true, false, true})
    );
    REQUIRE ( t_eq.eq(expected_t_eq).all().all() );

    // ne
    const r_table_t t_ne = t0.ne(t1);
    const r_table_t expected_t_ne(
      column<bool>({true,true,false,false}),
      column<bool>({false,false,true,false})
    );
    REQUIRE ( t_ne.eq(expected_t_ne).all().all() );
  }

  SECTION( "lt/le" ) {

    const my_table_t t0(
      {1.0, 2.0, 3.0, dbl_none },
      {10, 20, 30, uint_none }
    );

    const my_table_t t1(
      {10.0, 0.0, 30.0, 40.0},
      {uint_none, 20, 35, 40}
    );

    using r_table_t = table<
      std::pair<c0,bool>,
      std::pair<c1,bool>
    >;

    // lt
    const r_table_t t_lt = t0.lt(t1);     

    const r_table_t expected_t_lt(
      column<bool>({true, false, true, false}),
      column<bool>({true, false, true, false})
    );
    REQUIRE ( t_lt.eq(expected_t_lt).all().all() );

    // le
    const r_table_t t_le = t0.le(t1);     

    const r_table_t expected_t_le(
      column<bool>({true, false, true, false}),
      column<bool>({true, true, true, false})
    );
    REQUIRE ( t_le.eq(expected_t_le).all().all() );
  }
}
