// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;
using namespace std;

using sys_ns = chrono::sys_time<chrono::nanoseconds>;
using sys_ms = chrono::sys_time<chrono::milliseconds>;

TEST_CASE( "tcolumn", "[test0]" ) {

  SECTION( "apply" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now-24h, now-48h } );

    const auto f = [] ( const sys_ns tp ) { return tp - 12h; };

    const column<sys_ns> res = c.apply(f);
    const column<sys_ns> expected_res ({ now-12h, now - 36h, now - 60h});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "reverse" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now-24h, now-48h } );

    const column<sys_ns> res = c.reverse();
    const column<sys_ns> expected_res ({ now-48h, now-24h, now });

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "at/where" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now-24h, now-48h } );
    const column<bool> bc ( { true, true, false } );

    const column<sys_ns> res = c.at( bc.where() );
    const column<sys_ns> expected_res ({ now, now-24h});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "all/any" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;
    constexpr sys_ns zero = prim_traits<sys_ns>::zero;

    const column<sys_ns> c0( {now, none, now});
    const column<sys_ns> c1( {now, zero, now});
    const column<sys_ns> c2( {now, now, now});

    REQUIRE( !c0.all() );
    REQUIRE( !c1.all() );
    REQUIRE( c2.all() );

    REQUIRE( c0.any() );
    REQUIRE( c1.any() );
    REQUIRE( c2.any() );
  }

  SECTION( "is_none" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;

    const column<sys_ns> c( {now, none, now});
    const column<bool> res = c.is_none();

    const column<bool> expected_res({false,true,false});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator<=" ) {
  
    const auto now = chrono::system_clock::now();

    const column<sys_ns> c0( {now, now+1h, now+2h});
    const column<sys_ns> c1( {now, now-1h, now+3h});

    const column<bool> res0 = (c0 <= c1);
    const column<bool> res1 = (c1 <= now);
    const column<bool> res2 = (now <= c1);

    const column<bool> expected_res0 ({true,false,true});
    const column<bool> expected_res1 ({true,true,false});
    const column<bool> expected_res2 ({true,false,true});

    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator<" ) {
  
    const auto now = chrono::system_clock::now();

    const column<sys_ns> c0( {now, now+1h, now+2h});
    const column<sys_ns> c1( {now, now-1h, now+3h});

    const column<bool> res0 = (c0 < c1);
    const column<bool> res1 = (c1 < now);
    const column<bool> res2 = (now < c1);

    const column<bool> expected_res0 ({false,false,true});
    const column<bool> expected_res1 ({false,true,false});
    const column<bool> expected_res2 ({false,false,true});

    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }


  SECTION( "operator>=" ) {
  
    const auto now = chrono::system_clock::now();

    const column<sys_ns> c0( {now, now+1h, now+2h});
    const column<sys_ns> c1( {now, now-1h, now+3h});

    const column<bool> res0 = (c0 >= c1);
    const column<bool> res1 = (c1 >= now);
    const column<bool> res2 = (now >= c1);

    const column<bool> expected_res0 ({true,true,false});
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> expected_res1 ({true,false,true});
    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> expected_res2 ({true,true,false});
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator>" ) {
  
    const auto now = chrono::system_clock::now();

    const column<sys_ns> c0( {now, now+1h, now+2h});
    const column<sys_ns> c1( {now, now-1h, now+3h});

    const column<bool> res0 = (c0 > c1);
    const column<bool> res1 = (c1 > now);
    const column<bool> res2 = (now > c1);

    const column<bool> expected_res0 ({false,true,false});
    const column<bool> expected_res1 ({false,false,true});
    const column<bool> expected_res2 ({false,true,false});

    REQUIRE( (res0 == expected_res0).all() );
    REQUIRE( (res1 == expected_res1).all() );
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "fills" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;

    const column<sys_ns> c( {now, none, now+1ns});
    const column<sys_ns> res = c.fills();

    const column<sys_ns> expected_res({now,now,now+1ns});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "replace" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;

    const column<sys_ns> c0( {now, now+1ns, none});
    const column<sys_ns> c1( {none, now+1h, now+2h});
    const column<sys_ns> res = c0.replace(c1);

    const column<sys_ns> expected_res({now,now+1h,now+2h});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "fill_with(scalar)" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;

    const column<sys_ns> c( {now, now+1ns, none});
    const column<sys_ns> res = c.fill_with(now+24h);

    const column<sys_ns> expected_res({now,now+1ns,now+24h});

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "take/drop" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now+1h, now+2h});

    const column<sys_ns> res0 = c.take(2);
    const column<sys_ns> expected_res0( {now, now+1h} );
    REQUIRE( res0.eq(expected_res0).all() );

    const column<sys_ns> res1 = c.drop(2);
    const column<sys_ns> expected_res1( {now+2h} );
    REQUIRE( res1.eq(expected_res1).all() );
  }

  SECTION( "rtake/rdrop" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now+1h, now+2h});

    const column<sys_ns> res0 = c.rtake(2);
    const column<sys_ns> expected_res0( {now+1h, now+2h} );
    REQUIRE( res0.eq(expected_res0).all() );

    const column<sys_ns> res1 = c.rdrop(2);
    const column<sys_ns> expected_res1( {now} );
    REQUIRE( res1.eq(expected_res1).all() );
  }

  SECTION( "append" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now+1h, now+2h});

    const column<sys_ns> res = c.append(now+3h);
    const column<sys_ns> expected_res( {now, now+1h,now+2h,now+3h} );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "except" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c0( {now, now+1h, now+2h});
    const column<sys_ns> c1( {now+1h, now+2h});

    const column<sys_ns> res = c0.except(c1);
    const column<sys_ns> expected_res( {now} );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "apply" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now+1h, now+2h});
    const auto f = [](const sys_ns tp) { return tp + 1h; };

    const column<sys_ns> res = c.apply(f);
    const column<sys_ns> expected_res( {now+1h,now+2h,now+3h} );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "reverse" ) {

    const auto now = chrono::system_clock::now();

    const column<sys_ns> c( {now, now+1h, now+2h});

    const column<sys_ns> res = c.reverse();
    const column<sys_ns> expected_res( {now+2h,now+1h,now} );
    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "time_since_epoch" ) {

    const auto now = chrono::system_clock::now();
    constexpr sys_ns none_ns = prim_traits<sys_ns>::none;


    const column<sys_ns> c( { now, now+1s, none_ns });
    const column<std::chrono::nanoseconds> res = c.time_since_epoch();
    const column<std::chrono::nanoseconds> expected_res( { now.time_since_epoch(), (now+1s).time_since_epoch(), prim_traits<std::chrono::nanoseconds>::none } );

    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }
 
  SECTION( "time_point_cast" ) {

    constexpr chrono::sys_time<chrono::seconds> none_s = prim_traits<chrono::sys_time<chrono::seconds>>::none;
    constexpr chrono::sys_time<chrono::nanoseconds> none_ns = prim_traits<chrono::sys_time<chrono::nanoseconds>>::none;

    const auto now_ns = chrono::system_clock::now();
    const auto now_s = chrono::time_point_cast<chrono::seconds>(now_ns);

    const column<sys_ns> c( {now_ns, now_ns+1s, now_ns+2s, none_ns });

    const column<chrono::sys_seconds> res = c.time_point_cast<chrono::seconds>();
    const column<chrono::sys_seconds> expected_res( { now_s, now_s + 1s, now_s+2s, none_s });

    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
    
  }

  SECTION( "to_time_t" ) {

    const auto now = chrono::system_clock::now();

    constexpr sys_ns none = prim_traits<sys_ns>::none;

    const column<sys_ns> c( {now, now+1h, none});

    const column<std::time_t> res = c.to_time_t();
    const column<std::time_t> expected_res( { 
      std::chrono::system_clock::to_time_t(now), 
      std::chrono::system_clock::to_time_t(now+1h), 
      prim_traits<std::time_t>::none 
    } );
    
    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }

  SECTION( "operator+-(duration)" ) {

    const auto now = chrono::system_clock::now();
    constexpr sys_ns none_ns = prim_traits<sys_ns>::none;
    constexpr chrono::seconds none_ds = prim_traits<chrono::seconds>::none;

    const column<sys_ns> c0( {now, now, now, none_ns, now });
    const column<chrono::seconds> c1({1s, 2s, 3s, 4s, none_ds });

    const column<sys_ns> res0 = c0 + c1;
    const column<sys_ns> expected_res0({ now+1s, now+2s,now+3s,none_ns,none_ns});
    REQUIRE( res0.zfill().eq(expected_res0.zfill()).all() );

    const column<sys_ns> res1 = c0 - c1;
    const column<sys_ns> expected_res1({ now-1s, now-2s,now-3s,none_ns,none_ns});
    REQUIRE( res1.zfill().eq(expected_res1.zfill()).all() );
    
  }
}
