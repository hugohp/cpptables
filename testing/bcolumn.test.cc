// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

constexpr double none_dbl = prim_traits<double>::none;
constexpr int none_int = prim_traits<int>::none;

TEST_CASE( "bcolumns", "[test0]" ) {

  SECTION( "apply" ) {

    const column<bool> c ( { true,false,true} );
    const auto f = [] ( const bool v ) { return (int)v; };

    const column<int> rs = c.apply(f);
    const column<int> expected_rs ({ 1,0,1});

    REQUIRE( (rs == expected_rs).all() );
  }

  SECTION( "reverse" ) {

    const column<bool> c ( { false,false,true} );
    const column<bool> rc = c.reverse();

    const column<bool> expected_rc ( { true,false,false} );

    REQUIRE( (rc == expected_rc).all() );
  }

  SECTION( "replace" ) {

    const column<bool> c0 ( { false,false,true} );
    const column<bool> c1 ( { true,true,true} );
    const column<bool> rc = c0.replace(c1);

    const column<bool> expected_rc ( { true,true,true} );

    REQUIRE( (rc == expected_rc).all() );
  }

  SECTION( "at/where" ) {

    const column<bool> c ( { true, true, false,false} );
    const column<bool> res = c.at( where( c ) );
    const column<bool> expected_res ({true,true});
    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "all/any" ) {

    const column<bool> c0 ( { true, true} );
    const column<bool> c1 ( { true, false} );
    const column<bool> c2 ( { false, false} );

    REQUIRE( c0.all() );
    REQUIRE( !c1.all() );

    REQUIRE( c0.any() );
    REQUIRE( c1.any() );
    REQUIRE( ! c2.any() );
  }

  SECTION( "operator<" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res = c0 < c1;
    const column<bool> expected_res( { false, false, true, false} );

    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "operator<=" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res = c0 <= c1;
    const column<bool> expected_res( { true, false, true, true} );

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator>=" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res = (c0 >= c1);
    const column<bool> expected_res( { true, true, false, true} );

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator>" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res = c0 > c1;
    const column<bool> expected_res( { false, true, false, false} );

    REQUIRE( res.eq(expected_res).all() );
  }

  SECTION( "operator&&" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res0 = c0 && c1;
    const column<bool> expected_res0( { true, false, false, false} );

    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> res1 = c0 && true;
    const column<bool> expected_res1( { true, true, false, false} );

    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> res2 = true && c0;
    const column<bool> expected_res2( { true, true, false, false} );

    REQUIRE( (res2 == expected_res2).all() );
 
  }

  SECTION( "operator||" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, false,true,false });

    const column<bool> res0 = c0 || c1;
    const column<bool> expected_res0( { true, true, true, false} );

    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> res1 = c0 || true;
    const column<bool> expected_res1( { true, true, true, true} );

    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> res2 = true || c0;
    const column<bool> expected_res2( { true, true, true, true} );

    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "distinct" ) {

    const column<bool> c0 ( { true, true,false,false });
    const column<bool> c1 ( { true, true});

    const column<bool> res0 = c0.distinct();
    const column<bool> expected_res0( {true,false} );
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> res1 = c1.distinct();
    const column<bool> expected_res1( {true} );
    REQUIRE( (res1 == expected_res1).all() );
  }

  SECTION( "differ" ) {

    const column<bool> c ( { true, true,false,false });

    const column<bool> res = c.differ();
    const column<bool> expected_res( {true,false,true,false});
    REQUIRE( (res == expected_res).all() );
  }

  SECTION( "except" ) {

    const column<bool> c ( { true, false,true, false });
    const column<bool> c1 ( { true });
    const column<bool> c2 ( { true, false});

    const column<bool> res0 = c.except(c1);
    const column<bool> expected_res0( {false,false} );
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> res1 = c.except(c2);
    REQUIRE( res1.size() == 0 );
  }

  SECTION( "sort/rsort" ) {

    const column<bool> c ( { true, false,true, false });
    const column<bool> sc = c.sort();

    const column<bool> expected_sc( {false,false,true,true});
    REQUIRE( (sc == expected_sc).all() );

    const column<bool> rsc = c.rsort();
    const column<bool> expected_rsc( {true,true,false,false});
    REQUIRE( (rsc == expected_rsc).all() );
  }

  SECTION( "mins" ) {

    const column<bool> c ( { true, false,true, false });
    const column<bool> res = c.mins();

    const column<bool> expected_res ( { true, false, false, false});

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "maxs" ) {

    const column<bool> c ( { false, true,true, false });
    const column<bool> res = c.maxs();

    const column<bool> expected_res ( { false, true, true, true});

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "mmin" ) {

    const column<bool> c ( { true, false,true, false });
    const column<bool> res = c.mmin(2);

    const column<bool> expected_res ( { true, false, false, false});

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "prev/next" ) {

    using col_t = column<bool>;

    const col_t col ( { true, false, true, true } );
    const col_t pcol = col.prev();
    const col_t ncol = col.next();

    const col_t expected_pcol ( { false, true, false, true });
    REQUIRE( pcol.eq(expected_pcol).all() );

    const col_t expected_ncol ( { false, true, true, false });
    REQUIRE( ncol.eq(expected_ncol).all() );
  }

  SECTION( "mmax" ) {

    const column<bool> c ( { false, false,true, false });
    const column<bool> res = c.mmax(2);

    const column<bool> expected_res ( { false, false, true, true});

    REQUIRE( (res == expected_res ).all() );
  }

  SECTION( "all/any/neg" ) {

    const column<bool> c ( { false, false,true, false });

    const bool all = c.all();
    REQUIRE( !all );

    const bool any = c.any();
    REQUIRE( any );

    const column<bool> nc = c.neg();
    const column<bool> expected_nc ( { true, true, false, true});
    REQUIRE( (nc == expected_nc ).all() );
  }

  SECTION( "is_none" ) {

    const column<bool> c ( { true, false} );
    const column<bool> bc = c.is_none();
    const column<bool> expected_bc({false,false});

    REQUIRE( (bc == expected_bc).all() );
  }

  SECTION( "cross" ) {

    const column<bool> c0 ( { true,false });
    const column<unsigned> c1 ( { 1, 2});

    const std::pair<column<bool>,column<unsigned>> res = c0.cross(c1);
    const std::pair<column<bool>,column<unsigned>> expected_res = 
    {
        column<bool>({true,true,false,false}),
        column<unsigned>({1,2,1,2})
    };

    REQUIRE( (res.first == expected_res.first).all() );
    REQUIRE( (res.second == expected_res.second).all() );
  }
}
