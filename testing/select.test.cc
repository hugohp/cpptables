// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"

using namespace tables;

TEST_CASE( "select", "[test0]" ) {

  SECTION( "select" ) {

    const px_vol_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );


   const px_t t0 = t.select<security_id_t,price_t>();

   const px_t expected_t0(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0}
    );

   REQUIRE( t0.eq(expected_t0 ).all().all() );
  }
}
