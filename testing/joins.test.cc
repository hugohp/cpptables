// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"

using namespace tables;
constexpr double dbl_none = prim_traits<double>::none;
constexpr unsigned uint_none = prim_traits<unsigned>::none;

struct c0{};
struct c1{};
struct c2{};

TEST_CASE( "joins", "[joins]" ) {

  SECTION( "lj" ) {

    const px_vol_t t (
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );

    using foo_table_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<foo_t,double>
    >;

    const foo_table_t vt(
      {"abc","def","ghi"},
      {1000.0, 4000.0, 9000.0}
    );

    using foo_kt = ktable<foo_table_t,std::tuple<security_id_t>>;
    const foo_kt kvt(vt);

    using result_t = px_vol_t::add_t<std::pair<foo_t,double>>;

    const result_t rt = t.lj(kvt);

    const result_t expected_rt(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {1000.0, 4000.0, 9000.0, 1000.0, 4000.0, 9000.0}
     );

    REQUIRE( rt.eq(expected_rt ).all().all() );
  }

  SECTION( "lj two keys" ) {

    using primary_table_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<listingid_t,std::string_view>,
      std::pair<price_t,double>,
      std::pair<volume_t,unsigned int>
    >;
 
    const primary_table_t t(
      {"abc","abc","def","def","ghi","ghi"},
      {"l0","l1","l2","l2","l3","l4" },
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );

    using foo_table_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<listingid_t,std::string_view>,
      std::pair<foo_t,double>
    >;
 
    const foo_table_t vt(
      {"abc","def","ghi"},
      {"l0","l2","l3"},
      {1000.0, 4000.0, 9000.0}
    );

    using foo_kt = ktable<foo_table_t,std::tuple<security_id_t,listingid_t>>;
    const foo_kt kvt(vt);

    using result_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<listingid_t,std::string_view>,
      std::pair<price_t,double>,
      std::pair<volume_t,unsigned int>,
      std::pair<foo_t,double>
    >;

    const result_t rt = t.lj(kvt);

    const result_t expected_rt(
      {"abc","abc","def","def","ghi","ghi"},
      {"l0","l1","l2","l2","l3","l4" },
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {1000.0, dbl_none, 4000.0, 4000.0, 9000.0, dbl_none}
    );
    
    REQUIRE( rt.eq(expected_rt).all().all() );

  }

  SECTION( "ljf" ) {

   using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table_t t_lhs(
      {"abc","def","ghi", "abc","def","ghi"},
      {1,2,3,4,5,6},
      {10,20,30,40,50,60}
    );

    const table_t t_rhs(
      {"abc","ghi"},
      {111,222},
      {uint_none,444}
    );

    const table_t rt = t_lhs.ljf( t_rhs.key_by<c0>() );

    const table_t expected_rt(
      {"abc","def","ghi", "abc","def","ghi"},
      {111,2,222,111,5,222},
      {10,20,444,40,50,444}
     );

    REQUIRE( rt.eq(expected_rt ).all().all() );
  }


  SECTION( "ij" ) {

    using primary_table_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<listingid_t,std::string_view>,
      std::pair<price_t,double>,
      std::pair<volume_t,unsigned int>
    >;
 
    const primary_table_t t(
      {"abc","abc","def","def","ghi","ghi"},
      {"l0","l1","l2","l2","l3","l4" },
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );

    using foo_table_t = table<
      std::pair<security_id_t,std::string_view>,
      std::pair<listingid_t,std::string_view>,
      std::pair<foo_t,double>
    >;
 
    const foo_table_t vt (
      {"abc","def","ghi"},
      {"l0","l2","l3"},
      {1000.0, 4000.0, 9000.0}
    );

    using foo_kt = ktable<foo_table_t,std::tuple<security_id_t,listingid_t>>;
    const foo_kt kvt(vt);

    using result_t = table<
       std::pair<security_id_t,std::string_view>,
       std::pair<listingid_t,std::string_view>,
       std::pair<price_t,double>,
       std::pair<volume_t,unsigned int>,
       std::pair<foo_t,double>
     >;

    const result_t rt = t.ij(kvt);

    const result_t expected_rt (
      {"abc","def","def","ghi"},
      {"l0","l2","l2","l3"},
      {10.0,30.0, 40.0, 50.0},
      {100, 300, 400, 500},
      {1000.0, 4000.0, 4000.0, 9000.0}
    );
    
    REQUIRE( rt.eq(expected_rt).all().all() );
  }

  SECTION( "uj" ) {

    using table0_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>
    >;

    const table0_t t0(
      {"abc","def","ghi"},
      {10.0, 20.0, 30.0}
    );

    using table1_t = table<
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const table1_t t1(
      {40.0, 50.0},
      {100, 200}
    );

    using res_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const res_table_t res = t0.uj(t1);

    const res_table_t expected_res =  res_table_t(
      {"abc","def","ghi", "", ""},
      {10.0, 20.0, 30.0, 40.0, 50.0},
      {uint_none,uint_none,uint_none,100, 200}
     );

    REQUIRE( res.eq(expected_res ).all().all() );
  }

  SECTION( "ijf" ) {

   using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table_t t_lhs(
      {"abc","def","ghi", "abc","def","ghi"},
      {1,2,3,4,5,6},
      {10,20,30,40,50,60}
    );

    const table_t t_rhs(
      {"abc","ghi"},
      {111,222},
      {uint_none,444}
    );

    const table_t rt = t_lhs.ijf( t_rhs.key_by<c0>() );

    const table_t expected_rt(
      {"abc","ghi", "abc","ghi"},
      {111,222,111,222},
      {10,444,40,444}
     );

    REQUIRE( rt.eq(expected_rt ).all().all() );
  }

  SECTION( "aj/aj0" ) {

   using table0_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>
    >;

    const table0_t t0(
      {"abc","def","ghi"},
      {0,5,10}
    );
 
   using table1_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table1_t t1(
      {"abc","def","ghi","abc","def","ghi","abc","def","ghi"},
      {1,2,3,4,5,6,7,8,9},
      {100,200,300,400,500,600,700,800,900}
    );

    const table1_t aj_res = t0.aj<c1>( t1.key_by<c0>() );

    const table1_t expected_aj_res(
      {"abc","def","ghi"},
      {0,5,10},
      {uint_none,500,900}
    );
    REQUIRE( aj_res.eq(expected_aj_res ).all().all() );

    const table1_t aj0_res = t0.aj0<c1>( t1.key_by<c0>() );

    const table1_t expected_aj0_res(
      {"abc","def","ghi"},
      {uint_none,5,9},
      {uint_none,500,900}
    );
    REQUIRE( aj0_res.eq(expected_aj0_res ).all().all() );
  }
}
