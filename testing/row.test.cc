// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;

constexpr double none_dbl = prim_traits<double>::none;
constexpr int none_int = prim_traits<int>::none;

struct c0 {};
struct c1 {};
struct c2 {};
struct c3 {};

TEST_CASE( "row", "[test0]" ) {

  SECTION( "all/any" ) {

    using row_t = row<
      std::pair<c0,unsigned>,
      std::pair<c1,double>,
      std::pair<c2,std::string_view>,
      std::pair<c3,bool>
    >;
    const row_t r0(10,20.0, "foo",true);

    REQUIRE( r0.all() );
    REQUIRE( r0.any() );

    const row_t r1(10,20.0, "foo",false);

    REQUIRE( ! r1.all() );
    REQUIRE( r1.any() );

    const row_t r2(10,20.0, "",true);

    REQUIRE( ! r1.all() );
    REQUIRE( r1.any() );
  }

  SECTION( "eq/ne" ) {

    using row_t = row<
      std::pair<c0,unsigned>,
      std::pair<c1,double>,
      std::pair<c2,std::string_view>,
      std::pair<c3,bool>
    >;
    const row_t r0(10,20.0, "foo",true);
    const row_t r1(10,21.0, "foo",true);

    REQUIRE( r0.eq(r0) );
    REQUIRE( ! r0.ne(r0) );

    REQUIRE( ! r0.eq(r1) );
    REQUIRE( r0.ne(r1) );
  }

  SECTION( "lt/le/gt/ge" ) {

    using row_t = row<
      std::pair<c0,unsigned>,
      std::pair<c1,double>,
      std::pair<c2,std::string_view>,
      std::pair<c3,bool>
    >;
    const row_t r0(10,20.0, "foo",true);
    const row_t r1(10,21.0, "foo",true);

    REQUIRE( r0.le(r0) );
    REQUIRE( r0.lt(r1) );

    REQUIRE( r1.ge(r1) );
    REQUIRE( r1.gt(r0) );
  }

  SECTION( "neg" ) {

    using row_t = row<
      std::pair<c0,bool>,
      std::pair<c1,bool>
    >;
    const row_t r(true, false);
    const row_t r_neg = r.neg();

    const row_t expected_neg(false, true);

    REQUIRE( r.eq(expected_neg).all() );
    REQUIRE( r.neg().neg().eq(r) );
  }
}
