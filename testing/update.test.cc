// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <vector>
#include <random>
#include <iostream>
#include <catch2/catch_test_macros.hpp>
#include "common.test.hh"
#include "../include/cpptables/table.hh"
#include "../include/cpptables/ktable.hh"
#include "../include/cpptables/types.hh"

using namespace tables;

struct c0{ static constexpr char const* const name = "Col0"; };
struct c1{ static constexpr std::string_view name = "Col1"; };
struct c2{ static constexpr std::string_view name = "Col2"; };

column<double> fdbl( const column<double>& xs )
{
  std::vector<double> res(xs.size());
  for ( std::size_t i = 0 ; i < xs.size() ; ++i )
  {
    res[i] = xs[i]*2.0;
  }
  return column<double>(res);
}

column<unsigned> fuint( const column<unsigned>& xs )
{
  std::vector<unsigned> res(xs.size());
  for ( std::size_t i = 0 ; i < xs.size() ; ++i )
  {
    res[i] = xs[i]*2;
  }
  return column<unsigned>(res);
}

TEST_CASE( "update", "[test0]" ) {

  SECTION( "t.update(column<T>)" ) {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );

   const column<unsigned> upd0 ( { 1,2,3,4,5,6,} );
   const column<unsigned> upd1 ( { 1000,2000,3000,4000,5000,6000 } );

   using res_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

   const res_table_t res = t.update<c1,c2>(upd0,upd1);

   const res_table_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {1, 2, 3, 4, 5, 6},
      {1000,2000,3000,4000,5000,6000}
    );

   REQUIRE( res.eq(expected_res ).all().all() );
  }

  SECTION( "t.update(table)" ) {

    using table0_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const table0_t t0(
      {"abc","def","ghi"},
      {10.0, 20.0, 30.0},
      {100, 200, 300}
    );

    using table1_t = table<
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const table1_t t1(
      {11.0,12.0,13.0},
      {1,2,3}
    );

   const table0_t res = t0.update(t1);

   const table0_t expected_res(
      {"abc","def","ghi"},
      {11.0,12.0,13.0},
      {1,2,3}
    );

   REQUIRE( res.eq(expected_res ).all().all() );
  }

  SECTION( "t.update(cte)" ) {

    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,double>,
      std::pair<c2,unsigned>
    >;

    const table_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 }
    );
   

   using res_table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
   >;

   const res_table_t res = t.update<c1,c2>( 100U, 100U);

   const res_table_t expected_res(
      {"abc","def","ghi", "abc","def","ghi"},
      {100,100,100,100,100,100},
      {100,100,100,100,100,100}
    );

   REQUIRE( res.eq(expected_res ).all().all() );
  }

  SECTION( "t.update_at(ixs,cte)" ) {

    const px_vol_foo_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
    );

   const auto t0 = t.update_at<foo_t>( vixs_t({0,3}), 100.0 );

   const px_vol_foo_t expected_t0(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {100.0, 0.0, 0.0, 100.0, 0.0, 0.0 }
    );

   REQUIRE( t0.eq(expected_t0 ).all().all() );
  }

  SECTION( "t.update_at(ixs,cte)" ) {

    const px_vol_foo_t t(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
    );

   const auto t0 = t.update_at<foo_t>( vixs_t({0,3}), 100.0 );

   const px_vol_foo_t expected_t0(
      {"abc","def","ghi", "abc","def","ghi"},
      {10.0, 20.0, 30.0, 40.0, 50.0, 60.0},
      {100, 200, 300, 400, 500, 600 },
      {100.0, 0.0, 0.0, 100.0, 0.0, 0.0 }
    );

   REQUIRE( t0.eq(expected_t0 ).all().all() );
  }

  SECTION( "t.updatef (column)" ) {

    constexpr unsigned none_uint = prim_traits<unsigned>::none;
    using table_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table_t t(
      {"abc","def","ghi", "jkl", "mno" },
      {1, none_uint, none_uint, 4, 5},
      {10, 20, 30, 40, 50}
    );

    const column<unsigned> col0({100, none_uint, 300, 400, 500});
    const column<unsigned> col1({1000, none_uint, 3000, 4000, 5000});

    const table_t res = t.updatef<c1,c2>( col0, col1);

    const table_t expected_res(
      {"abc","def","ghi", "jkl", "mno" },
      {100, none_uint, 300, 400, 500},
      {1000, 20, 3000, 4000, 5000}
    );

   REQUIRE( res.eq(expected_res ).all().all() );
  }

  SECTION( "t.updatef (table)" ) {

    constexpr unsigned none_uint = prim_traits<unsigned>::none;
    using table0_t = table<
      std::pair<c0,std::string_view>,
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table0_t t0(
      {"abc","def","ghi", "jkl", "mno" },
      {1, none_uint, none_uint, 4, 5},
      {10, 20, 30, 40, 50}
    );

    using table1_t = table<
      std::pair<c1,unsigned>,
      std::pair<c2,unsigned>
    >;

    const table1_t t1(
      {100, none_uint, 300, 400, 500},
      {1000, none_uint, 3000, 4000, 5000}
    );


    const table0_t res = t0.updatef(t1);

    const table0_t expected_res(
      {"abc","def","ghi", "jkl", "mno" },
      {100, none_uint, 300, 400, 500},
      {1000, 20, 3000, 4000, 5000}
    );

   REQUIRE( res.eq(expected_res ).all().all() );
  }
}
