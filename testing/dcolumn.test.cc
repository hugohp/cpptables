// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include "common.test.hh"
#include "../include/cpptables/column.hh"
#include "../include/cpptables/types.hh"
#include "../include/cpptables/where.hh"
#include <catch2/catch_test_macros.hpp>

using namespace tables;
using namespace std;

using ns = chrono::nanoseconds;

TEST_CASE( "dcolumn", "[test0]" ) {

  SECTION( "operator<=" ) {
  
    const column<ns> c0( { 1ns, 2ns, 3ns } );
    const column<ns> c1( { 1ns, 0ns, 5ns } );

    const column<bool> res0 = (c0 <= c1);
    const column<bool> res1 = (c1 <= 1ns);
    const column<bool> res2 = (1ns <= c1);

    const column<bool> expected_res0 ({true,false,true});
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> expected_res1 ({true,true,false});
    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> expected_res2 ({true,false,true});
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator<" ) {
  
    const column<ns> c0( { 1ns, 2ns, 3ns } );
    const column<ns> c1( { 1ns, 0ns, 5ns } );

    const column<bool> res0 = (c0 < c1);
    const column<bool> res1 = (c1 < 1ns);
    const column<bool> res2 = (1ns < c1);

    const column<bool> expected_res0 ({false,false,true});
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> expected_res1 ({false,true,false});
    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> expected_res2 ({false,false,true});
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator>=" ) {
  
    const column<ns> c0( { 1ns, 2ns, 3ns } );
    const column<ns> c1( { 1ns, 0ns, 5ns } );

    const column<bool> res0 = (c0 >= c1);
    const column<bool> res1 = (c1 >= 1ns);
    const column<bool> res2 = (1ns >= c1);

    const column<bool> expected_res0 ({true,true,false});
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> expected_res1 ({true,false,true});
    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> expected_res2 ({true,true,false});
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "operator>" ) {
  
    const column<ns> c0( { 1ns, 2ns, 3ns } );
    const column<ns> c1( { 1ns, 0ns, 5ns } );

    const column<bool> res0 = (c0 > c1);
    const column<bool> res1 = (c1 > 1ns);
    const column<bool> res2 = (1ns > c1);

    const column<bool> expected_res0 ({false,true,false});
    REQUIRE( (res0 == expected_res0).all() );

    const column<bool> expected_res1 ({false,false,true});
    REQUIRE( (res1 == expected_res1).all() );

    const column<bool> expected_res2 ({false,true,false});
    REQUIRE( (res2 == expected_res2).all() );
  }

  SECTION( "duration_cast" ) {

    constexpr ns none_ns = prim_traits<chrono::nanoseconds>::none;
    constexpr chrono::seconds none_s = prim_traits<chrono::seconds>::none;

    const auto now_ns = chrono::system_clock::now().time_since_epoch();

    const column<ns> c( {now_ns, now_ns+1s, none_ns });
    const column<chrono::seconds> res = c.duration_cast<chrono::seconds>();

    const column<chrono::seconds> expected_res( { 
      chrono::duration_cast<chrono::seconds>(now_ns), 
      chrono::duration_cast<chrono::seconds>(now_ns+1s), 
      none_s
    });

    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }

  SECTION( "floor" ) {

    const column<chrono::milliseconds> c( { 1000ms, 1001ms, 1900ms } );
    const column<chrono::seconds> res = c.floor<chrono::seconds>();

    const column<chrono::seconds> expected_res( { 1s, 1s, 1s, } );
    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }

  SECTION( "ceil" ) {

    const column<chrono::milliseconds> c( { 1000ms, 1001ms, 1900ms } );
    const column<chrono::seconds> res = c.ceil<chrono::seconds>();

    const column<chrono::seconds> expected_res( { 1s, 2s, 2s, } );
    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }

  SECTION( "round" ) {

    const column<chrono::milliseconds> c( { 1000ms, 1001ms, 1900ms } );
    const column<chrono::seconds> res = c.round<chrono::seconds>();

    const column<chrono::seconds> expected_res( { 1s, 1s, 2s, } );
    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }

  SECTION( "abs" ) {

    const column<chrono::milliseconds> c( { 1000ms, -1001ms, 1900ms } );
    const column<chrono::milliseconds> res = c.abs();

    const column<chrono::milliseconds> expected_res( { 1000ms, 1001ms, 1900ms });
    REQUIRE( res.zfill().eq(expected_res.zfill()).all() );
  }
}
