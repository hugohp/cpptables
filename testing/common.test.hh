// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <string_view>
#include "../include/cpptables/column.hh"
#include "../include/cpptables/traits.hh"
#include "../include/cpptables/table.hh"

using namespace tables;

struct security_id_t  {};
struct listingid_t  {};
struct price_t {};
struct volume_t {};
struct foo_t {};

namespace tables
{
template<>
struct type_traits<security_id_t>
{
  constexpr static char const * const name = "secid";
};

template<>
struct type_traits<listingid_t>
{
  constexpr static char const * const name = "listing";
};

template<>
struct type_traits<price_t>
{
  constexpr static char const * const name = "price";
};

template<>
struct type_traits<volume_t>
{
  constexpr static char const * const name = "volume";
};

template<>
struct type_traits<foo_t>
{
  constexpr static char const * const name = "foo";
};

using px_t = table<
  std::pair<security_id_t,std::string_view>,
  std::pair<price_t,double>
>;

using px_vol_t = px_t::add_t<std::pair<volume_t,unsigned int>>;   // secid,price,volume
using px_vol_foo_t = px_vol_t::add_t<std::pair<foo_t,double>>;  // secid,price,volume,foo

} // namespace tables


