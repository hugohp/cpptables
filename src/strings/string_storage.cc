// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <unordered_set>
#include <string_view>
#include <memory>
#include "cpptables/strings/string_storage.hh"

namespace tables {
namespace impl {

namespace {


static std::unordered_set<std::string>& get_set_of_strings()
{
  static std::unordered_set<std::string> s;
  return s;
}

} // namespace anonymous


string_storage& string_storage::instance()
{
  static string_storage inst;
  return inst;
};

string_storage::string_storage()
{}

string_storage::~string_storage() 
{};

// ==================================== add_string =================================

std::string_view string_storage::add_string(const std::string_view sv)
{
  std::unordered_set<std::string>& sstrs = get_set_of_strings();

  return std::string_view(*(sstrs.emplace(sv).first));
}

// ==================================== add_string =================================

std::string_view string_storage::add_string(std::string&& sv)
{
  std::unordered_set<std::string>& sstrs = get_set_of_strings();

  return std::string_view(*(sstrs.insert(std::move(sv)).first));
}

// ==================================== add_strings =================================

std::vector<std::string_view> string_storage::add_strings(const std::vector<std::string_view>& svs)
{
  std::unordered_set<std::string>& sstrs = get_set_of_strings();
  
  std::vector<std::string_view> rsvs;
  rsvs.reserve(svs.size());
  for ( const std::string_view sv : svs)
  {
    rsvs.push_back(std::string_view(*(sstrs.emplace(sv).first)));
  }
  return rsvs;
}
 
// ==================================== n_strings  =================================

std::size_t string_storage::n_strings() const
{
  return get_set_of_strings().size();
}

} // namespace impl
} // namespace tables
