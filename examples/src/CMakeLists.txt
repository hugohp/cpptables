add_executable (
  example
  main.cc
  covid.cc
  readme.cc
)

target_link_libraries(example PRIVATE cpptables::cpptables)

add_compile_options(-Wall -std=c++20)
