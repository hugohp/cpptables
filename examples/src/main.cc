// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <string_view>
#include <string>
using namespace std;

extern void covid(const string& csv_file_path);
extern void questdb(const string& csv_file_path);
extern void readme();

int main(int argc, char const * const argv[])
{
  if ( argc < 2 )
  {
    return 1;
  }

  const string_view example = argv[1];

  if ( example == "covid" )
  {
    if ( argc < 3)
    {
      return 1;
    }
    const std::string csv_file_path(argv[2]);
    covid(csv_file_path);
  }
  else if ( example == "readme" )
  {
    readme();
  }
  return 0;
}
