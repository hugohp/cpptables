// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <cpptables/table.hh>
#include <cpptables/csv.hh>
#include <cpptables/where.hh>
#include "covid_schema.hh"
#include <iostream>

using namespace tables;
using namespace std;


void covid(const std::string& csv_file_path)
{
  covid_table_t t = from_csv<covid_table_t>::run(csv_file_path, csv_options::header);

  // Prune out unwanted data.
  t = t.at( where( t.col<continent_t>() != std::string_view("Other") ) );
  
  std::cout << "Top 10 rows:\n" << t.take(10) << "\n";

  // Total number of deaths
  const unsigned int n_deaths = t.col<deaths_t>().sum();
  std::cout << "Total number of deaths:" << n_deaths << "\n";

  // Numbers of deaths by country
  const auto fsum = [](const column<auto>& xs) { return xs.sum(); };
  const auto t1 = 
      t.fby<country_t>().agg<deaths_t>(fsum).rsort_by<deaths_t>();
  //    ^                     ^               ^ 
  //    |                     |               | 
  //    |                     |               Sort in descending order by deaths_t 
  //    |                     Apply aggregate to deaths_t
  //    Group to aggregate by
  cout << "Death by country:\n" << t1.take(10) << "\n";

  // Top two most deadly countries by continent
  const auto t2 =
      t.select<continent_t,country_t>().distinct().lj( t1.key_by<country_t>() ).group_by<continent_t>().rsort_by<deaths_t>().take(2);
  //    ^                               ^          ^                            ^                       ^                    ^
  //    |                               |          |                            |                       |                    |
  //    |                               |          |                            |                       |                    Select top two results per group
  //    |                               |          |                            |                       sort in descending order by deaths (per group)
  //    |                               |          |                            Group by continent
  //    |                               |          left-join with t1 on country_t column
  //    |                               Select distinct rows
  //    Select required columns

  std::cout << "Top two most deadly countries by continent:\n" << t2 << "\n";
}
