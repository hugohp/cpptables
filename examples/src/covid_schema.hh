// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cpptables/table.hh>

using namespace tables;

struct day_t { constexpr static char const * const name = "day"; };
struct month_t { constexpr static char const * const name = "month"; };
struct year_t { constexpr static char const * const name = "year"; };
struct cases_t { constexpr static char const * const name = "cases"; };
struct deaths_t { constexpr static char const * const name = "deaths"; };
struct country_t { constexpr static char const * const name = "country"; };
struct loc_code_t { constexpr static char const * const name = "loc_code"; };
struct population_t { constexpr static char const * const name = "population"; };
struct continent_t { constexpr static char const * const name = "continent"; };
struct rate_t { constexpr static char const * const name = "rate"; };

using covid_table_t = table<
  std::pair<day_t,unsigned>,
  std::pair<month_t,unsigned>,
  std::pair<year_t,unsigned>,
  std::pair<cases_t,unsigned>,
  std::pair<deaths_t,unsigned>,
  std::pair<country_t,std::string_view>,
  std::pair<loc_code_t,unsigned>,
  std::pair<population_t,unsigned>,
  std::pair<continent_t,std::string_view>,
  std::pair<rate_t,double>
>;
