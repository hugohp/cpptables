cmake_minimum_required (VERSION 3.5.1)
project (examples)

find_package(tuple_ext REQUIRED)
find_package(cpptables REQUIRED)

add_compile_options(-Wall -std=c++20)

add_subdirectory (src)
