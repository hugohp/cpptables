// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

// ==================================== operators < =================================

template<is_primitive_type P>
inline column<bool> operator<(const P cte, const column<P>& col)
{
  return col.gt(cte);
}

// ==================================== operators <= =================================

template<is_primitive_type P>
inline column<bool> operator<=(const P cte, const column<P>& col)
{
  return col.ge(cte);
}

// ==================================== operators == =================================

template<is_primitive_type P>
inline column<bool> operator==(const P cte, const column<P>& col)
{
  return col.eq(cte);
}

// ==================================== operators != =================================

template<is_primitive_type P>
inline column<bool> operator!=(const P cte, const column<P>& col)
{
  return col.ne(cte);
}

// ==================================== operators >= =================================

template<is_primitive_type P>
inline column<bool> operator>=(const P cte, const column<P>& col)
{
  return col.le(cte);
}

// ==================================== operators > =================================


template<is_primitive_type P>
inline column<bool> operator>(const P cte, const column<P>& col)
{
  return col.lt(cte);
}

} // namespace tables
