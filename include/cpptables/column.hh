// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <utility>
#include <chrono>
#include <iomanip>
#include "concepts.hh"
#include "types.hh"
#include "traits.hh"
#include "column_fwd.hh"
#include "column_impl_fwd.hh"

namespace tables
{

// ==================================== column_base  =================================

template<is_primitive_type P>
class column_base
{
public:

  using prim_t = P;

  using vector_t = std::vector<prim_t>;
  using column_t = column<prim_t>;
  using column_impl_t = column_impl<prim_t>;
  using ptr_to_impl_t = shared_ptr<column_impl_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_base();

  explicit column_base(std::initializer_list<prim_t> l);

  explicit column_base(const vector_t& v);

  explicit column_base(vector_t&& v);

  explicit column_base(ptr_to_impl_t&& impl);
 
  column_base(const column_base<prim_t>& other) = default;

  column_base(column_base<prim_t>&& other) = default;

  column_base& operator=(const column_base<prim_t>& other) = default;

  column_base& operator=(column_base<prim_t>&& other) = default;

  // ==================================== size/count/n_nones =================================

  std::size_t size() const;

  std::size_t count() const;

  std::size_t n_nones() const;

  // ==================================== v/impl/_v/_impl =================================

  const vector_t& v() const;

  const column_impl_t& impl() const;

  vector_t& _v();

  ptr_to_impl_t& _impl();

  // ==================================== operator[] / at =================================

  prim_t operator[](const std::size_t i ) const;

  column_t at(const vixs_t& ixs) const;

  column_t at(const column<std::size_t>& ixs) const;

  // ==================================== take/drop =================================

  column_t take(const std::size_t n) const;

  column_t drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  column_t rtake(const std::size_t n) const;

  column_t rdrop(const std::size_t n) const;

  // ==================================== append (scalar) =================================

  column_t append(const prim_t val) const;

  // ==================================== _append (scalar) =================================

  void _append(const prim_t val);

  // ==================================== _append (scalar) =================================

  void _append(const std::size_t count, const prim_t val);

  // ==================================== _append (column) =================================

  void _append(const column_t& col);

  // ==================================== append (column) =================================

  column_t append(const column_t& col) const;

  // ==================================== except =================================

  column_t except(const column_t& xs) const;

  // ==================================== apply =================================

  template<typename F>
  //requires (
  	//std::invocable<F,typename column_base<P>::prim_t> &&
  	//is_primitive_type<std::invoke_result_t<F,typename column_base<P>::prim_t>>
  //)
  auto apply(F f) const;

  // ==================================== cast_to =================================

  template<is_primitive_type P1>
  requires ( std::is_convertible_v<prim_t,P1> )
  column<P1> cast_to() const;

  // ==================================== reverse =================================

  column_t reverse() const;

  // ==================================== fills =================================

  column_t fills() const;

  // ==================================== fill_with (scalar) =================================

  column_t fill_with(const prim_t val) const;

  // ==================================== fill_with (column) =================================

  column_t fill_with(const column_t c) const;

  // ==================================== zfill =================================

  column_t zfill() const;

  // ==================================== replace =================================

  column_t replace(const column_t& c) const;

  // ==================================== differ =================================

  column<bool> differ() const;

  // ==================================== is_none =================================

  column<bool> is_none() const;

  // ==================================== distinct =================================

  column_t distinct() const;

  // ==================================== sort =================================

  column_t sort() const;

  // ==================================== rsort =================================

  column_t rsort() const;

  // ==================================== mins / maxs =================================

  column_t mins() const;

  column_t maxs() const;
 
  // ==================================== mmin / mmax =================================

  column_t mmin(const std::size_t m) const;

  column_t mmax(const std::size_t m) const;

  // ==================================== next / prev =================================

  column_t next() const;

  column_t prev() const;

  // ==================================== max/min =================================

  prim_t max() const;

  prim_t min() const;

  // ==================================== all =================================

  bool all() const;

  // ==================================== any =================================

  bool any() const;

  // ==================================== cross =================================

  template<is_primitive_type P1>
  std::pair<column<prim_t>,column<P1>> cross(const column<P1>& other) const;

  // ==================================== eq =================================

  column<bool> eq(const column_t& other) const;

  column<bool> eq(const prim_t cte) const;

  // ==================================== ne =================================

  column<bool> ne(const column_t& other) const;

  column<bool> ne(const prim_t cte) const;

  // ==================================== lt =================================

  column<bool> lt(const column_t& other) const;

  column<bool> lt(const prim_t cte) const;


  // ==================================== le =================================

  column<bool> le(const column_t& other) const;

  column<bool> le(const prim_t cte) const;

  // ==================================== gt =================================

  column<bool> gt(const column_t& other) const;

  column<bool> gt(const prim_t cte) const;

  // ==================================== ge =================================

  column<bool> ge(const column_t& other) const;

  column<bool> ge(const prim_t cte) const;

  // ==================================== in =================================

  column<bool> in(const column_t& other) const;

protected:

  ptr_to_impl_t pimpl_;
};

// ==================================== column<is_arithmetic_type>  =================================

template<is_arithmetic_type P>
class column<P> : public column_base<P>
{
public:

  using prim_t = P;

  using column_base_t = column_base<prim_t>;

  using vector_t = typename column_base_t::vector_t;
  using column_t = typename column_base_t::column_t;
  using column_impl_t = typename column_base_t::column_impl_t;
  using ptr_to_impl_t = typename column_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base<prim_t>()
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base<prim_t>(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base<prim_t>(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base<prim_t>(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base<prim_t>(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<prim_t>& other) : 
    column_base<prim_t>(other)
  {}

  column(column<prim_t>&& other) :
    column_base<prim_t>( std::forward<column_base_t>(other) )
  {}

  inline column& operator=(const column<prim_t>& other) 
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=(other));
  }

  inline column& operator=(column<prim_t>&& other)
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=( std::forward<column_base_t>(other) ));
  }

  // ==================================== deltas =================================

  column_t deltas() const;

  // ==================================== xbar =================================

  column_t xbar(const prim_t val) const;

  // ==================================== sums / msum =================================

  column_t sums() const;

  column_t msum(const std::size_t m) const;

  // ==================================== abs =================================
  
  column_t abs() const;

  // ==================================== log / log2 / log10 =================================

  column<double> log() const;

  column<double> log2() const;

  column<double> log10() const;

  // ==================================== exp =================================

  column<double> exp() const;

  // ==================================== mod =================================

  column<double> mod(const prim_t y) const;

  // ==================================== sum =================================

  prim_t sum() const;

  // ==================================== avg =================================

  prim_t avg() const;

  // ==================================== dot =================================

  template<is_arithmetic_type P1>
  requires requires (P a, P1 b) { a * b; }
  auto dot(const column<P1>& rhs) const;

  // ==================================== norm =================================

  double norm() const;
};

// ==================================== column<std::string_view>  =================================

template<>
class column<std::string_view> : public column_base<std::string_view>
{
public:

  using prim_t = std::string_view;

  using column_base_t = column_base<std::string_view>;

  using vector_t = typename column_base_t::vector_t;
  using column_t = typename column_base_t::column_t;
  using column_impl_t = typename column_base_t::column_impl_t;
  using ptr_to_impl_t = typename column_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base<prim_t>()
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base<prim_t>(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base<prim_t>(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base<prim_t>(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base<prim_t>(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<prim_t>& other) : 
    column_base<prim_t>(other)
  {}

  column(column<prim_t>&& other) :
    column_base<prim_t>( std::forward<column_base_t>(other) )
  {}

  inline column& operator=(const column<prim_t>& other) 
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=(other));
  }

  inline column& operator=(column<prim_t>&& other)
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=( std::forward<column_base_t>(other)));
  }

  column<bool> starts_with(const std::string_view sv) const;

  column<bool> starts_with(const column<std::string_view>& svs) const;

  column<bool> ends_with(const std::string_view sv) const;

  column<bool> ends_with(const column<std::string_view>& svs) const;

  column<std::string_view> substr(const std::size_t pos = 0, const std::size_t count = std::string::npos) const;

  column<std::string_view> substr(const column<std::size_t>& pos, const column<std::size_t>& count) const;

  column<std::string_view> concat(const std::string_view sv) const;

  column<std::string_view> concat(const column<std::string_view>& svs) const;

  std::string to_string(const std::string_view delim = "" ) const;
};
// ==================================== column<bool>  =================================

template<>
class column<bool> : public column_base<bool>
{
public:

  using prim_t = bool;

  using column_base_t = column_base<bool>;

  using vector_t = typename column_base_t::vector_t;
  using column_t = typename column_base_t::column_t;
  using column_impl_t = typename column_base_t::column_impl_t;
  using ptr_to_impl_t = typename column_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base<prim_t>()
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base<prim_t>(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base<prim_t>(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base<prim_t>(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base<prim_t>(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<prim_t>& other) : 
    column_base<prim_t>(other)
  {}

  column(column<prim_t>&& other) :
    column_base<prim_t>( std::forward<column_base_t>(other) )
  {}

  inline column& operator=(const column<prim_t>& other) 
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=(other));
  }

  inline column& operator=(column<prim_t>&& other)
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=( std::forward<column_base_t>(other) ));
  }

  // ==================================== neg =================================

  column<bool> neg() const;

  // ==================================== where =================================

  vixs_t where() const;
};

// ==================================== column<is_time_point>  =================================

template<is_time_point P>
class column<P> : public column_base<P>
{
public:

  using prim_t = P;

  using column_base_t = column_base<prim_t>;

  using vector_t = typename column_base_t::vector_t;
  using column_t = typename column_base_t::column_t;
  using column_impl_t = typename column_base_t::column_impl_t;
  using ptr_to_impl_t = typename column_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base<prim_t>()
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base<prim_t>(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base<prim_t>(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base<prim_t>(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base<prim_t>(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<prim_t>& other) : 
    column_base<prim_t>(other)
  {}

  column(column<prim_t>&& other) :
    column_base<prim_t>( std::forward<column_base_t>(other) )
  {}

  inline column& operator=(const column<prim_t>& other) 
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=(other));
  }

  inline column& operator=(column<prim_t>&& other)
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=( std::forward<column_base_t>(other) ));
  }

  // ==================================== time_since_epoch  =================================

  column<typename P::duration>
  time_since_epoch() const;

  // ==================================== time_point_cast  =================================

  template <
    is_duration ToDuration, 
    typename Clock = typename P::clock,
    typename Duration = typename P::duration
  >
  column<std::chrono::time_point<Clock,ToDuration>>
  time_point_cast() const;

  // ==================================== to_time_t  =================================

  column<std::time_t>
  to_time_t() const;

  // ==================================== operator+(duration)  =================================

  template<is_duration D>
  constexpr column<
    std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
    >
  >
  operator+(const column<D>& ds) const;

  // ==================================== operator-(duration)  =================================

  template<is_duration D>
  constexpr column<
    std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
    >
  >
  operator-(const column<D>& ds) const;
};

// ==================================== column<is_duration>  =================================

template<is_duration P>
class column<P> : public column_base<P>
{
public:

  using prim_t = P;

  using column_base_t = column_base<prim_t>;

  using vector_t = typename column_base_t::vector_t;
  using column_t = typename column_base_t::column_t;
  using column_impl_t = typename column_base_t::column_impl_t;
  using ptr_to_impl_t = typename column_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base<prim_t>()
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base<prim_t>(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base<prim_t>(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base<prim_t>(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base<prim_t>(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<prim_t>& other) : 
    column_base<prim_t>(other)
  {}

  column(column<prim_t>&& other) :
    column_base<prim_t>( std::forward<column_base_t>(other) )
  {}

  inline column& operator=(const column<prim_t>& other) 
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=(other));
  }

  inline column& operator=(column<prim_t>&& other)
  {
    return static_cast<column_t&>(column_base<prim_t>::operator=( std::forward<column_base_t>(other) ));
  }

  // ==================================== duration_cast  =================================

  template <is_duration ToDuration>
  column<ToDuration>
  duration_cast() const;

  // ==================================== floor  =================================

  template <is_duration ToDuration>
  column<ToDuration>
  floor() const;

  // ==================================== ceil  =================================

  template <is_duration ToDuration>
  column<ToDuration>
  ceil() const;

  // ==================================== round  =================================

  template <is_duration ToDuration>
  column<ToDuration>
  round() const;

  // ==================================== abs  =================================

  column<P>
  abs() const;
};


} // namespace tables

#include "column_impl.hh"

#include "impl/column.tcc"
#include "impl/column_impl.tcc"

#include "col_comp_col.hh"
#include "col_comp_scalar.hh"
#include "scalar_comp_col.hh"

#include "col_op_col.hh"
#include "col_op_scalar.hh"
#include "scalar_op_col.hh"

#include "til.hh"
#include "where.hh"
