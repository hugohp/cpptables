// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <tuple_ext/tuple_ext.h>
#include "concepts.hh"

namespace tables
{

// ==================================== snd_elem_at =================================

// For a given T in T0, return corresponding T' in T1 where T0 and T1 are same-sized tuples.
template<tuple_ext::is_tuple T0,tuple_ext::is_tuple T1,typename T>
requires ( std::tuple_size_v<T0> == std::tuple_size_v<T1> && in<T,T0> )
struct snd_elem_at
{
  using type = tuple_ext::elem_at_t<
    tuple_ext::elem_index_v<T,T0>,
    T1
  >;
};

template<tuple_ext::is_tuple T0,tuple_ext::is_tuple T1,typename T>
using snd_elem_at_t = typename snd_elem_at<T0,T1,T>::type;

// ==================================== many_snd_elem_at =================================

template<tuple_ext::is_tuple T0,tuple_ext::is_tuple T1,tuple_ext::is_tuple Ts>
struct many_snd_elem_at;

template<tuple_ext::is_tuple T0,tuple_ext::is_tuple T1,typename... Ts>
struct many_snd_elem_at<T0,T1,std::tuple<Ts...>>
{
  using type = std::tuple<snd_elem_at_t<T0,T1,Ts>...>;
};

template<tuple_ext::is_tuple T0,tuple_ext::is_tuple T1,tuple_ext::is_tuple Ts>
using many_snd_elem_at_t = typename many_snd_elem_at<T0,T1,Ts>::type;

} // namespace tables
