// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <iosfwd>
#include "row_fwd.hh"
#include "column.hh"
#include "mtuple.hh"
#include "concepts.hh"
#include "is_function.hh"
#include "types.hh"
#include "table_fwd.hh"
#include "ktable_fwd.hh"
#include "gtable_fwd.hh"
#include "ftable_fwd.hh"
#include "type_functions.hh"
#include "table_concepts.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== table with one or more columns =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
class table<std::pair<Ts,Ps>...>
{
public:

  using table_t = table<std::pair<Ts,Ps>...>;

  using ts_t = std::tuple<Ts...>;
  using ps_t = std::tuple<Ps...>;
  using zs_t = tuple_ext::zip_t<ts_t,ps_t>;

  using cs_t = std::tuple<column<Ps>...>;
  using zcs_t = tuple_ext::zip_t<ts_t,cs_t>;

  using cols_t = mtuple<std::pair<Ts,column<Ps>>...>;
  using row_t = row<std::pair<Ts,Ps>...>;

  // ==================================== p_of_t =================================

  template<typename T>
  requires ( in<T,ts_t> )
  using p_of_t = snd_elem_at_t<ts_t,ps_t,T>;

  // ==================================== ps_of_ts_t =================================

  template<is_tuple_unique Ss>
  requires ( all_in<Ss,ts_t> )
  using ps_of_ts_t = many_snd_elem_at_t<ts_t,ps_t,Ss>;

  // ==================================== select_t =================================

  template<is_tuple_unique Ss>
  requires ( all_in<Ss,ts_t> )
  using select_t = to_table_t<
    tuple_ext::zip_t<
      Ss,
      ps_of_ts_t<Ss>
    >
  >;

  // ==================================== add_t =================================

  template<is_pair_with_prim P>
  requires ( ! in<tuple_ext::fst_t<P>,ts_t> )
  using add_t = to_table_t<tuple_ext::concat_t<zs_t,std::tuple<P>>>;

  // ==================================== remove_t =================================

  template<is_tuple_unique Rs>
  requires ( all_in<Rs,ts_t> )
  using remove_t = select_t<tuple_ext::remove_t<ts_t,Rs>>;

  // ==================================== group_by_t =================================
 
  template<is_tuple_unique Gs>
  requires ( all_in<Gs,ts_t> )
  using group_by_t = gtable<table_t,Gs>;

  // ==================================== fby_t =================================
 
  template<is_tuple_unique Gs>
  requires ( all_in<Gs,ts_t> )
  using fby_t = ftable<table_t,Gs>;

  // ==================================== constructor / assignment  =================================

  explicit table();

  // Constructor with initializer_list
  explicit table(std::initializer_list<Ps>... ls);

  // Constructor with columns
  explicit table(const column<Ps>&... cols);

  // Constructor with columns by rvalue
  explicit table(column<Ps>&&... cols);

  // Constructor with tuple of columns
  explicit table(const std::tuple<column<Ps>...>& cs);

  // Constructor with tuple of columns by rvalue
  explicit table(std::tuple<column<Ps>...>&& cs);

  // Constructor with mtuple of Ts,column<Ps>
  explicit table(const cols_t& cols);

  // Constructor with mtuple of Ts,column<Ps> (rvalue)
  explicit table(cols_t&& cols);

  // Copy constructor
  table(const table<std::pair<Ts,Ps>...>& t);

  // Copy assignment operator
  table& operator=(const table<std::pair<Ts,Ps>...>& t);

  // Move constructor
  table(table<std::pair<Ts,Ps>...>&& t);

  // Move assignment operator
  table& operator=(table<std::pair<Ts,Ps>...>&& t);

  // ==================================== ncols =================================

  inline static constexpr std::size_t ncols()
  {
    return sizeof...(Ts);
  }

  // ==================================== nrows =================================

  std::size_t nrows() const;

  // ==================================== col =================================

  template<typename I>
  requires in<I,ts_t>
  const auto col() const;

  // ==================================== _col =================================

  template<typename I>
  requires in<I,ts_t>
  auto _col();

  // ==================================== _set_row =================================

  void _set_row(const std::size_t irow, const row_t& row);

  // ==================================== at =================================

  table at(const vixs_t& ixs) const;

  // ==================================== irow =================================

  row_t irow(const std::size_t irow) const;

  // ==================================== take/drop =================================

  constexpr table_t take(const std::size_t n) const;

  constexpr table_t drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  constexpr table_t rtake(const std::size_t n) const;

  constexpr table_t rdrop(const std::size_t n) const;

  // ==================================== append (row) =================================

  table_t append(const row_t& row) const;

  // ==================================== _append (row) =================================

  void _append(const row_t& row);

  // ==================================== _append (table) =================================

  void _append(const table_t& other);

  // ==================================== append (table)=================================

  table_t append(const table_t& other) const;

 // ==================================== select =================================

  template<typename... Ss>
  requires ( ( in<Ss,ts_t> && ... ) && is_tuple_unique<std::tuple<Ss...>> )
  constexpr auto select() const;

  // ==================================== remove =================================

  template<tuple_ext::is_tuple I>
  requires all_in<I,ts_t>
  constexpr auto remove() const;

  template<typename... Rs>
  requires ( ( in<Rs,ts_t> && ... ) && is_tuple_unique<std::tuple<Rs...>> )
  constexpr auto remove() const;

  // ==================================== add (column) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    (!in<TAs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  constexpr auto add(const column<PAs>&... cs) const;

  // ==================================== add (table) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    (!in<TAs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  constexpr auto add(const table<std::pair<TAs,PAs>...>& t) const;

  // ==================================== addu (column) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  constexpr auto addu(const column<PAs>&... cs) const;

  // ==================================== addu (table) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  constexpr auto addu(const table<std::pair<TAs,PAs>...>& t) const;

  // ==================================== update (with constants) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    (in<TUs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TUs...>> &&
    ( sizeof...(TUs) == sizeof...(PUs) )
  )
  auto update(const PUs... ctes) const;

  // ==================================== update (with columns) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    (in<TUs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TUs...>> &&
    ( sizeof...(TUs) == sizeof...(PUs) )
  )
  auto update(const column<PUs>&... cs) const;

  // ==================================== update (with table) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    (in<TUs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TUs...>> &&
    ( sizeof...(TUs) == sizeof...(PUs) )
  )
  auto update(const table<std::pair<TUs,PUs>...>& t) const;

  // ==================================== updatef (with columns) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    (in<TUs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TUs...>> &&
    ( sizeof...(TUs) == sizeof...(PUs) )
  )
  auto updatef(const column<PUs>&... cs) const;

  // ==================================== updatef (with table) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    (in<TUs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TUs...>> &&
    ( sizeof...(TUs) == sizeof...(PUs) )
  )
  auto updatef(const table<std::pair<TUs,PUs>...>& t) const;

  // ==================================== update_at (with constants) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    ( (in<TUs,ts_t>) && ...) &&
    ( sizeof...(TUs) == sizeof...(PUs) ) &&
    (
      std::is_same_v<                    // TUs/PUs matches same pair in ts_t,ps_t
        snd_elem_at_t<ts_t,ps_t,TUs>,
        PUs
       > && ...
    )
  )
  table_t update_at(const vixs_t& ixs, const PUs... ctes) const;

  // ==================================== update_at (with columns) =================================

  template<typename... TUs,is_primitive_type... PUs>
  requires (
    ( (in<TUs,ts_t>) && ...) &&
    ( sizeof...(TUs) == sizeof...(PUs) ) &&
    (
      std::is_same_v<                    // TUs/PUs matches same pair in ts_t,ps_t
        snd_elem_at_t<ts_t,ps_t,TUs>,
        PUs
       > && ...
    )
  )
  table_t update_at(const vixs_t& ixs, const column<PUs>&... cs) const;

  // ==================================== fupdate =================================

  template<typename... Ss,typename... Fs>
  requires (
    (sizeof...(Ss) == sizeof...(Fs)) &&
    all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
    ( is_col_to_col_f<Fs,column<snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss>>> && ... )
  )
  table_t fupdate(const Fs... fs) const;

  // ==================================== key_by =================================

  template<typename... Ks>
  requires ( ( in<Ks,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Ks...>> )
  ktable<table_t,std::tuple<Ks...>>
  key_by() const;

  // ==================================== group_by =================================

  template<typename... Gs>
  requires ( ( in<Gs,ts_t> && ... ) && is_tuple_unique<std::tuple<Gs...>> )
  gtable<table_t,std::tuple<Gs...>>
  group_by() const;

  // ==================================== fby =================================

  template<typename... Gs>
  requires ( ( in<Gs,ts_t> && ... ) && is_tuple_unique<std::tuple<Gs...>> )
  ftable<table_t,std::tuple<Gs...>>
  fby() const;

  // ==================================== indices_by =================================

  template<typename... Ss>
  requires ( (in<Ss,ts_t> && ... ) && is_tuple_unique<std::tuple<Ss...>>)
  auto indices_by() const;

  // ==================================== lj =================================

  template<is_ktable KT>
  requires ( is_kjoinable<table_t,KT> )
  auto lj(const KT& kt) const;

  // ==================================== ljf =================================

  template<is_ktable KT>
  requires ( is_kjoinable<table_t,KT> )
  auto ljf(const KT& kt) const;

  // ==================================== ij =================================

  template<is_ktable KT>
  requires ( is_kjoinable<table_t,KT> )
  auto ij(const KT& kt) const;

  // ==================================== ijf =================================

  template<is_ktable KT>
  requires ( is_kjoinable<table_t,KT> )
  auto ijf(const KT& kt) const;

  // ==================================== uj =================================

  template<is_table T>
  requires ( is_joinable<table_t,T> )
  auto uj(const T& t1) const;

  // ==================================== aj =================================

  template<typename Tm,is_ktable KT>
  requires ( 
    in<Tm,ts_t> && 
    !in<Tm,typename KT::ks_t> && 
    is_kjoinable<table_t,KT> 
  )
  auto aj(const KT& kt) const;

  // ==================================== aj0 =================================

  template<typename Tm,is_ktable KT>
  requires ( 
    in<Tm,ts_t> && 
    !in<Tm,typename KT::ks_t> && 
    is_kjoinable<table_t,KT> 
  )
  auto aj0(const KT& kt) const;


  // ==================================== apply =================================

  template<typename... Ss,typename... Fs>
  requires (
    (sizeof...(Ss) == sizeof...(Fs)) &&
    all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
    ( is_prim_to_prim_f<Fs,snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss>> && ... )
  )
  table_t apply(Fs... fs) const;

  // ==================================== apply_at =================================

  template<typename... Ss,typename... Fs>
  requires (
    (sizeof...(Ss) == sizeof...(Fs)) &&
    all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
    ( is_prim_to_prim_f<Fs,snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss>> && ... )
  )
  table_t apply_at(const vixs_t& ixs, Fs... fs) const;

  // ==================================== except =================================

  table_t except(const table_t& xt) const;

  // ==================================== reverse =================================

  constexpr table_t reverse() const;

  // ==================================== sort =================================

  table_t sort() const;

  // ==================================== fills =================================

  table_t fills() const;

  // ==================================== fill_with =================================

  table_t fill_with(const row_t& vals) const;

  // ==================================== zfill =================================

  table_t zfill() const;

  // ==================================== deltas =================================

  table_t deltas() const;

  // ==================================== differ =================================

  column<bool> differ() const;

  // ==================================== distinct =================================

  table_t distinct() const;

  // ==================================== sort_by =================================

  template<typename... Ss>
  requires ( (in<Ss,ts_t> && ... ) && is_tuple_unique<std::tuple<Ss...>>)
  table_t sort_by() const;

  // ==================================== rsort_by =================================

  template<typename... Ss>
  requires (
    (in<Ss,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<Ss...>>
  )
  table_t rsort_by() const;

  // ==================================== next / prev =================================

  table_t next() const;

  table_t prev() const;

  // ==================================== abs =================================

  table_t abs() const;

  // ==================================== log / log2 / log10 =================================

  auto log() const;

  auto log2() const;

  auto log10() const;

  // ==================================== exp =================================

  auto exp() const;

  // ==================================== mod =================================

  auto mod(const row_t& vals) const;

  // ==================================== max/min =================================

  row_t max() const;

  row_t min() const;

  // ==================================== sum =================================

  row_t sum() const;

  // ==================================== avg =================================

  row_t avg() const;

  // ==================================== all =================================

  auto all() const;

  // ==================================== any =================================

  auto any() const;

  // ==================================== neg =================================

  auto neg() const;

  // ==================================== eq =================================

  auto eq(const table_t& rhs) const;

  // ==================================== ne =================================

  auto ne(const table_t& rhs) const;

  // ==================================== lt =================================

  auto lt(const table_t& rhs) const;

  // ==================================== le =================================

  auto le(const table_t& rhs) const;

  // ==================================== gt =================================

  auto gt(const table_t& rhs) const;

  // ==================================== ge =================================

  auto ge(const table_t& rhs) const;

  // ==================================== ne =================================

  // ==================================== repeated function (tuple version) =================================

  template<tuple_ext::is_tuple Ks>
  requires ( all_in<Ks,std::tuple<Ts...>> && is_tuple_unique<Ks> )
  ktable<table_t,Ks>
  key_by() const;

  template<tuple_ext::is_tuple Gs>
  requires ( all_in<Gs,std::tuple<Ts...>> && is_tuple_unique<Gs> )
  gtable<table_t,Gs>
  group_by() const;

  template<tuple_ext::is_tuple Gs>
  requires ( all_in<Gs,std::tuple<Ts...>> && is_tuple_unique<Gs> )
  ftable<table_t,Gs>
  fby() const;

  template<tuple_ext::is_tuple Ss>
  requires ( all_in<Ss,std::tuple<Ts...>> && is_tuple_unique<Ss> )
  auto select() const;

  template<tuple_ext::is_tuple Ss>
  requires ( all_in<Ss,ts_t> && is_tuple_unique<Ss>)
  table_t sort_by() const;

  template<tuple_ext::is_tuple Ss>
  requires ( all_in<Ss,ts_t> && is_tuple_unique<Ss> )
  auto indices_by() const;

  // ==================================== workspace =================================

  vixs_t& _workspace() const;

private:
  cols_t cols_;
  mutable vixs_t workspace_;
  std::size_t nrows_;
};

// ==================================== table with no columns =================================

template <>
class table<>
{
public:

  using table_t = table<>;

  // ==================================== add_t =================================

  template<is_pair_with_prim P>
  using add_t = table<P>;

  // ==================================== constructor / assignment  =================================

  explicit constexpr table() {}

  // Copy constructor
  inline
  table(const table<>& t)
  {}

  // Copy assignment operator
  inline
  table& operator=(const table<>& t)
  {
    return *this;
  }

  // Move constructor
  table(table<>&& t)
  {}

  // Move assignment operator
  table& operator=(table<>&& t)
  {
    return *this;
  }

  // ==================================== ncols =================================

  inline constexpr std::size_t ncols()
  {
    return 0;
  }

  // ==================================== nrows =================================

  constexpr inline std::size_t nrows()
  {
    return 0;
  }

  constexpr table_t at(const vixs_t& ixs) const
  {
    return table_t();
  }

  // ==================================== add (column) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  inline
  constexpr table<std::pair<TAs,PAs>...> 
  add(const column<PAs>&... cs) const
  {
    return table<std::pair<TAs,PAs>...>(cs...);
  }

  // ==================================== add (table) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  inline
  constexpr table<std::pair<TAs,PAs>...> 
  add(const table<std::pair<TAs,PAs>...>& t) const
  {
    return t;
  }

  // ==================================== addu (column) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  inline
  constexpr table<std::pair<TAs,PAs>...> 
  addu(const column<PAs>&... cs) const
  {
    return add(cs...);
  }

  // ==================================== addu (table) =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    is_tuple_unique<std::tuple<TAs...>> &&
    ( sizeof...(TAs) == sizeof...(PAs) )
  )
  inline
  constexpr table<std::pair<TAs,PAs>...> 
  addu(const table<std::pair<TAs,PAs>...>& t) const
  {
    return add(t);
  }

};

// ==================================== operator<< =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
std::ostream& operator<< (std::ostream& os, const table<std::pair<Ts,Ps>...>& t);

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
std::ostream& operator<< (std::ostream& os, const table<>& t);

// ==================================== as_table_t =================================

namespace detail 
{
  template<is_tuple_unique Ts,tuple_ext::is_tuple Ps> struct as_table_s;

  template<typename... Ts,is_primitive_type... Ps>
  struct as_table_s<std::tuple<Ts...>,std::tuple<Ps...>>
  {
    using type = table<std::pair<Ts,Ps>...>;
  };
} // namespace detail

template<typename Ts,typename Ps>
using as_table_t = typename detail::as_table_s<Ts,Ps>::type;

} // namespace tables

#include "table_comp_table.hh"
#include "impl/table.tcc"
