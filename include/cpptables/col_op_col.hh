// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

namespace detail
{

  template<is_primitive_type P,typename F>
  requires ( 
    std::invocable<F,P,P> && 
    std::is_same_v<std::invoke_result_t<F,P,P>,P>
  )
  inline 
  column<P>
  col_op_col(const column<P>& lhs, const column<P>& rhs, const F f)
  {
    assert( rhs.size() == lhs.size() );
    constexpr P none = prim_traits<P>::none;
  
    const std::size_t n = lhs.size();
  
    using vector_t = typename column_impl<P>::vector_t;
    vector_t dst(n);
  
    for ( std::size_t i = 0 ; i < n ; ++i )
    {
      const P lhs_val = lhs.v()[i];
      const P rhs_val = rhs.v()[i];

      const bool is_none = prim_traits<P>::is_none(lhs_val) || prim_traits<P>::is_none(rhs_val);
      dst[i] = is_none ? none : f(lhs_val,rhs_val);
    }
    return column<P>(std::move(dst));
  }
} // namespace detail

template<is_arithmetic_type P>
inline auto operator+(const column<P>& lhs, const column<P>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const auto f = [](const P v0, const P v1) { return (v0 + v1); };
  return detail::col_op_col(lhs,rhs,f);
}

// ==================================== operator- =================================

template<is_arithmetic_type P>
inline auto operator-(const column<P>& lhs, const column<P>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const auto f = [](const P v0, const P v1) { return (v0 - v1); };
  return detail::col_op_col(lhs,rhs,f);
}

// ==================================== operator* =================================

template<is_arithmetic_type P>
inline auto operator*(const column<P>& lhs, const column<P>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const auto f = [](const P v0, const P v1) { return (v0 * v1); };
  return detail::col_op_col(lhs,rhs,f);
}

// ==================================== operator/ =================================

template<is_arithmetic_type P>
inline auto operator/(const column<P>& lhs, const column<P>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const auto f = [](const P v0, const P v1) { return (v0 / v1); };
  return detail::col_op_col(lhs,rhs,f);
}

// ==================================== operator&& =================================

inline column<bool> operator&&(const column<bool>& lhs, const column<bool>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const std::size_t n = lhs.size();

  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const bool lhs_val = lhs.v()[i];
    const bool rhs_val = rhs.v()[i];

    dst[i] = (lhs_val && rhs_val);
  }
  return column<bool>(std::move(dst));
}

// ==================================== operator|| =================================

inline column<bool> operator||(const column<bool>& lhs, const column<bool>& rhs)
{
  if (rhs.size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const std::size_t n = lhs.size();

  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const bool lhs_val = lhs.v()[i];
    const bool rhs_val = rhs.v()[i];

    dst[i] = (lhs_val || rhs_val);
  }
  return column<bool>(std::move(dst));
}

} // namespace tables
