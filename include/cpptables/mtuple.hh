// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include "mtuple_fwd.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== mtuple =================================

template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
class mtuple<std::pair<Ks,Vs>...>
{
public:

  using mtuple_t = mtuple<std::pair<Ks,Vs>...>;
  using ks_t = std::tuple<Ks...>;
  using vs_t = std::tuple<Vs...>;
  using to_tuple_t = std::tuple<std::pair<Ks,Vs>...>;

  explicit mtuple();

  // Constructor with value-types
  explicit mtuple(const Vs... vs);

  // Constructor with tuple-of-values
  explicit mtuple(const vs_t& vs);

  // Constructor with tuple-of-values (by rvalue)
  explicit mtuple(vs_t&& vs);

  // Copy constructor
  mtuple(const mtuple&) = default;

  // Copy assignment operator
  mtuple& operator=(const mtuple&) = default;

  // Move constructor
  mtuple(mtuple&& other) = default;

  // Move assignment operator
  mtuple& operator=(mtuple&& other) = default;

/*
  template<is_mtuple M>
  requires ( std::tuple_size_v<tuple_ext::inter_t<ks_t,typename M::ks_t>> == 0 )
  auto insert(const M& other) const;

  template<typename K>
  requires in<K,ks_t>
  constexpr auto remove() const;
*/

  template<typename K>
  requires in<K,ks_t>
  tuple_ext::elem_at_t<tuple_ext::elem_index_v<K,ks_t>,vs_t>
  get() const;

  template<typename K>
  requires in<K,ks_t>
  tuple_ext::elem_at_t<tuple_ext::elem_index_v<K,ks_t>,vs_t>&
  _get();

  inline static constexpr std::size_t size()
  {
    return sizeof...(Ks);
  }

  inline const vs_t& vs() const
  {
    return vs_;
  }

private:
  friend auto operator<=>(const mtuple_t& lhs, const mtuple_t& rhs) = default;

  vs_t vs_;
};

// ==================================== to_mtuple =================================

} // namespace tables

#include "impl/mtuple.tcc"
