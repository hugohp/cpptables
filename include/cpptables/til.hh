// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include "column.hh"

namespace tables
{

inline column<std::size_t> til(const std::size_t n)
{
  std::vector<std::size_t> ixs(n);
  std::iota(ixs.begin(), ixs.end(), 0);

  return column<std::size_t>(std::move(ixs));
}

} // namespace tables
