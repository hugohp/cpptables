// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <map>
#include <iosfwd>
#include "column.hh"
#include "concepts.hh"
#include "types.hh"
#include "row_fwd.hh"
#include "ftable_fwd.hh"
#include "table_fwd.hh"
#include "type_functions.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
struct ftable
{
public:

  using ts_t = typename T::ts_t;
  using table_t = T;

  using gs_t = Gs;
  using pgs_t = typename table_t::ps_of_ts_t<gs_t>;
  using ftable_t = ftable<table_t,gs_t>;

  using g_row_t = to_row_t<tuple_ext::zip_t<gs_t,pgs_t>>;

  using map_t = std::map<g_row_t, vixs_t>;

  // Construct from table
  explicit ftable(const table_t& t);

  // Copy constructor
  ftable(const ftable_t&);

  // Copy assignment operator
  ftable& operator=(const ftable_t&);

  // Move constructor
  ftable(ftable_t&& other);

  // Move assignment operator
  ftable& operator=(ftable_t&& other);

  // ==================================== n_groups =================================
   
  std::size_t n_groups() const;

  // ==================================== t =================================
   
  table_t t() const;

  // ==================================== m =================================

  const map_t& m() const;

  // ==================================== agg =================================

  template<typename... Ss,typename... Fs>
  requires (
    (sizeof...(Ss) == sizeof...(Fs))
    && ( all_in<std::tuple<Ss...>,tuple_ext::remove_t<Gs,typename T::ts_t>> )
    && ( is_col_to_prim_f<Fs,typename T::p_of_t<Ss>> && ... )
  )
  auto agg(const Fs... fs) const;

  // ==================================== fupdate =================================

  template<typename... Ss,typename... Fs>
  requires (
    (sizeof...(Ss) == sizeof...(Fs))
    && ( all_in<std::tuple<Ss...>,tuple_ext::remove_t<Gs,typename T::ts_t>> )
    && ( is_col_to_col_f<Fs,typename T::p_of_t<Ss>> && ... )
  )
  auto fupdate(const Fs... fs) const;

private:
  table_t t_;
  map_t m_;
};

} // namespace tables

#include "impl/ftable.tcc"
