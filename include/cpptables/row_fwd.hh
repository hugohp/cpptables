// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <tuple>
#include "concepts.hh"
#include "types.hh"

namespace tables
{

// ==================================== row =================================

template <typename... Ts>
requires ( is_key_value_pairs<Ts...> )
class row;

// ==================================== is_row =================================

namespace detail
{

template<typename T>
struct is_row_s : std::false_type {};

template <typename... Ts,is_primitive_type... Ps>
struct is_row_s<row<std::pair<Ts,Ps>...>> : std::true_type {};

} // namespace detail

template<typename T>
concept is_row = detail::is_row_s<T>::value;

// ==================================== to_row_t =================================

namespace detail
{

template <tuple_ext::is_tuple T>
struct to_row_s;

template <typename... Ts,typename... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
struct to_row_s<std::tuple<std::pair<Ts,Ps>...>>
{
  using type = row<std::pair<Ts,Ps>...>;
};

} // namespace detail

template<tuple_ext::is_tuple T>
using to_row_t = detail::to_row_s<T>::type;


} // namespace tables
