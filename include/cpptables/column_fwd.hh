// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <tuple>
#include "concepts.hh"
#include "types.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== column =================================

template <is_primitive_type P>
class column;

// ==================================== is_column =================================

namespace detail
{

template<typename T>
struct is_column_s : std::false_type {};

template <is_primitive_type P>
struct is_column_s<column<P>> : std::true_type {};

} // namespace detail

template<typename T>
concept is_column = detail::is_column_s<T>::value;

} // namespace tables
