// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include "concepts.hh"
#include "types.hh"
#include "mtuple.hh"
#include "row_fwd.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

template <typename... Ts, is_primitive_type... Ps>
class row<std::pair<Ts,Ps>...>
{
public:

  using ts_t = std::tuple<Ts...>;
  using ps_t = std::tuple<Ps...>;
  using zs_t = tuple_ext::zip_t<ts_t,ps_t>;
  using row_t = row<std::pair<Ts,Ps>...>;

  using mtuple_t = mtuple<std::pair<Ts,Ps>...>;

  explicit row();

  // Constructor with Ps
  row(const Ps&... ps);

  // Constructor with tuple-of-primitives
  row(const ps_t& ps);

  // Constructor with tuple-of-primitives (by rvalue)
  row(ps_t&& ps);

  // Copy constructor
  row(const row&) = default;

  // Copy assignment operator
  row& operator=(const row&) = default;

  // Move constructor
  row(row&& other) = default;

  // Move assignment operator
  row& operator=(row&& other) = default;

  template<typename T>
  requires in<T,ts_t>
  auto
  col() const;

  inline static constexpr std::size_t ncols()
  {
    return sizeof...(Ts);
  }

  // ==================================== add =================================

  template<typename... TAs,is_primitive_type... PAs>
  requires (
    ( sizeof...(TAs) == sizeof...(PAs) ) &&
    (!in<TAs,ts_t> && ... ) &&
    is_tuple_unique<std::tuple<TAs...>>
  )
  auto add(const row<std::pair<TAs,PAs>...>& other) const;

  // ==================================== all =================================

  bool all() const;

  // ==================================== any =================================

  bool any() const;

  // ==================================== neg =================================

  auto neg() const;

  // ==================================== eq =================================

  bool eq(const row_t& rhs) const;

  // ==================================== ne =================================

  bool ne(const row_t& rhs) const;

  // ==================================== lt =================================

  bool lt(const row_t& rhs) const;

  // ==================================== le =================================

  bool le(const row_t& rhs) const;

  // ==================================== ge =================================

  bool ge(const row_t& rhs) const;

  // ==================================== gt =================================

  bool gt(const row_t& rhs) const;

private:

  friend auto operator<=>(
    const row<std::pair<Ts,Ps>...>& lhs,
    const row<std::pair<Ts,Ps>...>& rhs
  ) = default;

  mtuple_t cols_;
};

// ==================================== operator<< =================================

template <is_row R>
std::ostream& operator<< (std::ostream&, const R&);

// ==================================== as_row_t =================================

namespace detail 
{
  template<is_tuple_unique Ts,tuple_ext::is_tuple Ps> struct as_row_s;

  template<typename... Ts,is_primitive_type... Ps>
  struct as_row_s<std::tuple<Ts...>,std::tuple<Ps...>>
  {
    using type = row<std::pair<Ts,Ps>...>;
  };
} // namespace detail

template<typename Ts,typename Ps>
using as_row_t = typename detail::as_row_s<Ts,Ps>::type;


} // namespace tables

#include "impl/row.tcc"
