// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <string_view>
#include <chrono>
#include <tuple_ext/tuple_ext.h>

// ==================================== is_time_point =================================

namespace tables
{

namespace detail {

template<typename T>
struct is_time_point_s : public std::false_type {};

template<typename Clock,typename Duration>
struct is_time_point_s<std::chrono::time_point<Clock,Duration>> : public std::true_type {};

} // namespace detail

template<typename T>
concept is_time_point = detail::is_time_point_s<T>::value;

// ==================================== is_duration =================================

namespace detail {

template<typename T>
struct is_duration_s : public std::false_type {};

template<typename Rep,typename Period>
struct is_duration_s<std::chrono::duration<Rep,Period>> : public std::true_type {};

} // namespace detail

template<typename T>
concept is_duration = detail::is_duration_s<T>::value;


// ==================================== is_primitive_type =================================

template<typename T>
concept is_primitive_type = (
     std::is_same_v<T,double>
  || std::is_same_v<T,int>
  || std::is_same_v<T,unsigned int>
  || std::is_same_v<T,long>
  || std::is_same_v<T,unsigned long>
  || std::is_same_v<T,long long>
  || std::is_same_v<T,unsigned long long>
  || std::is_same_v<T,char>
  || std::is_same_v<T,bool>
  || std::is_same_v<T,std::string_view>
  || std::is_same_v<T,std::time_t>
  || is_time_point<T> 
  || is_duration<T>
 );

// ==================================== is_arithmetic_type =================================

template<typename T>
concept is_arithmetic_type = is_primitive_type<T> && ( std::is_arithmetic_v<T> || std::is_same_v<T,std::time_t>);

// ==================================== // =================================

// true if T is in Tp
template<typename T,typename Tp>
concept in = tuple_ext::has_type_v<T,Tp>;

// true if T is not in Tp
template<typename T,typename Tp>
concept not_in = !tuple_ext::has_type_v<T,Tp>;

// true if all types in Tp1 are in Tp2
template<typename Tp1,typename Tp2>
concept all_in = std::is_same_v<tuple_ext::inter_t<Tp1,Tp2>,Tp1>;

// true if any types in Tp1 is in Tp2
template<typename Tp1,typename Tp2>
concept any_in = ! std::is_same_v<tuple_ext::inter_t<Tp1,Tp2>,std::tuple<>>;

// ==================================== is_tuple_of_prim =================================

template<typename>
struct is_tuple_of_prim_s : public std::false_type {};

template<is_primitive_type... Ps>
struct is_tuple_of_prim_s<std::tuple<Ps...>> : public std::true_type {};

template <typename T>
concept is_tuple_of_prim = is_tuple_of_prim_s<T>::value;

// ==================================== are_tuples_same_size =================================

template<typename T0,typename T1>
concept are_tuples_same_size = (std::tuple_size_v<T0> == std::tuple_size_v<T1>);

// ==================================== is_tuple_unique =================================

template<typename T>
concept is_tuple_unique = (std::is_same_v<tuple_ext::unique_t<T>,T>);

// ==================================== is_pair_with_prim =================================

template<typename>
struct is_pair_with_prim_s : public std::false_type {};

template<typename T,is_primitive_type P>
struct is_pair_with_prim_s<std::pair<T,P>> : public std::true_type {};

template<typename T>
concept is_pair_with_prim = is_pair_with_prim_s<T>::value;

// ==================================== is_key_value_pairs =================================

template<typename...>
struct is_key_value_pairs_s : public std::false_type {};

template<typename... Ts,is_primitive_type... Ps>
requires is_tuple_unique<std::tuple<Ts...>>
struct is_key_value_pairs_s<std::pair<Ts,Ps>...> : public std::true_type {};

template<typename... Ts>
concept is_key_value_pairs = is_key_value_pairs_s<Ts...>::value;

// ==================================== is_associated_map_of_types =================================

namespace detail {

template<typename T>
struct is_associated_map_of_types_s : public std::false_type {};

template<typename... Ks,typename... Vs>
requires is_tuple_unique<std::tuple<Ks...>>
struct is_associated_map_of_types_s<std::tuple<std::pair<Ks,Vs>...>> : public std::true_type {};

} // namespace detail

template<typename T>
concept is_associated_map_of_types = detail::is_associated_map_of_types_s<T>::value;

} // namespace tables
