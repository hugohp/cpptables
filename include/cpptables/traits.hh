// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cmath>
#include <chrono>
#include <limits>
#include <string_view>
#include "types.hh"
#include "concepts.hh"

namespace tables
{

template<is_primitive_type P> struct prim_traits;


// ==================================== prim_traits (double) =================================

template<>
struct prim_traits<double>
{
  static constexpr double none = std::numeric_limits<double>::max();
  static constexpr double zero = double();

  static constexpr bool is_none(double v) noexcept
  {
    return (v == none);
  }
 
  static double from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stod(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (int) =================================

template<>
struct prim_traits<int>
{
  static constexpr int none = std::numeric_limits<int>::max();
  static constexpr int zero = int();

  static constexpr bool is_none(int v) noexcept
  {
    return (v == none);
  }

  static int from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stoi(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};


// ==================================== prim_traits (unsigned int) =================================

template<>
struct prim_traits<unsigned int>
{
  static constexpr unsigned int none = std::numeric_limits<int>::max();
  static constexpr unsigned int zero = 0U;

  static constexpr bool is_none(unsigned int v) noexcept
  {
    return (v == none);
  }

  static unsigned int from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stoul(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (long) =================================

template<>
struct prim_traits<long>
{
  static constexpr long none = std::numeric_limits<int>::max();
  static constexpr long zero = long();

  static constexpr bool is_none(long v) noexcept
  {
    return (v == none);
  }

  static long from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stol(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (unsigned long) =================================

template<>
struct prim_traits<unsigned long>
{
  static constexpr unsigned long none = std::numeric_limits<unsigned long>::max();
  static constexpr unsigned long zero = 0;

  static constexpr bool is_none(unsigned long v) noexcept
  {
    return (v == none);
  }

  static unsigned long from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stoul(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (long long) =================================

template<>
struct prim_traits<long long>
{
  static constexpr long long none = std::numeric_limits<long long>::max();
  static constexpr long long zero = 0UL;

  static constexpr bool is_none(long long v) noexcept
  {
    return (v == none);
  }

  static long long from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stoll(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (unsigned long long) =================================

template<>
struct prim_traits<unsigned long long>
{
  static constexpr unsigned long long none = std::numeric_limits<unsigned long long>::max();
  static constexpr unsigned long long  zero = 0ULL;

  static constexpr bool is_none(unsigned long long v) noexcept
  {
    return (v == none);
  }

  static unsigned long long from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      str = sv;
      return std::stoull(str);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== prim_traits (char) =================================

template<>
struct prim_traits<char>
{
  static constexpr char none = std::numeric_limits<char>::max();
  static constexpr char zero = char();

  static constexpr bool is_none(char v) noexcept
  {
    return (v == none);
  }
 
  static char from_string(const std::string_view sv)
  {
    return (sv.size() > 0) ? sv[0] : none;
  }
};

// ==================================== prim_traits (bool) =================================

template<>
struct prim_traits<bool>
{
  static constexpr bool none = false;
  static constexpr bool zero = false;

  static constexpr bool is_none(bool) noexcept
  {
    return false;
  }

  static bool from_string(const std::string_view sv)
  {
    static constexpr char const* const true_str = "true";
    static constexpr char const* const trueC_str = "TRUE";

    if ( sv.size() == 4)
    {
      for ( std::size_t i = 0 ; i < 4 ; ++i)
      {
        if ((sv[i] != true_str[i]) && (sv[i] != trueC_str[i])) return false;
      }
      return true;
    }
    return false;
  }
};

// ==================================== prim_traits (string_view) =================================

template<>
struct prim_traits<std::string_view>
{
  static constexpr std::string_view none = "";
  static constexpr std::string_view zero = std::string_view();

  static constexpr bool is_none(std::string_view v) noexcept
  {
    return v.empty();
  }

  static std::string_view from_string(const std::string_view sv)
  {
    return sv;
  }
};

// ==================================== prim_traits (time_point) =================================

template<is_time_point T>
struct prim_traits<T>
{
  static constexpr T none = T::max();
  static constexpr T zero = T();

  static constexpr bool is_none(T v) noexcept
  {
    return (v == none);
  }

  static T from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      using duration = typename T::duration;

      str = sv;
      const typename duration::rep v = std::stol(str);

      return T(duration(v));
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};


// ==================================== prim_traits (duration) =================================

template<is_duration T>
struct prim_traits<T>
{
  static constexpr T none = T::max();
  static constexpr T zero = T();

  static constexpr bool is_none(T v) noexcept
  {
    return (v == none);
  }

  static T from_string(const std::string_view sv)
  {
    try
    {
      static std::string str;
      using duration = T;

      str = sv;

      const typename duration::rep v = std::stol(str);

      return duration(v);
    }
    catch ( const std::invalid_argument&)
    {
      return none;
    }
    catch ( const std::out_of_range&)
    {
      return none;
    }
  }
};

// ==================================== type_traits =================================

template<typename T>
struct type_traits
{
  static constexpr const char* const name = "<no name>";
};

template<typename T>
requires ( requires { std::string_view(T::name); } )
struct type_traits<T>
{
  static constexpr std::string_view name = T::name;
};

} // namespace tables
