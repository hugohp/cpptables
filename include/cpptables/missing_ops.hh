// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#include <chrono>

namespace std
{

template< class CharT, class Traits >
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, const std::chrono::year& y)
{
  return (os << int(y));
}

template< class CharT, class Traits >
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, const std::chrono::month& m)
{
  switch ((unsigned)m)
  {
    case (unsigned)std::chrono::January:
      return os << "January";
    case (unsigned)std::chrono::February:
      return os << "February";
    case (unsigned)std::chrono::March:
      return os << "March";
    case (unsigned)std::chrono::April:
      return os << "April";
    case (unsigned)std::chrono::May:
      return os << "May";
    case (unsigned)std::chrono::June:
      return os << "June";
    case (unsigned)std::chrono::July:
      return os << "July";
    case (unsigned)std::chrono::August:
      return os << "August";
    case (unsigned)std::chrono::September:
      return os << "September";
    case (unsigned)std::chrono::October:
      return os << "October";
    case (unsigned)std::chrono::November:
      return os << "November";
    case (unsigned)std::chrono::December:
      return os << "December";
    default:
      return os << "<Undefined>";
  }
}

template< class CharT, class Traits >
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, const std::chrono::day& d)
{
  return (os << unsigned(d));
}
} // namespace std
