// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

namespace detail
{

  template<is_primitive_type P,typename F>
  requires ( 
    std::invocable<F,P,P> && 
    std::is_same_v<std::invoke_result_t<F,P,P>,P>
  )
  inline 
  column<P>
  scalar_op_col(const P cte, const column<P>& col, const F f)
  {
    constexpr P none = prim_traits<P>::none;
  
    const std::size_t n = col.size();
  
    using vector_t = typename column_impl<P>::vector_t;
    vector_t dst(n);
  
    if ( prim_traits<P>::is_none(cte) )
    {
      dst.resize(n, none);
    }
    else
    {
      dst.resize(n);
          
      for ( std::size_t i = 0 ; i < n ; ++i )
      {
        const P vi = col.v()[i];
          
        const bool is_vi_none = prim_traits<P>::is_none(vi);
        dst[i] = is_vi_none ? none : f(cte,vi);
      }
    }
    return column<P>(std::move(dst));
  }
} // namespace detail

template<is_arithmetic_type P>
inline auto operator+(const P cte, const column<P>& col)
{
  const auto f = [](const P v0, const P v1) { return (v0 + v1); };
  return detail::scalar_op_col(cte, col, f);
}

// ==================================== operator- =================================

template<is_arithmetic_type P>
inline auto operator-(const P cte, const column<P>& col)
{
  const auto f = [](const P v0, const P v1) { return (v0 - v1); };
  return detail::scalar_op_col(cte, col, f);
}

// ==================================== operator* =================================

template<is_arithmetic_type P>
inline auto operator*(const P cte, const column<P>& col)
{
  const auto f = [](const P v0, const P v1) { return (v0 * v1); };
  return detail::scalar_op_col(cte, col, f);
}

// ==================================== operator/ =================================

template<is_arithmetic_type P>
inline auto operator/(const P cte, const column<P>& col)
{
  const auto f = [](const P v0, const P v1) { return (v0 / v1); };
  return detail::scalar_op_col(cte, col, f);
}

// ==================================== operator&& =================================

inline column<bool> operator&&(const bool cte, const column<bool>& col)
{
  const std::size_t n = col.size();

  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const bool val = col.v()[i];

    dst[i] = (val && cte);
  }
  return column<bool>(std::move(dst));
}

// ==================================== operator|| =================================

inline column<bool> operator||(const bool cte, const column<bool>& col)
{
  const std::size_t n = col.size();

  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const bool val = col.v()[i];

    dst[i] = (val || cte);
  }
  return column<bool>(std::move(dst));
}


} // namespace tables
