// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <vector>
#include <tuple_ext/tuple_ext.h>
#include "concepts.hh"
#include "column_fwd.hh"
#include "type_functions.hh"
#include "types.hh"

namespace tables
{

// ==================================== is_col_to_col_f =================================

// True if F is a function from C to C where C is a column.
template<typename F,typename P>
concept is_col_to_col_f = 
  is_primitive_type<P>
  && std::is_invocable_v<F,column<P>>
  && is_column<std::invoke_result_t<F,column<P>>>;

// ==================================== is_col_to_bcol_f =================================

// True if F is a function from C to column<bool> where C is a column
template<typename F,typename C>
concept is_col_to_bcol_f = 
  is_column<C> &&
  std::is_invocable_v<F,C> && 
  std::is_same_v<std::invoke_result_t<F,C>,column<bool>>;

// ==================================== is_col_to_prim_f =================================

// True if F is a function from column<P> to a primitive-type
template<typename F,typename P>
concept is_col_to_prim_f = 
  is_primitive_type<P>
  && std::is_invocable_v<F,column<P>>
  && is_primitive_type<std::invoke_result_t<F,column<P>>>;

// ==================================== is_prim_to_prim_f =================================

// True if F is a function from P to a primitive-type
template<typename F,typename P>
concept is_prim_to_prim_f = 
  is_primitive_type<P>
  && std::is_invocable_v<F,P>
  && is_primitive_type<std::invoke_result_t<F,P>>;

} // namespace tables
