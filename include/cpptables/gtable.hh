// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <map>
#include <tuple_ext/tuple_ext.h>
#include <iosfwd>
#include "column.hh"
#include "concepts.hh"
#include "types.hh"
#include "row_fwd.hh"
#include "gtable_fwd.hh"
#include "table_fwd.hh"
#include "type_functions.hh"

namespace tables
{

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
class gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>
{
public:

  using ts_t = std::tuple<Ts...>;
  using ps_t = std::tuple<Ps...>;
  using table_t = table<std::pair<Ts,Ps>...>;

  using gs_t = std::tuple<Gs...>;  // group-columns
  using pgs_t = many_snd_elem_at_t<ts_t,ps_t,gs_t>; // primitive-types for group-columns
  using gtable_t = gtable<table_t,gs_t>;
  using grow_t = to_row_t<tuple_ext::zip_t<gs_t,pgs_t>>;

  using ngs_t = tuple_ext::remove_t<gs_t,ts_t>;
  using ng_table_t = typename table_t::select_t<ngs_t>;
  using ngrow_t = typename ng_table_t::row_t;

  using map_t = std::map<grow_t, ng_table_t>;

  // Construct from table
  explicit gtable(const table_t& t);

  // Construct from map_t
  explicit gtable(map_t&& m);

  // Copy constructor
  gtable(const gtable_t&);

  // Copy assignment operator
  gtable& operator=(const gtable_t&);

  // Move constructor
  gtable(gtable_t&& other);

  // Move assignment operator
  gtable& operator=(gtable_t&& other);

  // ==================================== n_groups =================================

  std::size_t n_groups() const;

  // ==================================== m =================================

  map_t m() const;

  // ==================================== contains =================================

  bool contains(const grow_t& g) const;

  // ==================================== get =================================

  ng_table_t get(const grow_t& k) const;

  // ==================================== take/drop =================================

  gtable_t take(const std::size_t n) const;

  gtable_t drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  gtable_t rtake(const std::size_t n) const;

  gtable_t rdrop(const std::size_t n) const;

  // ==================================== ungroup =================================

  table_t ungroup() const;

  // ==================================== reverse =================================

  gtable_t reverse() const;

  // ==================================== fills =================================

  gtable_t fills() const;

  // ==================================== fill_with =================================

  gtable_t fill_with(const ngrow_t& row) const;

  // ==================================== zfill =================================

  gtable_t zfill() const;

  // ==================================== deltas =================================

  gtable_t deltas() const;

  // ==================================== distinct =================================

  gtable_t distinct() const;

  // ==================================== sort =================================

  gtable_t sort() const;

  // ==================================== sort_by =================================

  template<typename... Ss>
  requires (
    ( in<Ss,tuple_ext::remove_t<std::tuple<Gs...>,std::tuple<Ts...>>> && ... ) &&
    is_tuple_unique<std::tuple<Ss...>>
  )
  gtable_t sort_by() const;

  // ==================================== rsort_by =================================

  template<typename... Ss>
  requires (
    ( in<Ss,tuple_ext::remove_t<std::tuple<Gs...>,std::tuple<Ts...>>> && ... ) &&
    is_tuple_unique<std::tuple<Ss...>>
  )
  gtable_t rsort_by() const;

  // ==================================== abs =================================

  gtable_t abs() const;

  // ==================================== log / log2 / log10 =================================

  auto log() const;

  auto log2() const;

  auto log10() const;

  // ==================================== exp =================================

  auto exp() const;

  // ==================================== max/min =================================

  table_t max() const;

  table_t min() const;

  // ==================================== sum =================================

  table_t sum() const;

  // ==================================== avg =================================

  table_t avg() const;


private:

  table_t make_agg_table() const;

  void copy_ngs(
    const ng_table_t& src,
    const std::size_t irow,
    table_t& dst
  ) const;


private:
  map_t m_;
};

template <is_gtable GT>
bool operator==(const GT& lhs, const GT& rhs);


template <is_table T,tuple_ext::is_tuple G>
requires ( is_gtable<gtable<T,G>> )
std::ostream& operator<< (std::ostream& os, const gtable<T,G>& gt);

} // namespace tables

#include "impl/gtable.tcc"
