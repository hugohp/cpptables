// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== mtuple =================================

template <typename... Ts>
requires is_associated_map_of_types<std::tuple<Ts...>>
class mtuple;

// ==================================== is_mtuple =================================

namespace detail
{

template<typename T>
struct is_mtuple_s : std::false_type {};

template <typename... Ts,is_primitive_type... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ts,Vs>...>>
struct is_mtuple_s<mtuple<std::pair<Ts,Vs>...>> : std::true_type {};

} // namespace detail

template<typename T>
concept is_mtuple = detail::is_mtuple_s<T>::value;

} // namespace tables
