// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <iosfwd>
#include "row.hh"
#include "column.hh"
#include "concepts.hh"
#include "types.hh"
#include "table_fwd.hh"
#include "ktable_fwd.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

template <is_table T,typename... Ks>
class ktable<T,std::tuple<Ks...>>
{
public:

  using ts_t = typename T::ts_t;
  using ps_t = typename T::ps_t;
  using ks_t = std::tuple<Ks...>;

  using table_t = T;
  using ktable_t = ktable<table_t,ks_t>;

  using krow_t = typename table_t::select_t<ks_t>::row_t;

  // Constructor with initializer_list
  explicit ktable(const table_t& t);

  // Copy constructor
  ktable(const ktable_t&) = default;

  // Copy assignment operator
  ktable& operator=(const ktable_t&) = default;

  // Move constructor
  ktable(ktable_t&& other) = default;

  // Move assignment operator
  ktable& operator=(ktable_t&& other) = default;

  constexpr table_t t() const;

  // Returns index of the first element that is equal to `v`, or null_ix if not found.
  std::size_t find(const krow_t& v) const;

  template<typename Tm>
  requires ( 
    in<Tm,typename T::ts_t> &&
    !in<Tm,typename std::tuple<Ks...>> 
  )
  std::size_t find_asof(const krow_t& v,const typename T::p_of_t<Tm> tm) const;

  const vixs_t& sixs() const;

private:
  table_t t_;
  vixs_t sixs_;
};

template <is_ktable KT>
bool operator==(const KT& lhs, const KT& rhs);

template <is_table T,tuple_ext::is_tuple K>
requires ( is_ktable<ktable<T,K>> )
std::ostream& operator<< (std::ostream& os, const ktable<T,K>& kt);

} // namespace tables

#include "impl/ktable.tcc"
