// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <string>
#include <fstream>
#include "types.hh"
#include "concepts.hh"
#include "defines.hh"
#include "table.hh"
#include "column.hh"
#include "traits.hh"
#include "tmap.hh"
#include <tuple_ext/tuple_ext.h>
#include "strings/string_storage.hh"

namespace tables
{

// ==================================== is_tmap_of_invocables =================================

namespace detail
{

  template<typename T1,typename T2>
  struct is_tmap_of_invocables_s : std::false_type {};

  template<is_tmap M,typename... Ts,typename... Fs>
  requires ( 
    // Fs is invocable with std::string_view
    ( std::is_invocable_v<Fs,std::string_view> && ... ) 
    // Invoking Fs with std::string_view returns same primitive-type as primitive-type associated to Ts in T
    && ( std::is_same_v<
           std::invoke_result_t<Fs,std::string_view>,
           tmap_at_t<M,Ts>
         > && ... )
  )
  struct is_tmap_of_invocables_s<M,tmap<std::pair<Ts,Fs>...>> : std::true_type {};

} // namespace detail

template<typename T1,typename T2>
concept is_tmap_of_invocables = detail::is_tmap_of_invocables_s<T1,T2>::value;

namespace detail
{
  // ==================================== default_converter =================================

  template <is_primitive_type P>
  struct default_converter {

    P operator()(const std::string_view sv) const
    {
      return prim_traits<P>::from_string(sv);
    }
  };

  // ==================================== find_next =================================

  // Returns first &str[i] where str[i] == ch, for 0 <= i < n
  static char const* find_next(char const* const str, const std::size_t n, const char ch)
  {
    std::size_t i = 0;
    for ( ; i < n ; ++i )
    {
      if ( str[i] == ch ) break; 
    }
    return &str[i]; 
  }

  static std::string_view trim(const std::string_view& sv)
  {
    const char* const begin = sv.begin(); 
    const char* const end = sv.end(); 

    const char* from = begin;
    while ( from != end && *from == ' ') ++from;

    const char* to = end;
    while ( to != begin && *to == ' ') --to;

    assert ( from >= begin && from <= end);
    assert ( to >= begin && to <= end);
    assert ( from <= to );
    return std::string_view(from,to);
  }

  // ==================================== find_next_quoted_token =================================

  static std::string_view find_next_quoted_token(std::string_view& next, const std::size_t line_nb)   
  {
    const std::string_view nnext = next.substr(1);
    const char* const next_quote = find_next(nnext.data(), nnext.size(), '\"');
    if ( next_quote == nnext.end() )
    {
      std::stringstream ss;
      ss << "Could not find end-of-quote when parsing csv file at line " << line_nb;
      throw std::runtime_error(ss.str());
    }
    const std::string_view token = trim(std::string_view(nnext.begin(), next_quote));
    const char* next_pos = next_quote + 1;
    if ( next_pos != nnext.end() && *next_pos != ',')
    {
      std::stringstream ss;
      ss << "End-of-quote is not followed by delimier when parsing csv file at line " << line_nb;
      throw std::runtime_error(ss.str());
    }
    ++next_pos;
    next = std::string_view(next_pos,next.end());
    return token;
  }

   // ==================================== find_next_non_quoted_token =================================

  static std::string_view find_next_non_quoted_token(std::string_view& next, const std::size_t line_nb)   
  {
    const char* const next_delim = find_next(next.data(), next.size(), ',');
    const std::string_view token = trim(std::string_view(next.begin(), next_delim));
    const char* next_pos = next_delim + ( next_delim != next.end() );
    next = std::string_view(next_pos,next.end());
    return token;
  }

  // ==================================== find_next_token =================================

  static std::string_view find_next_token(std::string_view& next, const std::size_t line_nb)   
  {
    if ( next.empty() ) return next;

    const bool is_quote = (next[0] == '\"');
    if ( is_quote )
    {
      return find_next_quoted_token(next,line_nb);
    }
    else
    {
      return find_next_non_quoted_token(next,line_nb);
    }
  }

  // ==================================== loop =================================

  template<is_primitive_type P,typename F>
  requires ( 
    // F is invocable with std::string_view
    std::is_invocable_v<F,std::string_view>
    // Invoking F with std::string_view returns value of type P
    && std::is_same_v<std::invoke_result_t<F,std::string_view>,P>
  )
  struct loop
  {
    using prim_t = P;
    using col_t = column<P>;
    using f_t = F;

    static void run(std::string_view& next, col_t& col, const std::size_t line_nb)
    {
      const static f_t f;
      const std::string_view token = find_next_token(next, line_nb);
      const prim_t token_v = token.empty() ? prim_traits<prim_t>::none : f(token);
      col._v().push_back( token_v );
    }
  };

  // Slighlty different logic when primitive type is string_view
  template<typename F>
  requires ( 
    // F is invocable with std::string_view
    std::is_invocable_v<F,std::string_view>
    // Invoking F with std::string_view returns a value of type bool
    && std::is_same_v<std::invoke_result_t<F,std::string_view>,std::string_view>
  )
  struct loop<std::string_view,F>
  {
    using col_t = column<std::string_view>;
    using f_t = F;

    static void run(std::string_view& next, col_t& col, const std::size_t line_nb)
    {
      const static f_t f;
      const std::string_view token = find_next_token(next, line_nb);
      const std::string_view token_v = token.empty() ? prim_traits<std::string_view>::none : f(token);
      col._v().push_back( impl::string_storage::instance().add_string(token_v) );
    }
  };

  // Specialisation for bool
  template<typename F>
  requires ( 
    // F is invocable with std::string_view
    std::is_invocable_v<F,std::string_view>
    // Invoking F with std::string_view returns a value of type bool
    && std::is_same_v<std::invoke_result_t<F,std::string_view>,bool>
  )
  struct loop<bool,F>
  {
    using prim_t = bool;
    using col_t = column<prim_t>;
    using f_t = F;

    static void run(std::string_view& next, col_t& col, const std::size_t line_nb)
    {
      static f_t f;
      const std::string_view token = find_next_token(next, line_nb);
      col._v().push_back( f(token) );
    }
  };

  // ==================================== read_tokens =================================

  template<typename,typename> struct read_tokens;
 
  template<typename... Ts,is_primitive_type... Ps,is_tmap M>
  struct read_tokens<mtuple<std::pair<Ts,column<Ps>>...>,M>
  {
    using cols_t = mtuple<std::pair<Ts,column<Ps>>...>;
    using ts_t = std::tuple<Ts...>;
    using ps_t = std::tuple<Ps...>;

  private:

    template<typename S, typename enable = void>
    struct f_s 
    {
      using type = default_converter<snd_elem_at_t<ts_t,ps_t,S>>;
    };

    template<typename S>
    struct f_s<S,std::enable_if_t<tmap_has_v<M,S>>>
    {
      using type = tmap_at_t<M,S>;
    };

  public:
    template<typename S>
    using f_t = f_s<S>::type;

    static void run(std::string_view& line, cols_t& mt, const std::size_t line_nb)
    {
      (loop<Ps,f_t<Ts>>::run(line, mt.template _get<Ts>(), line_nb),...);
    }
  };

} // namespace detail

// ==================================== csv_options =================================

enum csv_options : unsigned int
{
  none = 0,
  header = 1 // csv has header
};

// ==================================== from_csv =================================

template <is_table T,is_tmap M = tmap<>>
requires is_tmap_of_invocables<to_tmap_t<typename T::ts_t,typename T::ps_t>,M> 
struct from_csv
{
  using table_t = T;

  public:
  static table_t run(const std::string& file_name, const csv_options opts = csv_options::none)
  {
     std::ifstream ifs(file_name, std::ios::in);
     
     constexpr std::size_t N = 64;
     std::string workspace;

     if ( opts & csv_options::header )
     {
       std::getline(ifs,workspace);
     }

     std::size_t line_nb = 0;
     table_t t;
     while ( ! ifs.eof() )
     {
       typename table_t::cols_t cols;
       std::array<std::string, N> lines; 

       // Read N lines
       std::size_t i_line = 0;
       while ( i_line < N && std::getline(ifs,lines[i_line]) )
         ++i_line;
      
       for (std::size_t i = 0 ; i < i_line ; ++i )
       {
         std::string_view line = lines[i];
         detail::read_tokens<typename table_t::cols_t,M>::run(line, cols, line_nb++);
       }
       t = t.append(table_t(cols));
     }
     return t;
  }
};

} // namespace tables
