// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cstdint>
#include <cassert>
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/defines.hh"
#include "cpptables/traits.hh"

namespace tables {
namespace impl {

struct string_buffer;

class string_storage
{
public:

  string_storage(const string_storage&) = delete;
  string_storage& operator=(const string_storage&) = delete;

  static string_storage& instance();

  // ==================================== add_string =================================
  
  std::string_view add_string(const std::string_view sv);

  // ==================================== add_string =================================

  std::string_view add_string(std::string&& sv);

  // ==================================== add_strings =================================

  std::vector<std::string_view> add_strings(const std::vector<std::string_view>& strs);

  // ==================================== n_strings  =================================

  std::size_t n_strings() const;

private:
  string_storage();
  ~string_storage();

  string_buffer* impl_ = nullptr;
};

} // namespace impl
} // namespace tables

