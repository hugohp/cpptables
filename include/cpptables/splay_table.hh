// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <filesystem>
#include "splay_table_fwd.hh"
#include "table.hh"

namespace tables {

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
class splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>
{
public:
  using table_t = table<std::pair<Ts,Ps>...>;

  using ts_t = std::tuple<Ts...>;
  using ds_t = std::tuple<Ds...>;
  using pds_t = typename table_t::ps_of_ts_t<ds_t>;
  using nds_t = tuple_ext::remove_t<ds_t,ts_t>;
  using splay_table_t = splay_table<table_t,ds_t>;

  using table_ds_t = typename table_t::select_t<ds_t>;

  // ==================================== constructor =================================

  explicit splay_table(const std::filesystem::path path);

  // ==================================== td =================================

  constexpr table_ds_t td() const;

  // ==================================== get =================================

  template<typename... Ss>
  requires( 
    ( sizeof...(Ss) > 0 ) &&
    ( in<Ss,std::tuple<Ts...>> && ... ) && 
    ( not_in<Ss,std::tuple<Ds...>> && ... )
  )
  auto get(const table_ds_t& td) const;

  // ==================================== update =================================

  void update(const table_t& t) const;

  // ==================================== upsert =================================

  void upsert(const table_t& t) const;

private:
  std::filesystem::path path_;
};

} // namespace tables

#include "impl/splay_table.tcc"
