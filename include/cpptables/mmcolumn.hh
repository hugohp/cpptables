// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <utility>
#include <chrono>
#include <iomanip>
#include "concepts.hh"
#include "types.hh"
#include "traits.hh"
#include "column_fwd.hh"
#include "column_impl_fwd.hh"

namespace tables
{

// ==================================== column_base  =================================

template<is_primitive_type P>
class mmcolumn_base
{
public:

  using prim_t = P;
  using mmcolumn_impl_t = mmcolumn_impl<prim_t>;
  using ptr_to_impl_t = shared_ptr<mmcolumn_impl_t>;

  // ==================================== constructor / assignment  =================================

  explicit mmcolumn_base(const std::filesystem::path path);

  mmcolumn_base(const mmcolumn_base<prim_t>& other) = delete;

  mmcolumn_base(mmcolumn_base<prim_t>&& other) = delete;

  mmcolumn_base& operator=(const mmcolumn_base<prim_t>& other) = delete;

  mmcolumn_base& operator=(mmcolumn_base<prim_t>&& other) = delete;

  // ==================================== size/count/n_nones =================================

  std::size_t size() const;

  std::size_t count() const;

  std::size_t n_nones() const;

  // ==================================== operator[] / at =================================

  prim_t operator[](const std::size_t i ) const;

  column<P> at(const vixs_t& ixs) const;

  column<P> at(const column<std::size_t>& ixs) const;

  // ==================================== take/drop =================================

  column<P> take(const std::size_t n) const;

  column<P> drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  column<P> rtake(const std::size_t n) const;

  column<P> rdrop(const std::size_t n) const;

  // ==================================== _append (scalar) =================================

  void _append(const prim_t val);

  // ==================================== _append (scalar) =================================

  void _append(const std::size_t count, const prim_t val);

  // ==================================== _append (column) =================================

  void _append(const column<P>& col);

  // ==================================== apply =================================

  template<typename F>
  requires (
    std::invocable<F,prim_t> &&
    is_same_v<std::invoke_result_t<F,P>,P>
  )
  auto _apply(F f) const;

  // ==================================== max/min =================================

  prim_t max() const;

  prim_t min() const;

  // ==================================== all =================================

  bool all() const;

  // ==================================== any =================================

  bool any() const;

protected:

  ptr_to_impl_t pimpl_;
};

// ==================================== mmcolumn<>  =================================

template<is_primitive_type P>
class mmcolumn<P> : column_base<P> {

public:
  using prim_t = P;

  using mmcolumn_base_t = mmcolumn_base<prim_t>;
  using mmcolumn_impl_t = typename mmcolumn_base_t::mmcolumn_impl_t;
  using ptr_to_impl_t = typename mmcolumn_base_t::ptr_to_impl_t;

  explicit mmcolumn(const std::filesystem::path path);

  mmcolumn(const mmcolumn<prim_t>& other) = delete;

  mmcolumn(mmcolumn<prim_t>&& other) = delete;

  mmcolumn& operator=(const mmcolumn<prim_t>& other) = delete;

  mmcolumn& operator=(mmcolumn<prim_t>&& other) = delete;
};

// ==================================== mmcolumn<is_arithmetic_type>  =================================

template<is_arithmetic_type P>
class mmcolumn<P> : public mmcolumn_base<P>
{
public:

  using prim_t = P;

  using mmcolumn_base_t = mmcolumn_base<prim_t>;
  using mmcolumn_impl_t = typename mmcolumn_base_t::mmcolumn_impl_t;
  using ptr_to_impl_t = typename mmcolumn_base_t::ptr_to_impl_t;

  // ==================================== constructor / assignment  =================================

  explicit mmcolumn(const std::filesystem::path path);

  mmcolumn(const mmcolumn<prim_t>& other) = delete;

  mmcolumn(mmcolumn<prim_t>&& other) = delete;

  mmcolumn& operator=(const mmcolumn<prim_t>& other) = delete;

  mmcolumn& operator=(mmcolumn<prim_t>&& other) = delete;

  // ==================================== sum =================================

  prim_t sum() const;

  // ==================================== avg =================================

  prim_t avg() const;

  // ==================================== dot =================================

  auto dot(const mmcolumn<P>& rhs) const;

  // ==================================== norm =================================

  double norm() const;
}

} // namespace tables

#include "mmcolumn_impl.hh"
