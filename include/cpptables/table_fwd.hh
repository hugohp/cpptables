// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <tuple>
#include "concepts.hh"
#include "types.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== table =================================

template <typename... Ts>
requires ( is_key_value_pairs<Ts...> )
class table;

// ==================================== is_table =================================

namespace detail
{

template<typename T>
struct is_table_s : std::false_type {};

template <typename... Ts,is_primitive_type... Ps>
struct is_table_s<table<std::pair<Ts,Ps>...>> : std::true_type {};

} // namespace detail

template<typename T>
concept is_table = detail::is_table_s<T>::value;

// ==================================== to_table_t =================================

namespace detail
{

template <tuple_ext::is_tuple T>
struct to_table_s;

template <typename... Ts,typename... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
struct to_table_s<std::tuple<std::pair<Ts,Ps>...>>
{
  using type = table<std::pair<Ts,Ps>...>;
};

} // namespace detail

template<tuple_ext::is_tuple T>
using to_table_t = detail::to_table_s<T>::type;

} // namespace tables
