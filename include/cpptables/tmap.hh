// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <tuple_ext/tuple_ext.h>
#include "concepts.hh"

namespace tables
{

template<typename... Ps>
requires is_associated_map_of_types<std::tuple<Ps...>>
class tmap;

// ==================================== is_tmap =================================

namespace detail
{
  template<typename Ts>
  struct is_tmap_s : public std::false_type {};

  template<typename... Ts>
  struct is_tmap_s<tmap<Ts...>> : public std::true_type {};

} // namespace detail

template<typename T>
concept is_tmap = detail::is_tmap_s<T>::value;

// ==================================== mtuple =================================

template<typename... Ks,typename... Vs>
struct tmap<std::pair<Ks,Vs>...>
{
  using ks_t = std::tuple<Ks...>;
  using vs_t = std::tuple<Vs...>;
};

// ==================================== tmap_size_v =================================

template<is_tmap M>
inline static constexpr std::size_t tmap_size_v = std::tuple_size_v<typename M::ks_t>;

// ==================================== tmap_at_t =================================

template<is_tmap M,typename T>
requires ( in<T,typename M::ks_t> )
using tmap_at_t = snd_elem_at_t<typename M::ks_t,typename M::vs_t,T>;

// ==================================== tmap_has_v =================================

template<is_tmap M,typename T>
inline static constexpr bool tmap_has_v = tuple_ext::has_type_v<T,typename M::ks_t>;

// ==================================== to_tmap_t =================================

namespace detail
{

template<tuple_ext::is_tuple Ks,tuple_ext::is_tuple Vs>
struct to_tmap_t;

template<typename... Ks,typename... Vs>
struct to_tmap_t<std::tuple<Ks...>,std::tuple<Vs...>>
{
  using type = tmap<std::pair<Ks,Vs>...>;
};

} // namespace detail

template<tuple_ext::is_tuple Ks,tuple_ext::is_tuple Vs>
using to_tmap_t = detail::to_tmap_t<Ks,Vs>::type;

} // namespace tables
