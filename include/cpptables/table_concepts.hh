// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include "table_fwd.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

namespace detail
{
  // True if primitive-types of the common columns in T1 and T2 are the same.
  template<is_table T1,is_table T2> 
  struct is_joinable_s
  {
    using cs_t = tuple_ext::inter_t<typename T1::ts_t,typename T2::ts_t>;       // common columns
    using p1s_t = typename T1::ps_of_ts_t<cs_t>;
    using p2s_t = typename T2::ps_of_ts_t<cs_t>;
  
    static constexpr bool value = std::is_same_v<p1s_t,p2s_t>;
  };
}

// ==================================== is_joinable =================================

// True if 
// * T1 and T2 are tables and 
// * primitive-types of the common columns of T1 and T2 are the same
template<typename T1,typename T2> 
concept is_joinable = (is_table<T1>) && (is_table<T2>) && detail::is_joinable_s<T1,T2>::value;

// ==================================== are_key_types_same =================================

// True if key-columns have same primitive-type
template<typename T,typename KT> 
concept are_key_types_same = (
  (is_table<T>) && (is_ktable<KT>) && 
  std::is_same_v<
    typename T::ps_of_ts_t<typename KT::ks_t>,            // primitive-types of T
    typename KT::table_t::ps_of_ts_t<typename KT::ks_t>   // primitive-types of KT::table_t
    //many_snd_elem_at_t<typename T::ts_t,typename T::ps_t,typename KT::ks_tt>,                     // primitive-types of T
    //many_snd_elem_at_t<typename KT::table_t::ts_t,typename KT::table_t::ps_t, typename KT::ks_t>  // primitive-types of KT::table_t
  >
);

// ==================================== is_kjoinable =================================

// True if:
// * T is a table, and KT is a ktable
// * key-columns of KT are in T::ts_t
// * key-columns have same primitive type
// * T and KT::table_t are joinable
template<typename T,typename KT> 
concept is_kjoinable = (
  (is_table<T>) && (is_ktable<KT>) && 
  all_in<typename KT::ks_t,typename T::ts_t> &&  // key-columns are in T::ts_t
  are_key_types_same<T,KT> &&                    // key-columns have same primitive-type
  is_joinable<T,typename KT::table_t>
);

} // namespace tables
