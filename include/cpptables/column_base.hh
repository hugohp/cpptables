// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include "concepts.hh"
#include "types.hh"
#include "traits.hh"
#include "column_fwd.hh"
#include "column_impl_fwd.hh"

namespace tables
{

// ==================================== column_base  =================================

template<is_column C>
class column_base
{
public:

  using prim_t = typename C::prim_t;

  using vector_t = std::vector<prim_t>;
  using column_t = C;
  using column_impl_t = column_impl<prim_t>;
  using ptr_to_impl_t = shared_ptr<column_impl_t>;

  // ==================================== constructor / assignment  =================================

  explicit column();

  explicit column(std::initializer_list<prim_t> l);

  explicit column(const vector_t& v);

  explicit column(vector_t&& v);

  explicit column(ptr_to_impl_t&& impl);
 
  column(const column<P>& other) = default;

  column(column<P>&& other) = default;

  column& operator=(const column<P>& other) = default;

  column& operator=(column<P>&& other) = default;

  // ==================================== size/count/n_nones =================================

  std::size_t size() const;

  std::size_t count() const;

  std::size_t n_nones() const;

  // ==================================== v/impl/_v/_impl =================================

  const vector_t& v() const;

  const column_impl_t& impl() const;

  vector_t& _v();

  ptr_to_impl_t& _impl();

  // ==================================== operator[] / at =================================

  P operator[](const std::size_t i ) const;

  column_t at(const vixs_t& ixs) const;

  column_t at(const column<std::size_t>& ixs) const;

  // ==================================== take/drop =================================

  column_t take(const std::size_t n) const;

  column_t drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  column_t rtake(const std::size_t n) const;

  column_t rdrop(const std::size_t n) const;

  // ==================================== append (scalar) =================================

  column_t append(const P val) const;

  // ==================================== _append (scalar) =================================

  void _append(const P val);

  // ==================================== append (column) =================================

  column_t append(const column_t& col) const;

  // ==================================== except =================================

  column_t except(const column_t& xs) const;

  // ==================================== apply =================================

  template<typename F>
  requires (
    std::invocable<F,prim_t> &&
    is_primitive_type<std::invoke_result_t<F,prim_t>>
  )
  auto apply(F f) const;

  // ==================================== cast_to =================================

  template<is_primitive_type P1>
  requires ( std::is_convertible_v<prim_t,P1> )
  column<P1> cast_to() const;

  // ==================================== reverse =================================

  column_t reverse() const;

  // ==================================== fills =================================

  column_t fills() const;

  // ==================================== fill_with (scalar) =================================

  column_t fill_with(const prim_t val) const;

  // ==================================== fill_with (column) =================================

  column_t fill_with(const column_t c) const;

  // ==================================== zfill =================================

  column_t zfill() const;

  // ==================================== replace =================================

  column_t replace(const column_t& c) const;

  // ==================================== deltas =================================

  column_t deltas() const;

  // ==================================== differ =================================

  column<bool> differ() const;

  // ==================================== is_none =================================

  column<bool> is_none() const;

  // ==================================== distinct =================================

  column_t distinct() const;

  // ==================================== sort =================================

  column_t sort() const;

  // ==================================== rsort =================================

  column_t rsort() const;

  // ==================================== xbar =================================

  column_t xbar(const prim_t val) const;

  // ==================================== sums / msum =================================

  column_t sums() const;

  column_t msum(const std::size_t m) const;

  // ==================================== mins / maxs =================================

  column_t mins() const;

  column_t maxs() const;
 
  // ==================================== mmin / mmax =================================

  column_t mmin(const std::size_t m) const;

  column_t mmax(const std::size_t m) const;

  // ==================================== next / prev =================================

  column_t next() const;

  column_t prev() const;

  // ==================================== abs =================================
  
  column_t abs() const;

  // ==================================== log / log2 / log10 =================================

  column<double> log() const;

  column<double> log2() const;

  column<double> log10() const;

  // ==================================== exp =================================

  column<double> exp() const;

  // ==================================== mod =================================

  column<double> mod(const prim_t y) const;

  // ==================================== max/min =================================

  P max() const;

  P min() const;

  // ==================================== sum =================================

  P sum() const;

  // ==================================== avg =================================

  P avg() const;

  // ==================================== dot =================================

  template<is_primitive_type P1>
  requires requires (P a, P1 b) { a * b; }
  auto dot(const column<P1>& rhs) const;

  // ==================================== norm =================================

  double norm() const;

  // ==================================== all =================================

  bool all() const;

  // ==================================== any =================================

  bool any() const;

  // ==================================== cross =================================

  template<is_primitive_type P1>
  std::pair<column<P>,column<P1>> cross(const column<P1>& other) const;

  // ==================================== eq =================================

  column<bool> eq(const column_t& other) const;

  column<bool> eq(const prim_t cte) const;

  // ==================================== ne =================================

  column<bool> ne(const column_t& other) const;

  column<bool> ne(const prim_t cte) const;

  // ==================================== lt =================================

  column<bool> lt(const column_t& other) const;

  column<bool> lt(const prim_t cte) const;


  // ==================================== le =================================

  column<bool> le(const column_t& other) const;

  column<bool> le(const prim_t cte) const;

  // ==================================== gt =================================

  column<bool> gt(const column_t& other) const;

  column<bool> gt(const prim_t cte) const;

  // ==================================== ge =================================

  column<bool> ge(const column_t& other) const;

  column<bool> ge(const prim_t cte) const;

private:

  ptr_to_impl_t pimpl_;
};

// ==================================== column<is_arithmetic_type>  =================================

template<is_arithmetic_type P>
class column : public column_base<column<P>>
{
public:

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base() 
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<P>& other) = default;

  column(column<P>&& other) = default;

  column& operator=(const column<P>& other) = default;

  column& operator=(column<P>&& other) = default;

  // ==================================== deltas =================================

  column_t deltas() const;

  // ==================================== xbar =================================

  column_t xbar(const prim_t val) const;

  // ==================================== sums / msum =================================

  column_t sums() const;

  column_t msum(const std::size_t m) const;

  // ==================================== abs =================================
  
  column_t abs() const;

  // ==================================== log / log2 / log10 =================================

  column<double> log() const;

  column<double> log2() const;

  column<double> log10() const;

  // ==================================== exp =================================

  column<double> exp() const;

  // ==================================== mod =================================

  column<double> mod(const prim_t y) const;

  // ==================================== sum =================================

  P sum() const;

  // ==================================== avg =================================

  P avg() const;

  // ==================================== dot =================================

  template<is_primitive_type P1>
  requires requires (P a, P1 b) { a * b; }
  auto dot(const column<P1>& rhs) const;

  // ==================================== norm =================================

  double norm() const;
};

// ==================================== column<std::string_view>  =================================

template<>
class column<std::string_view> : public column_base<column<std::string_view>>
{
public:

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base() 
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<P>& other) = default;

  column(column<P>&& other) = default;

  column& operator=(const column<P>& other) = default;

  column& operator=(column<P>&& other) = default;

};

// ==================================== column<bool>  =================================

template<>
class column<bool> : public column_base<column<bool>>
{
public:

  // ==================================== constructor / assignment  =================================

  explicit column() : 
    column_base() 
  {}

  explicit column(std::initializer_list<prim_t> l) : 
    column_base(l) 
  {}

  explicit column(const vector_t& v) : 
    column_base(v) 
  {}

  explicit column(vector_t&& v) : 
    column_base(std::forward<vector_t>(v)) 
  {}

  explicit column(ptr_to_impl_t&& impl) : 
    column_base(std::forward<ptr_to_impl_t>(impl))
  {}
 
  column(const column<P>& other) = default;

  column(column<P>&& other) = default;

  column& operator=(const column<P>& other) = default;

  column& operator=(column<P>&& other) = default;

  // ==================================== neg =================================

  column_t neg() const;

  // ==================================== where =================================

  vixs_t where() const;
};


} // namespace tables
