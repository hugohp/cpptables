// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cassert>
#include <algorithm>
#include <vector>
#include <string.h>
#include <iostream>
#include "cpptables/types.hh"

namespace tables {
namespace impl{


// A string_buffer contains `n` strings, with string i starting at str[pos[i]], ending at str[pos[i+1]].
struct string_buffer {

  std::string buffer; // string i is at pos[i], ends at pos[i+1] or at buffer.size() if i=n-1
  vixs_t pos;  

  inline std::size_t n_strings() const
  {
    return pos.size();
  }

  // Returns string at position i
  inline std::string_view at(const std::size_t i) const
  {
    assert( i < pos.size() );

    const std::size_t n = pos.size();

    const std::size_t pi = pos[i];
    char const * const str = &buffer.c_str()[pi];
    std::size_t len = ( i==(n-1) ) ? buffer.length()-pi : pos[i+1]-pi;
    return {str,len};
  }
};

} // namespace impl
} // namespace tables
