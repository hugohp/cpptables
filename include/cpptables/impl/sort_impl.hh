// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cassert>
#include <algorithm>
#include <numeric>
#include <vector>
#include <string.h>
#include "cpptables/table.hh"

namespace tables {
namespace impl {

namespace {

  static bool has_expected_indices(const vixs_t& sixs)
  {
    std::set<std::size_t> copy(sixs.begin(), sixs.end());
    const std::size_t n = sixs.size();
    return (n == 0 || ((copy.size() == n) && (*copy.begin() == 0) && (*copy.rbegin() == n-1)));
  }
} // anonymous namespace

template<is_table T>
void make_sorted_indices(const T& t, vixs_t& sixs)
{
  const std::size_t n = t.nrows();
  sixs.resize(n);

  if ( n == 0 ) return;

  std::iota(sixs.begin(), sixs.end(), 0);

  std::stable_sort(sixs.begin(), sixs.end(),
       [&t](std::size_t i1, std::size_t i2) {return t.irow(i1) < t.irow(i2);});
}

template<is_table T>
void make_sorted_indices_from(const T& t, vixs_t& sixs)
{
  assert(sixs.size() = t.nrows() && has_expected_indices(sixs));

  std::stable_sort(sixs.begin(), sixs.end(),
       [&t](std::size_t i1, std::size_t i2) {return t.irow(i1) < t.irow(i2);});
}

template<is_table T>
void make_rsorted_indices(const T& t, vixs_t& sixs)
{
  const std::size_t n = t.nrows();
  sixs.resize(n);

  if ( n == 0 ) return;

  std::iota(sixs.begin(), sixs.end(), 0);

  std::stable_sort(sixs.begin(), sixs.end(),
       [&t](std::size_t i1, std::size_t i2) {return t.irow(i1) > t.irow(i2);});
}

template<is_table T>
void make_rsorted_indices_from(const T& t, vixs_t& sixs)
{
  assert(sixs.size() = t.nrows() && has_expected_indices(sixs));

  std::stable_sort(sixs.begin(), sixs.end(),
       [&t](std::size_t i1, std::size_t i2) {return t.irow(i1) > t.irow(i2);});
}

} // namespace impl
} // namespace tables

