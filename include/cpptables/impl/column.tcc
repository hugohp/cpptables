// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

// ==================================== constructor / assignment  =================================

template<is_primitive_type P>
inline column_base<P>::column_base()
 : pimpl_(std::make_shared<column_base<P>::column_impl_t>())
{}


template<is_primitive_type P>
inline column_base<P>::column_base(std::initializer_list<prim_t> l)
 : pimpl_(std::make_shared<column_base<P>::column_impl_t>(l))
{}


template<is_primitive_type P>
inline column_base<P>::column_base(const column_base<P>::vector_t& v)
 : pimpl_(std::make_shared<column_base<P>::column_impl_t>(v))
{}


template<is_primitive_type P>
inline column_base<P>::column_base(column_base<P>::vector_t&& v)
 : pimpl_(std::make_shared<column_base<P>::column_impl_t>(std::forward<column_base<P>::vector_t>(v)))
{}

template<is_primitive_type P>
inline column_base<P>::column_base(column_base<P>::ptr_to_impl_t&& impl) :
  pimpl_(std::forward<column_base<P>::ptr_to_impl_t>(impl))
{}


// ==================================== size/count/n_nones =================================

template<is_primitive_type P>
std::size_t
inline column_base<P>::size() const
{
  return pimpl_->size();
}

template<is_primitive_type P>
std::size_t
inline column_base<P>::count() const
{
  return pimpl_->count();
}

template<is_primitive_type P>
std::size_t
inline column_base<P>::n_nones() const
{
  return pimpl_->n_nones();
}

    // ==================================== v/impl/_v/_impl =================================

template<is_primitive_type P>
inline
const column_base<P>::vector_t&
column_base<P>::v() const
{
  return pimpl_->v();
}

template<is_primitive_type P>
inline
const column_base<P>::column_impl_t&
column_base<P>::impl() const
{
  return *pimpl_;
}

template<is_primitive_type P>
inline
column_base<P>::vector_t&
column_base<P>::_v()
{
  return pimpl_->_v();
}

template<is_primitive_type P>
inline
column_base<P>::ptr_to_impl_t&
column_base<P>::_impl()
{
  return pimpl_;
}

// ==================================== operator[] / at =================================

template<is_primitive_type P>
inline
typename column_base<P>::prim_t
column_base<P>::operator[](const std::size_t i ) const
{
  return pimpl_->v()[i];
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::at(const vixs_t& ixs) const
{
  return column_t(pimpl_->at(ixs));
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::at(const column<std::size_t>& ixs) const
{
  return at(ixs.v());
}

// ==================================== take/drop =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::take(const std::size_t n) const
{
  return column_t(pimpl_->take(n));
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::drop(const std::size_t n) const
{
  return column_t(pimpl_->drop(n));
}

// ==================================== rtake/rdrop =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::rtake(const std::size_t n) const
{
  return column_t(pimpl_->rtake(n));
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::rdrop(const std::size_t n) const
{
  return column_t(pimpl_->rdrop(n));
}
// ==================================== append (scalar) =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::append(const prim_t val) const
{
  return column_t(pimpl_->append(val));
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline
void
column_base<P>::_append(const prim_t val)
{
  pimpl_->_append(val);
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline
void
column_base<P>::_append(const std::size_t count, const prim_t val)
{
  pimpl_->_append(count, val);
}

// ==================================== _append (column) =================================

template<is_primitive_type P>
inline
void
column_base<P>::_append(const column_t& col)
{
  pimpl_->_append(*col.pimpl_);
}
// ==================================== append (column) =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::append(const column_t& col) const
{
  return column_t(pimpl_->append(*col.pimpl_));
}

// ==================================== except =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::except(const column_t& xs) const
{
  return column_t(pimpl_->except(xs.impl()));
}

// ==================================== apply =================================

template<is_primitive_type P>
template<typename F>
////requires (
  //std::invocable<F,typename column_base<P>::prim_t> &&
  //is_primitive_type<std::invoke_result_t<F,typename column_base<P>::prim_t>>
//)
inline
auto
column_base<P>::apply(F f) const
{
  using R = std::invoke_result_t<F,prim_t>;
  return column<R>(pimpl_->template apply<F>(f));
}

// ==================================== cast_to =================================

template<is_primitive_type P>
template<is_primitive_type P1>
requires ( std::is_convertible_v<typename column_base<P>::prim_t,P1> )
inline
column<P1>
column_base<P>::cast_to() const
{
  return column<P1>(pimpl_->template cast_to<P1>());
}

// ==================================== reverse =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::reverse() const
{
  return column_t(pimpl_->reverse());
}

// ==================================== fills =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::fills() const
{
  return column_t(pimpl_->fills());
}

// ==================================== fill_with (scalar) =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::fill_with(const column_base<P>::prim_t val) const
{
  return column_t(pimpl_->fill_with(val));
}

// ==================================== fill_with (column) =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::fill_with(const column_t c) const
{
  return column_t(pimpl_->fill_with(c.impl()));
}

// ==================================== zfill =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::zfill() const
{
  return column_t(pimpl_->zfill());
}

// ==================================== replace =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::replace(const column_base<P>::column_t& c) const
{
  return column_t(pimpl_->replace(c.impl()));
}

// ==================================== differ =================================

template<is_primitive_type P>
inline
column<bool>
column_base<P>::differ() const
{
  return column<bool>(pimpl_->differ());
}

// ==================================== is_none =================================

template<is_primitive_type P>
inline
column<bool>
column_base<P>::is_none() const
{
  return column<bool>(pimpl_->is_none());
}

// ==================================== distinct =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::distinct() const
{
  return column_t(pimpl_->distinct());
}

// ==================================== sort =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::sort() const
{
  return column_t(pimpl_->sort());
}

// ==================================== rsort =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::rsort() const
{
  return column_t(pimpl_->rsort());
}



// ==================================== mins / maxs =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::mins() const
{
  return column_t(pimpl_->mins());
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::maxs() const
{
  return column_t(pimpl_->maxs());
}

// ==================================== mmin / mmax =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::mmin(const std::size_t m) const
{
  return column_t(pimpl_->mmin(m));
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::mmax(const std::size_t m) const
{
  return column_t(pimpl_->mmax(m));
}

// ==================================== next / prev =================================

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::next() const
{
  return column_t(pimpl_->next());
}

template<is_primitive_type P>
inline
column_base<P>::column_t
column_base<P>::prev() const
{
  return column_t(pimpl_->prev());
}


// ==================================== max/min =================================

template<is_primitive_type P>
inline
column_base<P>::prim_t
column_base<P>::max() const
{
  return pimpl_->max();
}

template<is_primitive_type P>
inline
column_base<P>::prim_t
column_base<P>::min() const
{
  return pimpl_->min();
}

// ==================================== all =================================

template<is_primitive_type P>
inline
bool
column_base<P>::all() const
{
  return pimpl_->all();
}

// ==================================== any =================================

template<is_primitive_type P>
inline
bool
column_base<P>::any() const
{
  return pimpl_->any();
}

// ==================================== cross =================================

template<is_primitive_type P>
template<is_primitive_type P1>
inline
std::pair<column<P>,column<P1>>
column_base<P>::cross(const column<P1>& other) const
{
  auto p = pimpl_->cross(other.impl());
  return {
    column<P>(std::move(p.first)),
    column<P1>(std::move(p.second))
  };
}


// ==================================== eq =================================

template<is_primitive_type P>
inline
column<bool>
column_base<P>::eq(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  return column<bool>(pimpl_->eq(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::eq(const P cte) const
{
  return column<bool>(pimpl_->eq(cte));
}

// ==================================== ne =================================

template<is_primitive_type P>
inline
column<bool>
column_base<P>::ne(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  return column<bool>(pimpl_->ne(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::ne(const P cte) const
{
  return column<bool>(pimpl_->ne(cte));
}

// ==================================== lt =================================

template<is_primitive_type P>
column<bool>
column_base<P>::lt(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  return column<bool>(pimpl_->lt(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::lt(const P cte) const
{
  return column<bool>(pimpl_->lt(cte));
}

// ==================================== le =================================

template<is_primitive_type P>
column<bool>
column_base<P>::le(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  return column<bool>(pimpl_->le(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::le(const P cte) const
{
  return column<bool>(pimpl_->le(cte));
}


// ==================================== gt =================================

template<is_primitive_type P>
column<bool>
column_base<P>::gt(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }
  return column<bool>(pimpl_->gt(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::gt(const P cte) const
{
  return column<bool>(pimpl_->gt(cte));
}


// ==================================== ge =================================

template<is_primitive_type P>
column<bool>
column_base<P>::ge(const column<P>& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }
  return column<bool>(pimpl_->ge(rhs.impl()));
}

template<is_primitive_type P>
inline
column<bool>
column_base<P>::ge(const P cte) const
{
  return column<bool>(pimpl_->ge(cte));
}


// ==================================== in =================================

template<is_primitive_type P>
inline
column<bool>
column_base<P>::in(const column<P>& other) const
{
  return column<bool>(pimpl_->in(other.impl()));
}



// ==================================== deltas =================================

template<is_arithmetic_type P>
inline
column<P>::column_t
column<P>::deltas() const
{
  return column_t(column_base_t::pimpl_->deltas());
}

// ==================================== xbar =================================

template<is_arithmetic_type P>
inline
column<P>::column_t
column<P>::xbar(const prim_t val) const
{
  return column_t(column_base_t::pimpl_->xbar(val));
}

// ==================================== sums / msum  =================================

template<is_arithmetic_type P>
inline
column<P>::column_t
column<P>::sums() const
{
  return column_t(column_base_t::pimpl_->sums());
}

template<is_arithmetic_type P>
inline
column<P>::column_t
column<P>::msum(const std::size_t m) const
{
  return column_t(column_base_t::pimpl_->msum(m));
}

// ==================================== abs =================================

template<is_arithmetic_type P>
inline
column<P>::column_t
column<P>::abs() const
{
  return column_t(column_base_t::pimpl_->abs());
}

// ==================================== log / log2 / log10 =================================

template<is_arithmetic_type P>
inline
column<double>
column<P>::log() const
{
  return column<double>(column_base_t::pimpl_->log());
}

template<is_arithmetic_type P>
inline
column<double>
column<P>::log2() const
{
  return column<double>(column_base_t::pimpl_->log2());
}

template<is_arithmetic_type P>
inline
column<double>
column<P>::log10() const
{
  return column<double>(column_base_t::pimpl_->log10());
}

// ==================================== exp =================================

template<is_arithmetic_type P>
inline
column<double>
column<P>::exp() const
{
  return column<double>(column_base_t::pimpl_->exp());
}

// ==================================== mod =================================

template<is_arithmetic_type P>
inline
column<double>
column<P>::mod(const P y) const
{
  return column<double>(column_base_t::pimpl_->mod(y));
}

// ==================================== sum =================================

template<is_arithmetic_type P>
inline
P
column<P>::sum() const
{
  return column_base_t::pimpl_->sum();
}


// ==================================== avg =================================

template<is_arithmetic_type P>
inline
P
column<P>::avg() const
{
  return column_base_t::pimpl_->avg();
}

// ==================================== dot =================================

template<is_arithmetic_type P>
template<is_arithmetic_type P1>
requires requires (P a, P1 b) { a * b; }
inline
auto
column<P>::dot(const column<P1>& rhs) const
{
  return column_base_t::pimpl_->dot(rhs.impl());
}

// ==================================== norm =================================

template<is_arithmetic_type P>
inline
double
column<P>::norm() const
{
  return column_base_t::pimpl_->norm();
}

// ==================================== neg =================================

inline
column<bool>
column<bool>::neg() const
{
  return column<bool>(pimpl_->neg());
}

// ==================================== where =================================

inline
vixs_t
column<bool>::where() const
{
  return column_base_t::pimpl_->where();
}

// ==================================== starts_with (scalar) =================================

inline
column<bool>
column<std::string_view>::starts_with(const std::string_view sv) const
{
  return column<bool>(column_base_t::pimpl_->starts_with(sv));
}

// ==================================== starts_with (column_base_t) =================================

inline
column<bool>
column<std::string_view>::starts_with(const column<std::string_view>& svs) const
{
  return column<bool>(column_base_t::pimpl_->starts_with(svs.impl()));
}

// ==================================== ends_with (scalar) =================================

inline
column<bool>
column<std::string_view>::ends_with(const std::string_view sv) const
{
  return column<bool>( column_base_t::pimpl_->ends_with(sv) );
}

// ==================================== ends_with (column) =================================

inline
column<bool>
column<std::string_view>::ends_with(const column<std::string_view>& svs) const
{
  return column<bool>( column_base_t::pimpl_->ends_with(svs.impl()) );
}

// ==================================== substr (scalar) =================================

inline
column<std::string_view>
column<std::string_view>::substr(const std::size_t pos, const std::size_t count) const
{
  return column<std::string_view>( column_base_t::pimpl_->substr(pos,count) );
}

// ==================================== substr (column) =================================

inline
column<std::string_view>
column<std::string_view>::substr(const column<std::size_t>& pos, const column<std::size_t>& count) const
{
  return column<std::string_view>( column_base_t::pimpl_->substr(pos.impl(),count.impl()) );
}

// ==================================== concat (scalar) =================================

inline
column<std::string_view>
column<std::string_view>::concat(const std::string_view sv) const
{
  return column<std::string_view>( column_base_t::pimpl_->concat(sv) );
}

// ==================================== concat (column) =================================

inline
column<std::string_view>
column<std::string_view>::concat(const column<std::string_view>& svs) const
{
  return column<std::string_view>( column_base_t::pimpl_->concat(svs.impl()) );
}

// ==================================== to_string =================================

inline
std::string
column<std::string_view>::to_string(const std::string_view delim) const
{
  return column_base_t::pimpl_->to_string(delim);
}

// ==================================== time_since_epoch  =================================

template<is_time_point P>
column<typename P::duration>
column<P>::time_since_epoch() const
{
  return column<typename P::duration>( column_base_t::pimpl_->time_since_epoch() );
}

// ==================================== time_point_cast  =================================

template<is_time_point P>
template <
  is_duration ToDuration,
  typename Clock,
  typename Duration
>
column<std::chrono::time_point<Clock,ToDuration>>
column<P>::time_point_cast() const
{
  return column<std::chrono::time_point<Clock,ToDuration>>(
    column_base_t::pimpl_->template time_point_cast<ToDuration,Clock,Duration>()
  );
}

// ==================================== to_time_t  =================================

template<is_time_point P>
column<std::time_t>
column<P>::to_time_t() const
{
  return column<std::time_t>( column_base_t::pimpl_->template to_time_t() );
}

// ==================================== operator+(duration)  =================================

template<is_time_point P>
template<is_duration D>
constexpr column<
  std::chrono::time_point<
    typename P::clock,
    std::common_type_t<D,typename P::duration>
  >
>
column<P>::operator+(const column<D>& col) const
{
  using R = std::chrono::time_point<
    typename P::clock,
    std::common_type_t<D,typename P::duration>
  >;

  return column<R>( column_base_t::pimpl_->template operator+<D>(col) );
}

// ==================================== operator-(duration)  =================================

template<is_time_point P>
template<is_duration D>
constexpr column<
  std::chrono::time_point<
    typename P::clock,
    std::common_type_t<D,typename P::duration>
  >
>
column<P>::operator-(const column<D>& col) const
{
  using R = std::chrono::time_point<
    typename P::clock,
    std::common_type_t<D,typename P::duration>
  >;

  return column<R>( column_base_t::pimpl_->template operator-<D>(col) );
}
// ==================================== duration_cast  =================================

template<is_duration P>
template<is_duration ToDuration>
column<ToDuration>
column<P>::duration_cast() const
{
  return column<ToDuration>( column_base_t::pimpl_->template duration_cast<ToDuration>() );
}


// ==================================== floor  =================================

template<is_duration P>
template<is_duration ToDuration>
column<ToDuration>
column<P>::floor() const
{
  return column<ToDuration>( column_base_t::pimpl_->template floor<ToDuration>() );
}

// ==================================== ceil  =================================

template<is_duration P>
template<is_duration ToDuration>
column<ToDuration>
column<P>::ceil() const
{
  return column<ToDuration>( column_base_t::pimpl_->template ceil<ToDuration>() );
}

// ==================================== round  =================================

template<is_duration P>
template<is_duration ToDuration>
column<ToDuration>
column<P>::round() const
{
  return column<ToDuration>( column_base_t::pimpl_->template round<ToDuration>() );
}

// ==================================== abs  =================================

template<is_duration P>
column<P>
column<P>::abs() const
{
  return column<P>( column_base_t::pimpl_->template abs() );
}

// ==================================== operator<< =================================

template<is_primitive_type P>
inline
std::ostream& operator<< (std::ostream& os, const column<P>& col)
{
  const std::size_t n = col.size();
  for ( std::size_t i = 0; i < n ; ++ i)
  {
    const P vi = col.v()[i];
    if ( prim_traits<P>::is_none(vi) )
    {
      os << "none";
    }
    else
    {
      if constexpr ( is_time_point<P> )
      {
        const std::time_t ti = std::chrono::system_clock::to_time_t(vi);
        os << std::put_time( std::gmtime(&ti), "%FT%T");

        const auto duration = vi.time_since_epoch();
        const auto nanoseconds = (std::chrono::duration_cast<std::chrono::nanoseconds>(duration) % std::chrono::nanoseconds(1000000000)).count();
        if ( nanoseconds != 0 )
        {
           os << " " << nanoseconds;
        }
      }
      else
      {
        os << vi;
      }
    }
    if (i < (n-1)) os << " ";
  }
  return os;
}

} // namespace tables
