// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <vector>
#include <ostream>
#include <experimental/array>
#include "cpptables/row.hh"
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/table.hh"
#include "cpptables/ktable.hh"
#include <tuple_ext/tuple_ext.h>
#include "cpptables/impl/sort_impl.hh"
#include "cpptables/impl/io/show_table.hh"

namespace tables
{

// ==================================== constructor =================================

template <is_table T,typename... Ks>
ktable<T,std::tuple<Ks...>>::ktable(const table_t& t)
 : t_(t)
 , sixs_(t.nrows())
{
   impl::make_sorted_indices(t.template select<ks_t>(), sixs_);
}

// ==================================== t =================================

template <is_table T,typename... Ks>
constexpr T ktable<T,std::tuple<Ks...>>::t() const
{
  return t_;
}

// ==================================== find =================================

template <is_table T,typename... Ks>
std::size_t
ktable<T,std::tuple<Ks...>>::find(const krow_t& v) const
{
  const std::size_t n = sixs_.size();
  if ( n == 0 ) return null_ix;

  const auto tk = t_.template select<Ks...>();

  const auto it = std::lower_bound(
    sixs_.begin(),
    sixs_.end(),
    v,
    [&tk](std::size_t i1, const krow_t& ri) {return tk.irow(i1) < ri;}
  );

  if ( (it != sixs_.end()) && (tk.irow(*it) == v) )
  {
    return *it;
  }

  return null_ix;
}

// ==================================== find_asof =================================

template <is_table T,typename... Ks>
template<typename Tm>
requires (
  in<Tm,typename T::ts_t> &&
  !in<Tm,typename std::tuple<Ks...>>
)
std::size_t

ktable<T,std::tuple<Ks...>>::find_asof(const krow_t& v,const typename T::p_of_t<Tm> tm) const
{
  assert ( std::is_sorted( t_.template col<Tm>().v().begin(), t_.template col<Tm>().v().end() ) );
  const std::size_t n = sixs_.size();

  if ( n == 0 ) return null_ix;

  const auto tk = t_.template select<Ks...>();

  const auto it_lb = std::lower_bound(
    sixs_.begin(),
    sixs_.end(),
    v,
    [&tk](std::size_t i1, const krow_t& ri) {return tk.irow(i1) < ri;}
  );

  if ( it_lb == sixs_.end() )
  {
    return null_ix;
  }

  const auto it_ub = std::upper_bound(
    it_lb,
    sixs_.end(),
    v,
    [&tk](const krow_t& ri, std::size_t i1) {return tk.irow(i1) < ri;}
  );

  using tm_prim_t = typename table_t::p_of_t<Tm>;
  const column<tm_prim_t> tms = t().template col<Tm>();

  const auto rit0 = std::make_reverse_iterator(it_ub);
  const auto rit1 = std::make_reverse_iterator(it_lb);

  const auto it = std::lower_bound(
    rit0,
    rit1,
    tm,
    [&tms](std::size_t i1, const tm_prim_t& vi) {return tms[i1] > vi;}
  );

  return (it == rit1) ? null_ix : *it;
}

// ==================================== operator== =================================

template <is_ktable KT>
bool operator==(const KT& lhs, const KT& rhs)
{
  return ( lhs.t() == rhs.t() );
}

// ==================================== operator<< =================================

template <is_table T,tuple_ext::is_tuple K>
requires ( is_ktable<ktable<T,K>> )
std::ostream& operator<< (std::ostream& os, const ktable<T,K>& kt)
{
  using namespace impl::io;

  using ts_t = typename T::ts_t;
  using ps_t = typename T::ps_t;

  using ks_t = K; // key columns
  using pks_t = many_snd_elem_at_t<ts_t,ps_t,ks_t>; // primitive-types for key columns

  using nks_t = tuple_ext::remove_t<ks_t,ts_t>;
  using pnks_t = many_snd_elem_at_t<ts_t,ps_t,nks_t>; // primitive-types for non-key columns

  const auto lhs = kt.t().template select<ks_t>();
  const auto rhs = kt.t().template select<nks_t>();

  const std::size_t nrows = kt.t().nrows();

  const auto lhs_css = make_many_column_string( lhs.to_simple_table() );
  const auto rhs_css = make_many_column_string( rhs.to_simple_table() );

  header_display<ks_t,pks_t>::run(os, lhs_css);
  os << "|";
  header_display<nks_t,pnks_t>::run(os, rhs_css);
  os << "\n";

  line_display<ks_t,pks_t>::run(os, lhs_css);
  os << "|";
  line_display<nks_t,pnks_t>::run(os, rhs_css);
  os << "\n";

  for ( std::size_t i = 0 ; i < nrows ; ++i )
  {
    row_display<ks_t,pks_t>::run(os, i, lhs_css);
    os << "|";
    row_display<nks_t,pnks_t>::run(os, i, rhs_css);
    os << "\n";
  }
  return os;
}

} // namespace tables
