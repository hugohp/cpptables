// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== constructor / assignment  =================================

// Constructor with value-types
template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
inline
mtuple<std::pair<Ks,Vs>...>::mtuple()
{}

// Constructor with tuple-of-values
template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
inline
mtuple<std::pair<Ks,Vs>...>::mtuple(const Vs... vs)
 : vs_(std::make_tuple(vs...))
{}

// Constructor with tuple-of-values (by rvalue)
template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
inline
mtuple<std::pair<Ks,Vs>...>::mtuple(const std::tuple<Vs...>& vs)
  : vs_(vs)
{}

// Constructor with tuple-of-values (by rvalue)
template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
inline
mtuple<std::pair<Ks,Vs>...>::mtuple(std::tuple<Vs...>&& vs)
 : vs_(std::forward<std::tuple<Vs...>>(vs))
{}

/*
// ==================================== insert =================================

template<typename... Ks,typename... Vs>
template<is_mtuple M>
requires ( std::tuple_size_v<tuple_ext::inter_t<std::tuple<Ks...>,typename M::ks_t>> == 0 )
inline 
auto
mtuple<std::pair<Ks,Vs>...>::insert(const M& other) const
{
  using r_ks_t = tuple_ext::concat_t<std::tuple<Ks...>,typename M::ks_t>;
  using r_vs_t = tuple_ext::concat_t<std::tuple<Vs...>,typename M::vs_t>;
  using r_mtuple_t = to_mtuple_t<tuple_ext::zip_t<r_ks_t,r_vs_t>>;
  return r_mtuple_t( std::tuple_cat(vs_,other.vs()) );
}

// ==================================== remove =================================

template<typename... Ks,typename... Vs>
template<typename K>
requires in<K,std::tuple<Ks...>>
inline 
auto
mtuple<std::pair<Ks,Vs>...>::remove() const
{
  constexpr std::size_t ix = tuple_ext::elem_index_v<ks_t,K>;
  using r_mtuple_t = to_mtuple_t<tuple_ext::remove_t<std::pair<K,V>,mtuple_t::to_tuple_t>;
  using nvs_t
  return r_mtuple_t( std::tuple( select<
}
*/

// ==================================== get =================================

template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
template<typename K>
requires in<K,std::tuple<Ks...>>
inline
tuple_ext::elem_at_t<tuple_ext::elem_index_v<K,std::tuple<Ks...>>,std::tuple<Vs...>>
mtuple<std::pair<Ks,Vs>...>::get() const
{
  return std::get<tuple_ext::elem_index_v<K,std::tuple<Ks...>>>(vs_);
}

// ==================================== get =================================

template<typename... Ks,typename... Vs>
requires is_associated_map_of_types<std::tuple<std::pair<Ks,Vs>...>>
template<typename K>
requires in<K,std::tuple<Ks...>>
inline
tuple_ext::elem_at_t<tuple_ext::elem_index_v<K,std::tuple<Ks...>>,std::tuple<Vs...>>&
mtuple<std::pair<Ks,Vs>...>::_get()
{
  return std::get<tuple_ext::elem_index_v<K,std::tuple<Ks...>>>(vs_);
}

} // namespace tables
