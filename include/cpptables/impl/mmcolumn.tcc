// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

// ==================================== constructor / assignment  =================================

template<is_primitive_type P>
inline mmcolumn_base<P>::mmcolumn_base(const std::filesystem::path path)
 : pimpl_(std::make_shared<mmcolumn_base<P>::column_impl_t>(path))
{}

// ==================================== size/count/n_nones =================================

template<is_primitive_type P>
std::size_t
inline mmcolumn_base<P>::size() const
{
  return pimpl_->size();
}

template<is_primitive_type P>
std::size_t
inline mmcolumn_base<P>::count() const
{
  return pimpl_->count();
}

template<is_primitive_type P>
std::size_t
inline mmcolumn_base<P>::n_nones() const
{
  return pimpl_->n_nones();
}

// ==================================== operator[] / at =================================

template<is_primitive_type P>
inline
typename mmcolumn_base<P>::prim_t
mmcolumn_base<P>::operator[](const std::size_t i ) const
{
  return pimpl_->v()[i];
}

template<is_primitive_type P>
inline
colum<P>
mmcolumn_base<P>::at(const vixs_t& ixs) const
{
  return column_t(pimpl_->at(ixs));
}

template<is_primitive_type P>
inline
colum<P>
mmcolumn_base<P>::at(const column<std::size_t>& ixs) const
{
  return at(ixs.v());
}

// ==================================== take/drop =================================

template<is_primitive_type P>
inline
column<P>
mmcolumn_base<P>::take(const std::size_t n) const
{
  return column_t(pimpl_->take(n));
}

template<is_primitive_type P>
inline
column<P>
mmcolumn_base<P>::drop(const std::size_t n) const
{
  return column_t(pimpl_->drop(n));
}

// ==================================== rtake/rdrop =================================

template<is_primitive_type P>
inline
column<P>
mmcolumn_base<P>::rtake(const std::size_t n) const
{
  return column_t(pimpl_->rtake(n));
}

template<is_primitive_type P>
inline
column<P>
mmcolumn_base<P>::rdrop(const std::size_t n) const
{
  return column_t(pimpl_->rdrop(n));
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline
void
mmcolumn_base<P>::_append(const prim_t val)
{
  pimpl_->_append(val);
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline
void
mmcolumn_base<P>::_append(const std::size_t count, const prim_t val)
{
  pimpl_->_append(count, val);
}

// ==================================== _append (column) =================================

template<is_primitive_type P>
inline
void
mmcolumn_base<P>::_append(const column_t& col)
{
  pimpl_->_append(*col.pimpl_);
}

// ==================================== apply =================================

template<is_primitive_type P>
template<typename F>
requires (
  std::invocable<F,prim_t> &&
  is_same_v<std::invoke_result_t<F,P>,P>
)
inline
auto
mmcolumn_base<P>::_apply(F f) const
{
  return column<P>(pimpl_->template apply<F>(f));
}

// ==================================== max =================================

template<is_primitive_type P>
inline
P
mmcolumn_base<P>::max() const
{
  return pimpl_->max();
}
 
template<is_primitive_type P>
inline
P
mmcolumn_base<P>::min() const
{
  return pimpl_->min();
}
 
// ==================================== all =================================

template<is_primitive_type P>
inline
bool
mmcolumn_base<P>::all() const
{
  return pimpl_->all();
}

// ==================================== any =================================

template<is_primitive_type P>
inline
bool
mmcolumn_base<P>::any() const
{
  return pimpl_->any();
}

// ==================================== constructor / assignment  =================================

template<is_primitive_type P>
inline mmcolumn<P>::mmcolumn(const std::filesystem::path path)
 : mmcolumn_base<P>(path)
{}

// ==================================== constructor / assignment  =================================

template<is_arithmetic_type P>
inline mmcolumn<P>::mmcolumn(const std::filesystem::path path)
 : mmcolumn_base<P>(path)
{}

// ==================================== sum =================================

template<is_arithmetic_type P>
inline
P
mmcolumn_base<P>::sum() const
{
  return pimpl_->sum();
}

// ==================================== avg =================================

template<is_arithmetic_type P>
inline
P
mmcolumn_base<P>::avg() const
{
  return pimpl_->avg();
}

// ==================================== dot =================================

template<is_arithmetic_type P>
inline
P
mmcolumn_base<P>::dot(const mmcolumn<P>& rhs) const
{
  return pimpl_->dot(rhs);
}

// ==================================== norm =================================

template<is_arithmetic_type P>
inline
P
mmcolumn_base<P>::norm() const
{
  return pimpl_->norm();
}




} // namespace tables
