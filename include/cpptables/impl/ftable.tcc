// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <vector>
#include <ostream>
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/table.hh"
#include "cpptables/type_functions.hh"
#include <tuple_ext/tuple_ext.h>
#include "cpptables/impl/io/show_table.hh"
#include "cpptables/impl/column_functions.hh"

namespace tables
{

// Construct from table
template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
ftable<T,Gs>::ftable(const table_t& t)
  : t_(t)
  , m_(t.template indices_by<gs_t>())
{
}

// Copy constructor
template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
ftable<T,Gs>::ftable( const ftable_t& other)
  : t_(other.t_)
  , m_(other.m_)
{}

// Copy assignment operator
template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
ftable<T,Gs>&
ftable<T,Gs>::operator=( const ftable_t& other)
{
  if ( this != &other )
  {
    t_ = other.t_;
    m_ = other.m_;
  }
  return *this;
}

// Move constructor
template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
ftable<T,Gs>::ftable( ftable_t&& other)
  : t_(other.t_)
  , m_(std::forward<map_t>(other.m_))
{}

// Move assignment operator
template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
ftable<T,Gs>&
ftable<T,Gs>::operator=( ftable_t&& other)
{
  t_ = std::forward<table_t>(other.t_);
  m_ = std::forward<map_t>(other.m_);
  return *this;
}

// ==================================== n_groups =================================

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
std::size_t
ftable<T,Gs>::n_groups() const
{
  return m_.size();
}

// ==================================== t =================================

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
T
ftable<T,Gs>::t() const
{
  return t_;
}
// ==================================== m =================================

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
const typename ftable<T,Gs>::map_t&
ftable<T,Gs>::m() const
{
  return m_;
}

// ==================================== agg =================================

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
template<typename... Ss,typename... Fs>
requires (
  (sizeof...(Ss) == sizeof...(Fs))
  && ( all_in<std::tuple<Ss...>,tuple_ext::remove_t<Gs,typename T::ts_t>> )
  && ( is_col_to_prim_f<Fs,typename T::p_of_t<Ss>> && ... )
)
auto
ftable<T,Gs>::agg(const Fs... fs) const
{
  using gs_t = Gs;
  // primitive-types paired to index-types Gs
  using pgs_t = typename table_t::ps_of_ts_t<gs_t>;

  using ss_t = std::tuple<Ss...>;
  // primitive-types paired to index-types Ss
  using sps_t = std::tuple<
    std::invoke_result_t<
      Fs,
      column<typename table_t::p_of_t<Ss>>
    >...
  >;


  using s_row_t = to_row_t<tuple_ext::zip_t<ss_t,sps_t>>;

  using r_table_t = as_table_t<
    tuple_ext::concat_t<gs_t,ss_t>,
    tuple_ext::concat_t<pgs_t,sps_t>
  >;

  using r_row_t = typename r_table_t::row_t;
  using r_cols_t = typename r_table_t::cols_t;

  const std::size_t n = m_.size();

  r_cols_t cols;
  detail::reserve_cols(cols, n);

  r_table_t rt(cols);

  for ( const auto& p : m_ )
  {
    const vixs_t& ixs = p.second;

    const g_row_t grow = p.first;
    const s_row_t srow( fs(t_.template col<Ss>().at(ixs))... );
    const r_row_t rrow = grow.add(srow);
    rt._append(rrow);
  }
  return rt;
}

// ==================================== fupdate =================================

namespace detail
{
  template<is_table Tin,is_table Tout, typename F, typename S>
  void fupdate_by(const Tin& t_in,Tout& t_out,F f,const vixs_t& ixs)
  {
    using prim_in_t = typename Tin::p_of_t<S>;
    using prim_out_t = typename Tout::p_of_t<S>;

    const column<prim_in_t> c = t_in.template col<S>().at(ixs);
    const column<prim_out_t> cf = f(c);

    if ( c.size() != cf.size() )
    {
      throw std::length_error(__FUNCTION__);
    }

    std::size_t i = 0;
    for ( std::size_t ix : ixs )
    {
      t_out.template _col<S>()._v()[ix] = cf[i++];
    }
  }
}

template <is_table T,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
template<typename... Ss,typename... Fs>
requires (
  (sizeof...(Ss) == sizeof...(Fs))
  && ( all_in<std::tuple<Ss...>,tuple_ext::remove_t<Gs,typename T::ts_t>> )
  && ( is_col_to_col_f<Fs,typename T::p_of_t<Ss>> && ... )
)
auto
ftable<T,Gs>::fupdate(const Fs... fs) const
{
  using ss_t = std::tuple<Ss...>;

  // primitive-types paired to index-types Ss
  using sps_t = std::tuple<
    typename std::invoke_result_t<
      Fs,
      column<typename table_t::p_of_t<Ss>>
    >::prim_t...
  >;

  using ss_table_t = as_table_t<ss_t,sps_t>;
  using cols_ss_t = typename ss_table_t::cols_t;

  const std::size_t n = t_.nrows();

  cols_ss_t cols_ss;
  detail::resize_cols(cols_ss, n);

  ss_table_t t_ss(cols_ss);

  for ( const auto& p : m_ )
  {
    const vixs_t& ixs = p.second;
    (detail::fupdate_by<table_t,ss_table_t,Fs,Ss>(t_,t_ss, fs, ixs), ...);
  }

  return t_.template remove<ss_t>().add(t_ss).template select<ts_t>();
}

}// namespace table
