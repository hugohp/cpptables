// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cstdint>
#include <cassert>
#include <numeric>
#include <set>
#include <functional>
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/defines.hh"
#include "cpptables/traits.hh"
#include "cpptables/strings/string_storage.hh"

namespace tables
{

// ==================================== constructor / assignment  =================================

// Empty constructor
template<is_primitive_type P>
inline
mm_column_impl_base<P>::mmcolumn_base(const std::filesystem::path path)
{
  const bool file_exists = std::filesystem::exists(path);
  n_ = file_exists ? std::filesystem::file_size(file_path) : 0;
  if ( ! file_exists )
  {
    std::ofstream ofs(file_path);
    assert( ofs.is_open() && ofs.rdstate() == std::ios::goodbit);
    ofs.close();
  }

  const int fd = open( path_.c_str(), O_CREAT| O_RDWR,S_IRWXU|S_IRUSR);
  addr_ = mmap(NULL,prev_file_len+write_len,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  close(fd);
}

// ==================================== size / count / n_nones  =================================

template<is_primitive_type P>
inline
std::size_t mm_column_impl_base<P>::size() const
{
  return n_;
}

template<is_primitive_type P>
inline
std::size_t mm_column_impl_base<P>::count() const
{
  return n_;
}

template<is_primitive_type P>
inline
std::size_t mm_column_impl_base<P>::n_nones() const
{
  const std::size_t n = size();
  std::size_t r = 0;
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    const prim_t vi = addr_[i];
    r += prim_traits<prim_t>::is_none(vi) ? 1 : 0;
  }
  return r;
}

// ==================================== operator[] / at =================================

template<is_primitive_type P>
inline
P
mm_column_impl_base<P>::operator[](const std::size_t i) const
{
  return addr_[i];
}

template<is_primitive_type P>
inline
column<P>
mm_column_impl_base<P>::at(const vixs_t& ixs) const
{
  const std::size_t n = ixs.size();
  vector_t dst(n);

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const std::size_t ix = ixs[i];
    assert (ix == null_ix || ix < v_.size());
    const prim_t val = (ix == null_ix) ? none : v_[ix];
    dst[i] = val;
  }

  return std::column<P>(std::move(dst);
}

// ==================================== take/drop =================================

template<is_primitive_type P>
inline
column<P>
mm_column_impl_base<P>::take(const std::size_t n) const
{
  const std::size_t n1 = std::min(n, n_);
  vector_t dst(n1);

  std::copy(v_,v_ + n1,dst.begin());
  return column<P>(std::move(dst));
}

template<is_primitive_type P>
inline
column<P>
mm_column_impl_base<P>::drop(const std::size_t n) const
{
  const std::size_t n1 = (n > n_) ? 0U : (n_-n);
  vector_t dst(n1);

  std::copy(v_ + std::min(n,n_),v_ + n_,dst.begin());
  return column<P>(std::move(dst));
}

// ==================================== rtake/rdrop =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
mm_column_impl_base<P>::rtake(const std::size_t n) const
{
  const std::size_t n1 = std::min(n,n_);
  vector_t dst(n1);

  std::copy(v_ + (n_-n1),v_+n_,dst.begin());
  return column<P>(std::move(dst));
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
mm_column_impl_base<P>::rdrop(const std::size_t n) const
{
  const std::size_t n1 = (n > n_) ? 0U : n_-n;
  vector_t dst(n1);

  std::copy(v_,v_-n1,dst.begin());
  return column<P>(std::move(dst));
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline void mm_column_impl_base<P>::_append(const prim_t val)
{
  std::filesystem::resize_file(path_,n_+1);

  const int fd = open( path_.c_str(), O_CREAT| O_RDWR,S_IRWXU|S_IRUSR);
  addr_ = mmap(NULL,n_+1,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  addr[n_]= val;
  n_ += 1;
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline void mm_column_impl_base<P>::_append(const std::size_t n,const prim_t val)
{
  std::filesystem::resize_file(path_,n_+n);

  const int fd = open( path_.c_str(), O_CREAT| O_RDWR,S_IRWXU|S_IRUSR);
  addr_ = mmap(NULL,n_+n,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  std::fill(addr_+n_,addr_+n_+n,val);
  n_ += n; 
}

// ==================================== _append (column) =================================

template<is_primitive_type P>
inline 
void 
mm_column_impl_base<P>::_append(const column<P>& other)
{
  const std::size_t n = other.size();
  std::filesystem::resize_file(path_,n_+n);

  const int fd = open( path_.c_str(), O_CREAT| O_RDWR,S_IRWXU|S_IRUSR);
  addr_ = mmap(NULL,n_ + n,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
  std::copy(add_ + n_, addr_ + n_ + n,other.v().data());
  n_ += n; 
}

// ==================================== _apply =================================

template<is_primitive_type P>
template<typename F>
inline
void
mm_column_impl_base<P>::_apply(F f) const
{
  const std::size_t n = size();

  for ( std::size_t i = 0; i < n ; ++i )
  {
    addr_[i] = f(addr_[i]);
  }
}

// ==================================== max/min =================================

template<is_primitive_type P>
inline
mm_column_impl_base<P>::prim_t 
mm_column_impl_base<P>::max() const
{
  const std::size_t n = size();
  if ( n == 0 ) return prim_traits<prim_t>::none;

  prim_t r = addr_[0];
  for ( std::size_t i = 1; i < n ; ++i )
  {
    const prim_t vi = addr_[i];
    r = prim_traits<prim_t>::is_none(vi) ? r : std::max(r,vi);
  }
  return r;
}

template<is_primitive_type P>
inline
mm_column_impl_base<P>::prim_t 
mm_column_impl_base<P>::min() const
{
  const std::size_t n = size();
  if ( n == 0 ) return prim_traits<prim_t>::none;

  prim_t r = addr_[0];
  for ( std::size_t i = 1; i < n ; ++i )
  {
    const prim_t vi = addr_[i];
    r = prim_traits<prim_t>::is_none(vi) ? r : std::min(r,vi);
  }
  return r;
}

// ==================================== all =================================

template<is_primitive_type P>
inline
bool 
mm_column_impl_base<P>::all() const
{
  for ( std::size_t i = 0 ; i < size() ; ++ i)
  {
    const prim_t vi = addr_[i];
    if (vi == prim_traits<prim_t>::zero || prim_traits<prim_t>::is_none(vi) ) return false;
  }
  return true;
}

// ==================================== any =================================

template<is_primitive_type P>
inline
bool 
mm_column_impl_base<P>::any() const
{
  for ( std::size_t i = 0 ; i < size() ; ++ i)
  {
    const prim_t vi = addr_[i];
    if (vi != prim_traits<prim_t>::zero && ! prim_traits<prim_t>::is_none(vi) ) return true;
  }
  return false;
}

// ==================================== sum =================================

template<is_arithmetic_type P>
inline
P
mm_column_impl_base<P>::sum() const
{
  const std::size_t n = size();
  if ( n == 0 ) return prim_traits<prim_t>::zero;

  prim_t r = prim_traits<prim_t>::zero;
  for ( std::size_t i = 0; i < n ; ++i )
  {
    r += column_impl_base_t::_zat(i);
  }
  return r;
}
} // namespace tables
