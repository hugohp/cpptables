// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <vector>
#include <ostream>
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/table.hh"
#include "cpptables/type_functions.hh"
#include <tuple_ext/tuple_ext.h>
#include "cpptables/impl/io/show_table.hh"

namespace tables
{

// Construct from table
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable(const table_t& t)
{
   std::map<grow_t,vixs_t> gixs = t.template indices_by<gs_t>();

   for ( const auto& p : gixs )
   {
      m_.insert( { p.first , t.template select<ngs_t>().at(p.second) } );
   }
}

// Construct from map_t
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable(map_t&& m)
  : m_(std::forward<map_t>(m))
{}

// Copy constructor
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable(
  const gtable_t& other
)
  : m_(other.m_)
{}

// Copy assignment operator
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>&
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::operator=(
  const gtable_t& other
)
{
  if ( this != &other )
  {
    m_ = other.m_;
  }
  return *this;
}

// Move constructor
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable(
  gtable_t&& other
)
  : m_(std::forward<map_t>(other.m_))
{}

// Move assignment operator
template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>&
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::operator=(
  gtable_t&& other
)
{
  m_ = std::forward<map_t>(other.m_);
  return *this;
}

// ==================================== n_groups =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
std::size_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::n_groups() const
{
  return m_.size();
}

// ==================================== m =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::map_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::m() const
{
  return m_;
}

// ==================================== contains =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
bool
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::contains(
  const grow_t& g
) const
{
  return m_.contains(g);
}

// ==================================== get =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::ng_table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::get(
  const grow_t& g
) const
{
  return m_.find(g)->second;
}

// ==================================== take/drop =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::take(const std::size_t n) const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.take(n) });
  }
  return gtable(std::move(m));
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::drop(const std::size_t n) const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.drop(n) });
  }
  return gtable(std::move(m));
}

// ==================================== rtake/rdrop =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::rtake(const std::size_t n) const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.rtake(n) });
  }
  return gtable(std::move(m));
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::rdrop(const std::size_t n) const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.rdrop(n) });
  }
  return gtable(std::move(m));
}

// ==================================== ungroup =================================

namespace detail
{
  template <typename... Ts,is_primitive_type... Ps>
  void copy_subtable(
    const table<std::pair<Ts,Ps>...>& src,
    table<std::pair<Ts,Ps>...>&& dst,
    const std::size_t irow
  )
  {
    const std::size_t n =  src.nrows();
    (
      std::copy(
        &src.template col<Ts>().v()[0],
        &src.template col<Ts>().v()[n],
        &dst.template _col<Ts>()._v()[irow]
      ),...
    );
  }
} // namespace detail

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::ungroup() const
{
  // Total number of rows
  std::size_t nrows = 0;
  for ( const auto& p : m_ )
  {
    nrows += p.second.nrows();
  }

  mtuple<std::pair<Ts,column<Ps>>...> tm;
  (tm.template _get<Ts>()._v().resize(nrows),...);
  table_t t(tm);

  std::size_t irow = 0;
  // Fill Gs... columns
  for ( const auto& p : m_ )
  {
    const std::size_t n = p.second.nrows();
    const grow_t& k = p.first;
    (
      std::fill(
        &t.template _col<Gs>()._v()[irow],
        &t.template _col<Gs>()._v()[irow+n],
        k.template col<Gs>()
      ),...
    );
    irow += n;
  }

  // Copy (non-Gs) columns
  irow = 0;
  for ( const auto& p : m_ )
  {
    const ng_table_t& ngt = p.second;
    detail::copy_subtable(ngt, t.template select<ngs_t>(), irow);
    irow += p.second.nrows();
  }

  return t;
}

// ==================================== reverse =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::reverse() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.reverse() } );
  }
  return gtable(std::move(m));
}

// ==================================== fills =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::fills() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template fills() } );
  }
  return gtable(std::move(m));
}

// ==================================== fill_with =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::fill_with(const ngrow_t& row) const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template fill_with(row) } );
  }
  return gtable(std::move(m));
}

// ==================================== zfill =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::zfill() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template zfill() } );
  }
  return gtable(std::move(m));
}

// ==================================== deltas =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::deltas() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template deltas() } );
  }
  return gtable(std::move(m));
}

// ==================================== distinct =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::distinct() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template distinct() } );
  }
  return gtable(std::move(m));
}

// ==================================== sort =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::sort() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.sort() } );
  }
  return gtable(std::move(m));
}

// ==================================== sort_by =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
template<typename... Ss>
requires (
  ( in<Ss,tuple_ext::remove_t<std::tuple<Gs...>,std::tuple<Ts...>>> && ... ) &&
  is_tuple_unique<std::tuple<Ss...>>
)
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::sort_by() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template sort_by<Ss...>() } );
  }
  return gtable(std::move(m));
}

// ==================================== rsort_by =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
template<typename... Ss>
requires (
  ( in<Ss,tuple_ext::remove_t<std::tuple<Gs...>,std::tuple<Ts...>>> && ... ) &&
  is_tuple_unique<std::tuple<Ss...>>
)
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::rsort_by() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.template rsort_by<Ss...>() } );
  }
  return gtable(std::move(m));
}

// ==================================== abs =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::gtable_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::abs() const
{
  map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.abs() } );
  }
  return gtable(std::move(m));
}

// ==================================== log / log2 / log10  =================================

namespace detail 
{
  template <is_gtable G>
  struct convert_to_doubles;

  template <typename... Ts,is_primitive_type... Ps,typename... Gs>
  struct convert_to_doubles<gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>>
  {
    using gtable_t = gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>;

    using ts_t = typename gtable_t::ts_t;

    using gs_t = typename gtable_t::gs_t;
    using pgs_t = typename gtable_t::pgs_t;

    using ngs_t = typename gtable_t::ngs_t;
    using pngs_t = tuple_ext::repeat_t<std::tuple_size_v<ngs_t>,double>;

    using r_table_t = typename as_table_t<
      tuple_ext::concat_t<gs_t,ngs_t>,
      tuple_ext::concat_t<pgs_t,pngs_t>
    >::select_t<ts_t>;

    using r_gtable_t = gtable<r_table_t,gs_t>;
  };
} // namespace detail

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
auto
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::log() const
{
  using r_gtable_t = typename detail::convert_to_doubles<gtable_t>::r_gtable_t;
  using r_map_t = typename r_gtable_t::map_t;

  r_map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.log() } );
  }
  return r_gtable_t(std::move(m));
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
auto
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::log2() const
{
  using r_gtable_t = typename detail::convert_to_doubles<gtable_t>::r_gtable_t;
  using r_map_t = typename r_gtable_t::map_t;

  r_map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.log2() } );
  }
  return r_gtable_t(std::move(m));
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
auto
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::log10() const
{
  using r_gtable_t = typename detail::convert_to_doubles<gtable_t>::r_gtable_t;
  using r_map_t = typename r_gtable_t::map_t;

  r_map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.log10() } );
  }
  return r_gtable_t(std::move(m));
}

// ==================================== exp =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
auto
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::exp() const
{
  using r_gtable_t = typename detail::convert_to_doubles<gtable_t>::r_gtable_t;
  using r_map_t = typename r_gtable_t::map_t;

  r_map_t m;
  for ( const auto& p : m_ )
  {
    m.insert( { p.first, p.second.exp() } );
  }
  return r_gtable_t(std::move(m));
}

// ==================================== make_agg_table =================================

namespace detail
{
  template<is_primitive_type P>
  inline void copy_elem(const P val,const std::size_t irow, column<P> dst)
  {
    dst._v()[irow] = val;
  }
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::make_agg_table() const
{
  const std::size_t n = m_.size();
  mtuple<std::pair<Ts,column<Ps>>...> tm;
  (tm.template _get<Ts>()._v().resize(n),...);
  table_t t(tm);

  std::size_t irow = 0;
  for ( const auto& p : m_)
  {
    ( detail::copy_elem(p.first.template col<Gs>(), irow, t.template _col<Gs>()), ... );
    ++irow;
  }
  return t;
}

// ==================================== max/min =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::max() const
{
  table_t t = make_agg_table();

  std::size_t irow = 0;
  for ( const auto& p : m_ )
  {
    const ngrow_t ngrow = p.second.max();
    t.template select<ngs_t>()._set_row(irow, ngrow);
    ++irow;
  }
  return t;
}

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::min() const
{
  table_t t = make_agg_table();

  std::size_t irow = 0;
  for ( const auto& p : m_ )
  {
    const ngrow_t ngrow = p.second.min();
    t.template select<ngs_t>()._set_row(irow, ngrow);
    ++irow;
  }
  return t;
}

  // ==================================== sum =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::sum() const
{
  table_t t = make_agg_table();

  std::size_t irow = 0;
  for ( const auto& p : m_ )
  {
    const ngrow_t ngrow = p.second.sum();
    t.template select<ngs_t>()._set_row(irow, ngrow);
    ++irow;
  }
  return t;
}

// ==================================== avg =================================

template <typename... Ts,is_primitive_type... Ps,typename... Gs>
typename gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::table_t
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>::avg() const
{
  table_t t = make_agg_table();

  std::size_t irow = 0;
  for ( const auto& p : m_ )
  {
    const ngrow_t ngrow = p.second.avg();
    t.template select<ngs_t>()._set_row(irow, ngrow);
    ++irow;
  }
  return t;
}


// ==================================== operator== =================================

template <is_gtable GT>
bool operator==(const GT& lhs, const GT& rhs)
{
  return ( lhs.t() == rhs.t() );
}

// ==================================== operator<< =================================

template <is_table T,tuple_ext::is_tuple G>
requires ( is_gtable<gtable<T,G>> )
std::ostream& operator<< (std::ostream& os, const gtable<T,G>& gt)
{
  using namespace impl::io;

  using ts_t = typename T::ts_t;
  using ps_t = typename T::ps_t;

  using gs_t = G; // group columns
  using pgs_t = many_snd_elem_at_t<ts_t,ps_t,gs_t>; // primitive-types for group columns

  using ngs_t = tuple_ext::remove_t<gs_t,ts_t>; // non-group columns
  using pngs_t = many_snd_elem_at_t<ts_t,ps_t,ngs_t>; // primitive-types for non-group columns

  const auto t = gt.ungroup();

  const auto lhs = t.template select<gs_t>();
  const auto rhs = t.template select<ngs_t>();

  const std::size_t nrows = t.nrows();

  const auto lhs_css = make_many_column_string( lhs );
  const auto rhs_css = make_many_column_string( rhs );

  header_display<gs_t,pgs_t>::run(os, lhs_css);
  os << "|";
  header_display<ngs_t,pngs_t>::run(os, rhs_css);
  os << "\n";

  line_display<gs_t,pgs_t>::run(os, lhs_css);
  os << "|";
  line_display<ngs_t,pngs_t>::run(os, rhs_css);
  os << "\n";

  for ( std::size_t i = 0 ; i < nrows ; ++i )
  {
    row_display<gs_t,pgs_t>::run(os, i, lhs_css);
    os << "|";
    row_display<ngs_t,pngs_t>::run(os, i, rhs_css);
    os << "\n";
  }
  return os;
}

} // namespace tables
