// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <cstdint>
#include <cassert>
#include <numeric>
#include <set>
#include <functional>
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/defines.hh"
#include "cpptables/traits.hh"
#include "cpptables/strings/string_storage.hh"

namespace tables
{

// ==================================== constructor / assignment  =================================

// Empty constructor
template<is_primitive_type P>
inline
column_impl_base<P>::column_impl_base()
{}

// Constructor with initializer list
template<is_primitive_type P>
inline
column_impl_base<P>::column_impl_base(std::initializer_list<prim_t> l)
  : v_(l)
{}

// Constructor with vector
template<is_primitive_type P>
inline
column_impl_base<P>::column_impl_base(const vector_t& v)
 : v_(v)
{}

// Constructor with vector_t (rvalue)
template<is_primitive_type P>
inline
column_impl_base<P>::column_impl_base(vector_t&& v)
  : v_(std::forward<vector_t>(v))
{}

// ==================================== size / count  =================================

template<is_primitive_type P>
inline
std::size_t column_impl_base<P>::size() const
{
  return v_.size();
}

template<is_primitive_type P>
inline
std::size_t column_impl_base<P>::count() const
{
  return v_.size();
}

// ==================================== v / _v =================================

template<is_primitive_type P>
inline
const column_impl_base<P>::vector_t& 
column_impl_base<P>::v() const
{
  return v_;
}

template<is_primitive_type P>
inline
column_impl_base<P>::vector_t& 
column_impl_base<P>::_v() 
{
  return v_;
}

// ==================================== n_nones =================================

template<is_primitive_type P>
inline
std::size_t 
column_impl_base<P>::n_nones() const
{
  const std::size_t n = size();
  std::size_t r = 0;
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    const prim_t vi = v_[i];
    r += prim_traits<prim_t>::is_none(vi) ? 1 : 0;
  }
  return r;
}

// ==================================== at =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::at(const vixs_t& ixs) const
{
  const std::size_t n = ixs.size();
  vector_t dst(n);

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const std::size_t ix = ixs[i];
    assert (ix == null_ix || ix < v_.size());
    const prim_t val = (ix == null_ix) ? none : v_[ix];
    dst[i] = val;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

  // ==================================== except =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::except(const column_impl_t& xs) const
{
  const std::size_t n = v_.size();

  vector_t res;
  res.reserve(n);

  vector_t sxs(xs.v());
  std::sort(sxs.begin(), sxs.end());

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const auto it = std::lower_bound(sxs.begin(), sxs.end(), vi);
    const bool not_found = (it == sxs.end()) || (*it != vi);
    if ( not_found )
    {
      res.push_back(vi);
    }
  }

  return std::make_shared<column_impl_t>(std::move(res));
}
 
  // ==================================== append (scalar) =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::append(const prim_t& val) const
{
  const std::size_t n = size();

  vector_t dst(v_);
  dst.resize(n+1,val);

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline void column_impl_base<P>::_append(const prim_t val)
{
  const std::size_t n = size();
  v_.resize(n+1);
  v_[n]= val;
}

// ==================================== _append (scalar) =================================

template<is_primitive_type P>
inline void column_impl_base<P>::_append(const std::size_t n,const prim_t val)
{
  v_.insert(v_.end(), n, val);
}

// ==================================== _append (column) =================================

template<is_primitive_type P>
inline 
void 
column_impl_base<P>::_append(const column_impl<prim_t>& other)
{
  v_.insert(v_.end(), other.v().begin(), other.v().end());
}

// ==================================== append (column) =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::append(const column_impl<prim_t>& other) const
{
  const std::size_t n0 = v_.size();
  const std::size_t n1 = other.size();
  const std::size_t n = (n0 + n1);

  vector_t dst(n);

  const auto it = std::copy(v_.begin(), v_.end(), dst.begin());
  std::copy(other.v_.begin(), other.v_.end(), it);
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== take/drop =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::take(const std::size_t n) const
{
  const std::size_t n1 = std::min(n, v_.size());
  vector_t dst(n1);

  typename vector_t::const_iterator it_end = v_.cbegin();
  std::advance(it_end,n1);

  std::copy(v_.cbegin(), it_end, dst.begin());
  return std::make_shared<column_impl_t>(std::move(dst));
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::drop(const std::size_t n) const
{
  const std::size_t sz = v_.size();
  const std::size_t n1 = (n>sz) ? 0U : (sz-n);
  vector_t dst(n1);

  typename vector_t::const_iterator it_begin = v_.cbegin();
  std::advance(it_begin,std::min(n,sz));

  std::copy(it_begin,v_.end(),dst.begin());
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== rtake/rdrop =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::rtake(const std::size_t n) const
{
  const std::size_t sz = v_.size();
  const std::size_t n1 = std::min(n,sz);
  vector_t dst(n1);

  typename vector_t::const_iterator it_begin = v_.cbegin();
  std::advance(it_begin, (sz - n1));

  std::copy(it_begin,v_.end(),dst.begin());
  return std::make_shared<column_impl_t>(std::move(dst));
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::rdrop(const std::size_t n) const
{
  const std::size_t sz = v_.size();
  const std::size_t n1 = (n > sz) ? 0U : sz-n;
  vector_t dst(n1);

  typename vector_t::const_iterator it_end = v_.cbegin();
  std::advance(it_end,n1);

  std::copy(v_.begin(),it_end,dst.begin());
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== apply =================================

template<is_primitive_type P>
template<typename F>
inline
auto 
column_impl_base<P>::apply(F f) const
{
  using P1 = std::invoke_result_t<F,prim_t>;
  const std::size_t n = size();
  std::vector<P1> dst(n);

  for ( std::size_t i = 0; i < n ; ++i )
  {
    dst[i] = f(v_[i]);
  }

  return std::make_shared<column_impl<P1>>(column_impl<P1>(std::move(dst)));
}

// ==================================== cast_to =================================

template<is_primitive_type P>
template<is_primitive_type P1>
inline
auto 
column_impl_base<P>::cast_to() const
{
  std::vector<P1> dst(v_.size());
  for ( std::size_t i = 0 ; i < v_.size() ; ++i )
  {
    const prim_t vi = v_[i];
    dst[i] = (prim_traits<prim_t>::is_none(vi)) ? prim_traits<P1>::none : static_cast<P1>(vi);
  }
  return std::make_shared<column_impl<P1>>(std::move(dst));
}

  // ==================================== reverse =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::reverse() const
{
  const std::size_t n = v_.size();
  vector_t dst(n);

  std::copy(v_.rbegin(), v_.rend(), dst.begin());
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== fills =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::fills() const
{
  const std::size_t n = v_.size();
  vector_t dst(n);

  if ( n > 0 )
  {
    dst[0] = v_[0];
    for ( std::size_t i = 1; i < n ; ++i )
    {
      const prim_t vi = v_[i];
      const prim_t vi1 = dst[i-1];
      const bool is_none = prim_traits<prim_t>::is_none(vi);
      dst[i] = is_none ? vi1 : vi;
    }
 }
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== fill_with (scalar) =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::fill_with(const prim_t val) const
{
  const std::size_t n = v_.size();
  vector_t dst(n);

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const bool is_none = prim_traits<prim_t>::is_none(vi);
    dst[i] = is_none ? val : vi;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== fill_with (column) =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::fill_with(const column_impl_t c) const
{
  const std::size_t n = v_.size();
  if (c.size() != size())
  {
    throw std::length_error(__FUNCTION__);
  }
  vector_t dst(n);

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const prim_t ci = c.v()[i];
    const bool is_none = prim_traits<prim_t>::is_none(vi);
    dst[i] = is_none ? ci : vi;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}
 
// ==================================== zfill =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::zfill() const
{
  const std::size_t n = v_.size();
  vector_t dst(n);
 
  constexpr prim_t zero = prim_traits<prim_t>::zero; 

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const bool is_none = prim_traits<prim_t>::is_none(vi);
    dst[i] = is_none ? zero : vi;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

  // ==================================== replace =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::replace(const column_impl_t& c) const
{
  if (c.size() != size())
  {
    throw std::length_error(__FUNCTION__);
  }

  const std::size_t n = c.size();
  vector_t dst(n);
 
  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const prim_t ci = c.v()[i];
    const bool is_none = prim_traits<prim_t>::is_none(ci);
    dst[i] = is_none ? vi : ci;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== differ =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::differ() const
{
  const std::size_t n = v_.size();
  std::vector<bool> dst(n);

  if ( n != 0 )
  {
    dst[0] = true;
    for ( std::size_t i = 1; i < n ; ++i )
    {
      const prim_t vi1 = v_[i-1];
      const prim_t vi = v_[i];
      dst[i] = (vi == vi1) ? false : true;
    }
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== is_none =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::is_none() const
{
  const std::size_t n = v_.size();
  std::vector<bool> dst(n);

  if ( n != 0 )
  {
    for ( std::size_t i = 0; i < n ; ++i )
    {
      const prim_t vi = v_[i];
      dst[i] = prim_traits<prim_t>::is_none(vi) ? true : false;
    }
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== distinct =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::distinct() const
{
  const std::size_t n = v_.size();
  vector_t dst;
  dst.reserve(n);

  std::set<prim_t> s;

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    const bool is_ok = !prim_traits<prim_t>::is_none(vi);
    if ( is_ok && (s.find(vi) == s.end()) ) 
    {
      dst.push_back(vi);
      s.insert(vi);
    }
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== sort =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>>
column_impl_base<P>::sort() const
{
  vector_t dst(v_);
  std::sort(dst.begin(), dst.end());

  return std::make_shared<column_impl_t>(std::move(dst));
}


// ==================================== rsort =================================
 
template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::rsort() const
{
  vector_t dst(v_);
  std::sort(dst.rbegin(), dst.rend());

  return std::make_shared<column_impl_t>(std::move(dst));
}
 
// ==================================== mins/maxs =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::mins() const
{
  const std::size_t n = size();

  vector_t dst(n);
  if ( n > 0 )
  {
    prim_t minv = _zat(0);
    dst[0] = minv;
    for ( std::size_t i = 1 ; i < n ; ++i )
    {
      const prim_t vi = _zat(i);
      minv = std::min(minv,vi);
      dst[i] = minv;
    }
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}
 
template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::maxs() const
{
  const std::size_t n = size();

  vector_t dst(n);
  if ( n > 0 )
  {
    prim_t maxv = _zat(0);
    dst[0] = maxv;
    for ( std::size_t i = 1 ; i < n ; ++i )
    {
      const prim_t vi = _zat(i);
      maxv = std::max(maxv,vi);
      dst[i] = maxv;
    }
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}
 
// ==================================== mmin/mmax =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::mmin(const std::size_t m) const
{
  vector_t dst(v_);

  const std::size_t n = size();

  if ( m == 0 )
  {
    std::copy(v_.begin(), v_.end(), dst.begin());
  }
  else
  {
    for ( std::size_t i = 0 ; i < n ; ++i )
    {
      const std::size_t i_start = (i >= m) ? (i+1-m) : 0;
      const std::size_t mn = i+1-i_start;
      prim_t minv = _zat(i_start);
      for ( std::size_t j = 1 ; j < mn ; ++j )
      {
        const prim_t vj = _zat(j+i_start);
        minv = std::min(minv,vj);
      }
      dst[i] = minv;
    }
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}
 
template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::mmax(const std::size_t m) const
{
  vector_t dst(v_);

  const std::size_t n = size();

  if ( m == 0 )
  {
    std::copy(v_.begin(), v_.end(), dst.begin());
  }
  else
  {
    for ( std::size_t i = 0 ; i < n ; ++i )
    {
      const std::size_t i_start = (i >= m) ? (i+1-m) : 0;
      const std::size_t mn = i+1-i_start;
      prim_t maxv = _zat(i_start);
      for ( std::size_t j = 1 ; j < mn ; ++j )
      {
        const prim_t vj = _zat(j+i_start);
        maxv = std::max(maxv,vj);
      }
      dst[i] = maxv;
    }
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== next/prev =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::next() const
{
  const std::size_t n = size();
  vector_t dst(n);

  for ( std::size_t i = 0 ; i < (n-1) ; ++i )
  {
    dst[i] = v_[i+1];
  }
  if ( n > 0 )
  { 
    dst[n-1] = prim_traits<prim_t>::none;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl_base<P>::prev() const
{
  const std::size_t n = size();
  vector_t dst(n);

  for ( std::size_t i = 1 ; i < n ; ++i )
  {
    dst[i] = v_[i-1];
  }
  if ( n > 0 )
  { 
    dst[0] = prim_traits<prim_t>::none;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}
 


// ==================================== max/min =================================

template<is_primitive_type P>
inline
column_impl_base<P>::prim_t 
column_impl_base<P>::max() const
{
  const std::size_t n = size();
  if ( n == 0 ) return prim_traits<prim_t>::none;

  prim_t r = v_[0];
  for ( std::size_t i = 1; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    r = prim_traits<prim_t>::is_none(vi) ? r : std::max(r,vi);
  }
  return r;
}

template<is_primitive_type P>
inline
column_impl_base<P>::prim_t 
column_impl_base<P>::min() const
{
  const std::size_t n = size();
  if ( n == 0 ) return prim_traits<prim_t>::none;

  prim_t r = v_[0];
  for ( std::size_t i = 1; i < n ; ++i )
  {
    const prim_t vi = v_[i];
    r = prim_traits<prim_t>::is_none(vi) ? r : std::min(r,vi);
  }
  return r;
}


// ==================================== all =================================

template<is_primitive_type P>
inline
bool 
column_impl_base<P>::all() const
{
  for ( std::size_t i = 0 ; i < v_.size() ; ++ i)
  {
    const prim_t vi = v_[i];
    if (vi == prim_traits<prim_t>::zero || prim_traits<prim_t>::is_none(vi) ) return false;
  }
  return true;
}

// ==================================== any =================================

template<is_primitive_type P>
inline
bool 
column_impl_base<P>::any() const
{
  for ( std::size_t i = 0 ; i < v_.size() ; ++ i)
  {
    const prim_t vi = v_[i];
    if (vi != prim_traits<prim_t>::zero && ! prim_traits<prim_t>::is_none(vi) ) return true;
  }
  return false;
}

// ==================================== cross =================================

template<is_primitive_type P>
template<is_primitive_type P1>
inline
std::pair<shared_ptr<column_impl<P>>,shared_ptr<column_impl<P1>>> 
column_impl_base<P>::cross(const column_impl<P1>& other) const
{
  const std::size_t n = size();
  const std::size_t m = other.size();
  const std::size_t nm = n * m;

  std::vector<prim_t> lhs(nm);
  std::vector<P1> rhs(nm);

  for (std::size_t i = 0 ; i < n ; ++i)
  {
    for (std::size_t j = 0 ; j < m ; ++j)
    {
      lhs[i*m+j] = v_[i];
      rhs[i*m+j] = other.v()[j];
    }
  }
  return {
    std::make_shared<column_impl<prim_t>>(std::move(lhs)),
    std::make_shared<column_impl<P1>>(std::move(rhs))
  };
}

// ==================================== col_comp_col =================================

namespace col_impl_base_detail
{

template<is_primitive_type P,typename F>
requires ( 
  std::invocable<F,P,P> && 
  std::is_same_v<std::invoke_result_t<F,P,P>,bool>
)
inline 
std::shared_ptr<column_impl<bool>>
col_comp_col(const column_impl_base<P>& lhs, const column_impl_base<P>& rhs, const F f)
{
  assert ( lhs.size() == rhs.size() );

  using vector_bool = typename column_impl<bool>::vector_t;
  const std::size_t n = lhs.size();

  vector_bool dst(n);

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const P lhs_val = lhs.v()[i];
    const P rhs_val = rhs.v()[i];

    dst[i] = f(lhs_val,rhs_val) ? true : false;
  }
  return std::make_shared<column_impl<bool>>(std::move(dst));
}


// ==================================== col_comp_scalar =================================

template<is_primitive_type P,typename F>
requires ( 
  std::invocable<F,P,P> && 
  std::is_same_v<std::invoke_result_t<F,P,P>,bool>
)
inline
std::shared_ptr<column_impl<bool>>
col_comp_scalar(const column_impl_base<P>& col, const P cte, const F f)
{
  const std::size_t n = col.size();

  using vector_bool = typename column_impl<bool>::vector_t;
  vector_bool dst(n);

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const P vi = col.v()[i];
    dst[i] = f(vi,cte);
  }
  return std::make_shared<column_impl<bool>>(std::move(dst));
}

} // namespace col_impl_base_detail

// ==================================== eq =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::eq(const column_impl_t& rhs) const
{
  if (size() != rhs.size() )
  {
    throw std::length_error(__FUNCTION__);
  }

  const auto f = [](const P v0, const P v1) { return (v0 == v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::eq(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 == v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== ne =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::ne(const column_impl_t& rhs) const
{
  assert( size() == rhs.size() );

  const auto f = [](const P v0, const P v1) { return (v0 != v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::ne(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 != v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== lt =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::lt(const column_impl_t& rhs) const
{
  assert( size() == rhs.size() );

  const auto f = [](const P v0, const P v1) { return (v0 < v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::lt(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 < v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== le =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::le(const column_impl_t& rhs) const
{
  assert( size() == rhs.size() );

  const auto f = [](const P v0, const P v1) { return (v0 <= v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::le(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 <= v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== gt =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::gt(const column_impl_t& rhs) const
{
  assert( size() == rhs.size() );

  const auto f = [](const P v0, const P v1) { return (v0 > v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::gt(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 > v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== ge =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::ge(const column_impl_t& rhs) const
{
  assert( size() == rhs.size() );

  const auto f = [](const P v0, const P v1) { return (v0 >= v1); };
  return col_impl_base_detail::col_comp_col(*this, rhs, f);
}

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::ge(const P cte) const
{
  const auto f = [](const P v0, const P v1) { return (v0 >= v1); };
  return col_impl_base_detail::col_comp_scalar(*this, cte,f);
}

// ==================================== in =================================

template<is_primitive_type P>
inline
std::shared_ptr<column_impl<bool>> 
column_impl_base<P>::in(const column_impl_t& other) const
{
  using vector_bool = typename column_impl<bool>::vector_t;
  const std::size_t n = size();

  vector_bool dst(n);
  const std::set<P> elems(other.v().cbegin(), other.v().cend());

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const P vi = v()[i];
    dst[i] = (elems.find(vi) != elems.end());
  }
  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== _zat =================================

template<is_primitive_type P>
inline
P column_impl_base<P>::_zat(const std::size_t i) const
{
  const prim_t vi = v_[i];
  return prim_traits<prim_t>::is_none(vi) ? prim_traits<prim_t>::zero : vi;
}

// ==================================== deltas =================================

template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl<P>::deltas() const
{
  const std::size_t n = column_impl_base_t::v_.size();
  vector_t dst(n);

  if ( n > 0 )
  {
    dst[0] = column_impl_base_t::v_[0];
    for ( std::size_t i = 1; i < n ; ++i )
    {
      const prim_t vi1 = column_impl_base_t::v_[i-1];
      const prim_t vi = column_impl_base_t::v_[i];
      const bool is_none = prim_traits<prim_t>::is_none(vi1) || prim_traits<prim_t>::is_none(vi);
      dst[i] = is_none ? prim_traits<prim_t>::none : (vi - vi1);
    }
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== xbar =================================

template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl<P>::xbar(const prim_t val) const
{
  constexpr static prim_t zero = prim_traits<prim_t>::zero;
  const std::size_t n = column_impl_base_t::size();

  if ( prim_traits<prim_t>::is_none(val) || (val == zero) )
  {
    const std::vector<prim_t> v(n, prim_traits<prim_t>::none );
    return std::make_shared<column_impl_t>(v);
  }
  std::vector<prim_t> res(n);

  if constexpr (std::numeric_limits<prim_t>::is_integer )
  {
    for ( std::size_t i = 0 ; i < n ; ++i)
    {
      const prim_t vi = column_impl_base_t::v_[i];
      res[i] = (vi/val)*val;
    }
    // a/b does a truncation but we need flooring.
    // Truncation and flooring are the same for positive numbers, not for negative numbers.
    // Example 
    // * trunc(5/2) = 2 and floor(5/2)=2
    // * trunc(-5/2) = -2 and floor(-5/2)=-3
    for ( std::size_t i = 0 ; i < n ; ++i)
    {
      const prim_t vi = column_impl_base_t::v_[i];
      prim_t ri = res[i];
      // if vi and val have opposite signs and vi is not multiple of val, then subtract val from result
      const bool decr = (vi*val < 0) && (ri != vi);
      ri -= val*decr;
      res[i] = ri;
    }
  }
  else
  {
    for ( std::size_t i = 0 ; i < n ; ++i)
    {
      const prim_t vi = column_impl_base_t::v_[i];
      const prim_t nvi = std::floor(vi/val)*val;
      res[i] = nvi;
    }
  }

  // Nones should stay nones
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    const prim_t vi = column_impl_base_t::v_[i];
    const prim_t ri = res[i];
    res[i] = prim_traits<prim_t>::is_none(vi) ? prim_traits<prim_t>::none : ri;
  }
 
  return std::make_shared<column_impl_t>(std::move(res));
}
 
// ==================================== sums =================================
 
template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl<P>::sums() const
{
  vector_t dst(column_impl_base_t::v_.size());
  prim_t soma = prim_traits<prim_t>::zero;
  for ( std::size_t i = 0 ; i < dst.size() ; ++i )
  {
    soma += column_impl_base_t::_zat(i);
    dst[i] = soma;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}
 
// ==================================== msum =================================
 
template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl<P>::msum(const std::size_t m) const
{
  constexpr static prim_t zero = prim_traits<prim_t>::zero;
  vector_t dst(column_impl_base_t::v_.size());

  const std::size_t n = column_impl_base_t::size();

  if ( m == 0 )
  {
    dst.resize(n,zero);
  }
  else
  {
    for ( std::size_t i = 0 ; i < n ; ++i )
    {
      const std::size_t i_start = (i >= m) ? (i+1-m) : 0;
      const std::size_t mn = i+1-i_start;
      prim_t soma = 0;
      for ( std::size_t j = 0 ; j < mn ; ++j )
      {
        soma += column_impl_base_t::_zat(j+i_start);
      }
      dst[i] = soma;
    }
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}
 
// ==================================== abs =================================

namespace detail
{
  template<is_primitive_type P>
  inline
  P abs(const P val) {
    return std::abs(val);
  }

  template<>
  inline
  unsigned abs(const unsigned val) {
    return val;
  }

  template<>
  inline
  unsigned long abs(const unsigned long val) {
    return val;
  }

  template<>
  inline
  unsigned long long abs(const unsigned long long val) {
    return val;
  }

  template<>
  inline
  double abs<double>(const double val) {
    return std::fabs(val);
  }
} // namespace detail

template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<P>> 
column_impl<P>::abs() const
{
  vector_t dst(column_impl_base_t::v_);

  const std::size_t n = column_impl_base_t::size();

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? vi : detail::abs(vi);
  }
  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== log / log2 / log10 =================================
 
template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<double>> 
column_impl<P>::log() const
{
  const std::size_t n = column_impl_base_t::size();

  std::vector<double> dst(n);

  constexpr double none = prim_traits<double>::none;

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? none : std::log(vi);
  }
 
  return std::make_shared<column_impl<double>>(std::move(dst));
}

template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<double>> 
column_impl<P>::log2() const
{
  const std::size_t n = column_impl_base_t::size();

  std::vector<double> dst(n);

  constexpr double none = prim_traits<double>::none;

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? none : std::log2(vi);
  }
 
  return std::make_shared<column_impl<double>>(std::move(dst));
}

template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<double>> 
column_impl<P>::log10() const
{
  const std::size_t n = column_impl_base_t::size();

  std::vector<double> dst(n);

  constexpr double none = prim_traits<double>::none;

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? none : std::log10(vi);
  }
 
  return std::make_shared<column_impl<double>>(std::move(dst));
}

// ==================================== exp =================================
 
template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<double>> 
column_impl<P>::exp() const
{
  const std::size_t n = column_impl_base_t::size();

  std::vector<double> dst(n);

  constexpr double none = prim_traits<double>::none;

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? none : std::exp(vi);
  }
 
  return std::make_shared<column_impl<double>>(std::move(dst));
}

// ==================================== mod =================================
 
template<is_arithmetic_type P>
inline
std::shared_ptr<column_impl<double>> 
column_impl<P>::mod(const P y) const
{
  const std::size_t n = column_impl_base_t::size();

  std::vector<double> dst(n);

  constexpr double none = prim_traits<double>::none;

  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<prim_t>::is_none(vi) ? none : std::fmod(vi, y);
  }
 
  return std::make_shared<column_impl<double>>(std::move(dst));
}

// ==================================== sum =================================

template<is_arithmetic_type P>
inline
column_impl<P>::prim_t 
column_impl<P>::sum() const
{
  const std::size_t n = column_impl_base_t::size();
  if ( n == 0 ) return prim_traits<prim_t>::zero;

  prim_t r = prim_traits<prim_t>::zero;
  for ( std::size_t i = 0; i < n ; ++i )
  {
    r += column_impl_base_t::_zat(i);
  }
  return r;
}

// ==================================== avg =================================

template<is_arithmetic_type P>
inline
column_impl<P>::prim_t 
column_impl<P>::avg() const
{
  const std::size_t n = column_impl_base_t::size();
  if ( n == 0 ) 
  {
    return prim_traits<prim_t>::zero;
  }
  const std::size_t n_ = n - column_impl_base_t::n_nones();
  if ( n_ == 0 ) 
  {
    return prim_traits<prim_t>::none;
  }
  return sum() / (prim_t)n_;
}

// ==================================== dot =================================

template<is_arithmetic_type P>
template<is_arithmetic_type P1>
inline
auto 
column_impl<P>::dot(const column_impl<P1>& rhs) const
{
  if (rhs.size() != column_impl_base_t::size())
  {
    throw std::length_error(__FUNCTION__);
  }
  using R = decltype(std::declval<prim_t>() * std::declval<P1>());

  const std::size_t n = column_impl_base_t::size();
  R soma = prim_traits<R>::zero;
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    prim_t vlhs = column_impl_base_t::v_[i];
    vlhs = prim_traits<prim_t>::is_none(vlhs) ? prim_traits<prim_t>::zero : vlhs;
    P1 vrhs = rhs.v()[i];
    vrhs = prim_traits<P1>::is_none(vrhs) ? prim_traits<P1>::zero : vrhs;
    soma += vlhs * vrhs;
  }
  return soma;
}

// ==================================== norm =================================

template<is_arithmetic_type P>
inline
double 
column_impl<P>::norm() const
{
  const std::size_t n = column_impl_base_t::size();
  double soma = 0.0;
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    const prim_t vi = column_impl_base_t::_zat(i);
    soma += vi*vi;
  }
  return std::sqrt(soma);
}

// ==================================== neg =================================

inline
std::shared_ptr<column_impl<bool>>
column_impl<bool>::neg() const
{
  vector_t dst(v_.size());
  for ( std::size_t i = 0 ; i < v_.size() ; ++i)
  { 
    const bool vi = v_[i];
    dst[i] = (vi == true) ? false : true;
  }

  return std::make_shared<column_impl_t>(std::move(dst));
}

// ==================================== where =================================

inline
vixs_t column_impl<bool>::where() const
{
  const std::size_t n = v_.size();

  vixs_t ixs;
  ixs.reserve(n);
  for ( std::size_t i = 0 ; i < n ; ++i)
  {
    if ( v_[i] == true ) ixs.push_back(i);
  }
  return ixs;
}

// ==================================== starts_with (scalar) =================================

inline
std::shared_ptr<column_impl<bool>>
column_impl<std::string_view>::starts_with(const std::string_view sv) const
{
  std::vector<bool> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view vi = column_impl_base_t::v_[i];
    dst[i] = vi.starts_with(sv);
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== starts_with (column) =================================

inline
std::shared_ptr<column_impl<bool>>
column_impl<std::string_view>::starts_with(const column_impl<std::string_view>& svs) const
{
  const std::size_t n = v_.size();
  if (svs.size() != n)
  {
    throw std::length_error(__FUNCTION__);
  }
  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view lhs = column_impl_base_t::v_[i];
    const std::string_view rhs = svs.v_[i];
    dst[i] = lhs.starts_with(rhs);
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== ends_with (scalar) =================================

inline
std::shared_ptr<column_impl<bool>>
column_impl<std::string_view>::ends_with(const std::string_view sv) const
{
  std::vector<bool> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view vi = column_impl_base_t::v_[i];
    dst[i] = vi.ends_with(sv);
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== ends_with (column) =================================

inline
std::shared_ptr<column_impl<bool>>
column_impl<std::string_view>::ends_with(const column_impl<std::string_view>& svs) const
{
  const std::size_t n = v_.size();
  if (svs.size() != n)
  {
    throw std::length_error(__FUNCTION__);
  }
  std::vector<bool> dst(n);
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view lhs = column_impl_base_t::v_[i];
    const std::string_view rhs = svs.v_[i];
    dst[i] = lhs.ends_with(rhs);
  }

  return std::make_shared<column_impl<bool>>(std::move(dst));
}

// ==================================== substr (scalar) =================================

inline
std::shared_ptr<column_impl<std::string_view>>
column_impl<std::string_view>::substr(const std::size_t pos, const std::size_t count) const
{
  std::vector<std::string_view> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view vi = column_impl_base_t::v_[i];
    dst[i] = vi.substr(pos,count);
  }

  return std::make_shared<column_impl<std::string_view>>(std::move(dst));
}

// ==================================== substr (column) =================================

inline
std::shared_ptr<column_impl<std::string_view>>
column_impl<std::string_view>::substr(const column_impl<std::size_t>& pos, const column_impl<std::size_t>& count) const
{
  const std::size_t n = v_.size();
  if (pos.size() != n || count.size() != n)
  {
    throw std::length_error(__FUNCTION__);
  }
  std::vector<std::string_view> dst(n);
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const std::string_view vi = column_impl_base_t::v_[i];
    dst[i] = vi.substr(pos.v()[i],count.v()[i]);
  }

  return std::make_shared<column_impl<std::string_view>>(std::move(dst));

}

// ==================================== concat (scalar) =================================

inline
std::shared_ptr<column_impl<std::string_view>>
column_impl<std::string_view>::concat(const std::string_view sv) const
{
  std::vector<std::string_view> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    std::string vi(column_impl_base_t::v_[i]);
    vi += sv;
    dst[i] = impl::string_storage::instance().add_string(std::move(vi));
  }

  return std::make_shared<column_impl<std::string_view>>(std::move(dst));
}

// ==================================== concat (column) =================================

inline
std::shared_ptr<column_impl<std::string_view>>
column_impl<std::string_view>::concat(const column_impl<std::string_view>& svs) const
{
  const std::size_t n = v_.size();
  if (svs.size() != n)
  {
    throw std::length_error(__FUNCTION__);
  }
  std::vector<std::string_view> dst(n);
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    std::string lhs(column_impl_base_t::v_[i]);
    const std::string_view rhs = svs.v_[i];
    lhs += rhs;
    dst[i] = impl::string_storage::instance().add_string(std::move(lhs));
  }

  return std::make_shared<column_impl<std::string_view>>(std::move(dst));
}

// ==================================== to_string =================================

inline
std::string
column_impl<std::string_view>::to_string(const std::string_view delim) const
{
  const std::size_t n = v_.size();
  std::string out;

  if ( n == 0 )
  {
    return out;
  }
  if ( n > 1 )
  {
    for ( std::size_t i = 0 ; i < (n-1) ; ++i)
    { 
      out += v_[i];
      out += delim;
    }
  }
  out += v_[n-1];
  
  return out;
}

// ==================================== time_since_epoch  =================================

template<is_time_point P>
std::shared_ptr<column_impl<typename P::duration>>
column_impl<P>::time_since_epoch() const
{
  using R = typename P::duration;

  std::vector<R> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<R>::none : vi.time_since_epoch();
  }

  return std::make_shared<column_impl<R>>(std::move(dst));
}

// ==================================== time_point_cast  =================================

template<is_time_point P>
template <
  is_duration ToDuration, 
  typename Clock,
  typename Duration
>
std::shared_ptr<column_impl<std::chrono::time_point<Clock,ToDuration>>>
column_impl<P>::time_point_cast() const
{
  using P1 = std::chrono::time_point<Clock,ToDuration>;

  std::vector<P1> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<P1>::none : 
      std::chrono::time_point_cast<ToDuration,Clock,Duration>(vi);
  }

  return std::make_shared<column_impl<P1>>(std::move(dst));
}

// ==================================== to_time_t  =================================

template<is_time_point P>
std::shared_ptr<column_impl<std::time_t>>
column_impl<P>::to_time_t() const
{
  std::vector<std::time_t> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<std::time_t>::none : std::chrono::system_clock::to_time_t(vi);
  }

  return std::make_shared<column_impl<std::time_t>>(std::move(dst));
}

// ==================================== operator+(duration)  =================================

template<is_time_point P>
template<is_duration D>
constexpr std::shared_ptr<
  column_impl<
    std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
    >
  >
>
column_impl<P>::operator+(const column<D>& ds) const
{
  using R = std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
  >;

  std::vector<R> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    const D di = ds.v()[i];
    const bool is_none = prim_traits<P>::is_none(vi) || prim_traits<D>::is_none(di);
    dst[i] = is_none ? prim_traits<R>::none : (vi+di);
  }

  return std::make_shared<column_impl<R>>(std::move(dst));

}

// ==================================== operator-(duration)  =================================

template<is_time_point P>
template<is_duration D>
constexpr std::shared_ptr<
  column_impl<
    std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
    >
  >
>
column_impl<P>::operator-(const column<D>& ds) const
{
  using R = std::chrono::time_point<
      typename P::clock, 
      std::common_type_t<D,typename P::duration>
  >;

  std::vector<R> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    const D di = ds.v()[i];
    const bool is_none = prim_traits<P>::is_none(vi) || prim_traits<D>::is_none(di);
    dst[i] = is_none ? prim_traits<R>::none : (vi-di);
  }

  return std::make_shared<column_impl<R>>(std::move(dst));
}

// ==================================== duration_cast  =================================

template<is_duration P>
template <is_duration ToDuration>
std::shared_ptr<column_impl<ToDuration>>
column_impl<P>::duration_cast() const
{
  std::vector<ToDuration> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<ToDuration>::none : 
      std::chrono::duration_cast<ToDuration>(vi);
  }

  return std::make_shared<column_impl<ToDuration>>(std::move(dst));
}

// ==================================== floor  =================================

template<is_duration P>
template <is_duration ToDuration>
std::shared_ptr<column_impl<ToDuration>>
column_impl<P>::floor() const
{
  std::vector<ToDuration> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<ToDuration>::none : 
      std::chrono::floor<ToDuration>(vi);
  }

  return std::make_shared<column_impl<ToDuration>>(std::move(dst));
}

// ==================================== ceil  =================================

template<is_duration P>
template <is_duration ToDuration>
std::shared_ptr<column_impl<ToDuration>>
column_impl<P>::ceil() const
{
  std::vector<ToDuration> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<ToDuration>::none : 
      std::chrono::ceil<ToDuration>(vi);
  }

  return std::make_shared<column_impl<ToDuration>>(std::move(dst));
}

// ==================================== round  =================================

template<is_duration P>
template <is_duration ToDuration>
std::shared_ptr<column_impl<ToDuration>>
column_impl<P>::round() const
{
  std::vector<ToDuration> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<ToDuration>::none : 
      std::chrono::round<ToDuration>(vi);
  }

  return std::make_shared<column_impl<ToDuration>>(std::move(dst));
}

// ==================================== abs  =================================

template<is_duration P>
std::shared_ptr<column_impl<P>>
column_impl<P>::abs() const
{
  std::vector<P> dst(column_impl_base_t::v_.size());
  for ( std::size_t i = 0 ; i < column_impl_base_t::v_.size() ; ++i)
  { 
    const prim_t vi = column_impl_base_t::v_[i];
    dst[i] = prim_traits<P>::is_none(vi) ? 
      prim_traits<P>::none : 
      std::chrono::abs(vi);
  }

  return std::make_shared<column_impl<P>>(std::move(dst));
}


} // namespace tables
