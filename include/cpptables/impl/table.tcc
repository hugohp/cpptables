// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <functional>
#include <stdexcept>
#include <vector>
#include <map>
#include <ostream>
#include <tuple_ext/tuple_ext.h>
#include "cpptables/row.hh"
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/ktable.hh"
#include "cpptables/gtable.hh"
#include "cpptables/ftable.hh"
#include "cpptables/type_functions.hh"
#include "cpptables/impl/io/show_table.hh"
#include "cpptables/impl/sort_impl.hh"

namespace tables
{

namespace detail
{
// ==================================== assert_cols_same_size  =================================

  template <typename T, typename... Ts,is_primitive_type P,is_primitive_type... Ps>
  void assert_cols_same_size(
     const mtuple<std::pair<T,column<P>>,std::pair<Ts,column<Ps>>...>& tm)
  {
    if constexpr ( sizeof...(Ts) > 0 )
    {
      const std::size_t nrows = tm.template get<T>().size();
      if ( ((tm.template get<Ts>().size() != nrows) || ... ) )
      {
        throw std::length_error(__FUNCTION__);
      }
    }
  }

  // ==================================== znull  =================================

  // If v is none return zero, else v.
  template<is_primitive_type P>
  inline P znull(const P v)
  {
    return prim_traits<P>::is_none(v) ? prim_traits<P>::zero : v;
  }
}

// ==================================== constructor / assignment  =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table()
  : nrows_(0)
{}

  // Constructor with initializer_list
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(std::initializer_list<Ps>... ls)
 : cols_(std::make_tuple(column<Ps>(ls)...))
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with columns
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(const column<Ps>&... cols)
 : cols_(std::make_tuple(cols...))
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with columns by rvalue
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(column<Ps>&&... cols)
 : cols_(std::make_tuple(std::forward<column<Ps>>(cols)...))
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with tuple of columns
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(const std::tuple<column<Ps>...>& cs)
 : cols_(cs)
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with tuple of columns by rvalue
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(std::tuple<column<Ps>...>&& cs)
 : cols_(std::forward<std::tuple<column<Ps>...>>(cs))
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with mtuple
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(
  const mtuple<std::pair<Ts,column<Ps>>...>& cols
)
 : cols_(cols)
{
  detail::assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Constructor with mtuple (rvalue)
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(
  mtuple<std::pair<Ts,column<Ps>>...>&& cols
)
 : cols_(std::forward<cols_t>(cols))
{
  assert_cols_same_size(cols_);
  nrows_ = cols_.template get<tuple_ext::head_t<ts_t>>().size();
  workspace_.resize(nrows_);
}

// Copy constructor
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(const table<std::pair<Ts,Ps>...>& t)
  : cols_(t.cols_)
  , workspace_(t.nrows())
  , nrows_(t.nrows_)
{}

// Copy assignment operator
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>&
table<std::pair<Ts,Ps>...>::operator=(const table<std::pair<Ts,Ps>...>& t)
{
  if ( this != &t )
  {
    cols_ = t.col_;
    workspace_.resize(t.nrows_());
    nrows_ = t.nrows_;
  }
  return *this;
}

// Move constructor
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::table(table<std::pair<Ts,Ps>...>&& t)
  : cols_(std::forward<mtuple<std::pair<Ts,column<Ps>>...>>(t.cols_))
  , workspace_(t.nrows())
  , nrows_(t.nrows_)
{}

// Move assignment operator
template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>&
table<std::pair<Ts,Ps>...>::operator=(table<std::pair<Ts,Ps>...>&& t)
{
  cols_ = std::move(t.cols_);
  workspace_ = std::move(t.workspace_);
  nrows_ = t.nrows_;
  return *this;
}

// ==================================== nrows =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
std::size_t
table<std::pair<Ts,Ps>...>::nrows() const
{
  return nrows_;
}

// ==================================== col / cols =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename T>
requires in<T,std::tuple<Ts...>>
inline
const auto
table<std::pair<Ts,Ps>...>::col() const
{
  return cols_.template get<T>();
}

// ==================================== _col =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename T>
requires in<T,std::tuple<Ts...>>
inline
auto
table<std::pair<Ts,Ps>...>::_col()
{
  return cols_.template _get<T>();
}

// ==================================== _set_row =================================

namespace detail
{
  template <is_primitive_type P>
  void copy_elem(column<P>&& col, const P elem, const std::size_t irow)
  {
    col._v()[irow] = elem;
  }
} // namespace detail

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
void
table<std::pair<Ts,Ps>...>::_set_row(const std::size_t irow, const row_t& row)
{
  ( detail::copy_elem(_col<Ts>(), row.template col<Ts>(), irow),...);
}

// ==================================== at =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::at(const vixs_t& ixs) const
{
  return table(col<Ts>().at(ixs)...);
}

// ==================================== irow =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::irow(const std::size_t irow) const
{
  return row_t( col<Ts>()[irow]... );
}

// ==================================== take/drop =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::take(const std::size_t n) const
{
  return table_t(col<Ts>().take(n)...);
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::drop(const std::size_t n) const
{
  return table_t(col<Ts>().drop(n)...);
}

// ==================================== rtake/rdrop =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::rtake(const std::size_t n) const
{
  return table_t(col<Ts>().rtake(n)...);
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::rdrop(const std::size_t n) const
{
  return table_t(col<Ts>().rdrop(n)...);
}


// ==================================== append (row) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::append(const row_t& row) const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t(col<Ts>().append(row.template col<Ts>())...);
}

// ==================================== _append (row) =================================


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
void
table<std::pair<Ts,Ps>...>::_append(const row_t& row)
{
  (_col<Ts>().template _append(row.template col<Ts>()),...);
  ++nrows_;
}

// ==================================== _append (table) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
void
table<std::pair<Ts,Ps>...>::_append(const table_t& t)
{
  (_col<Ts>().template _append(t.col<Ts>()), ... );
  nrows_ += t.nrows();
}


// ==================================== append (table)=================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::append(const table_t& other) const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t(col<Ts>().append(other.template col<Ts>())...);
}

// ==================================== lj =================================

namespace detail
{
  template<is_table T,is_ktable KT>
  void make_lj_join_ixs(const T& t, const KT& kt, vixs_t& join_ixs)
  {
    using ktable_t = KT;
    using ks_t = typename ktable_t::ks_t;
    using krow_t = typename KT::krow_t;

    const std::size_t n = t.nrows();

    join_ixs.resize(n);

    const auto tk = t.template select<ks_t>();

    // For each row in t, find corresponding match in kt
    krow_t prev_lhs_row;
    for ( std::size_t ix = 0 ; ix < n ; ++ix)
    {
      const krow_t lhs_row = tk.irow(ix);
      if ( ix > 0 && lhs_row == prev_lhs_row ) // avoid search if row is same as previous row
      {
         join_ixs[ix] = join_ixs[ix-1];
      }
      else
      {
         join_ixs[ix] = kt.find(lhs_row);
         prev_lhs_row = lhs_row;
      }
    }
  }
} // namespace detail


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<is_ktable KT>
requires ( is_kjoinable<table<std::pair<Ts,Ps>...>,KT> )
inline
auto
table<std::pair<Ts,Ps>...>::lj(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& join_ixs = workspace_;
  detail::make_lj_join_ixs<table_t,KT>(*this, kt, join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;
  return addu( kt.t().template remove<ks_t>().at(join_ixs) ).template select<js_t>();
}

// ==================================== ljf =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<is_ktable KT>
requires ( is_kjoinable<table<std::pair<Ts,Ps>...>,KT> )
inline
auto
table<std::pair<Ts,Ps>...>::ljf(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& join_ixs = workspace_;
  detail::make_lj_join_ixs<table_t,KT>(*this, kt, join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using nts_t = tuple_ext::remove_t<ts_t,kts_t>;                          // new-columns
  using its_t = tuple_ext::remove_t<ks_t,tuple_ext::inter_t<ts_t,kts_t>>; // common-columns
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;      // joint columns

  // update common-columns, filling from lhs when rhs is null
  const auto r0 = updatef( kt.t().template select<its_t>().at(join_ixs) );

  // add new columns
  const auto r1 = r0.template add( kt.t().template select<nts_t>().at(join_ixs) );

  return r1.template select<js_t>();
}

// ==================================== ij =================================

namespace detail
{
  template<is_table T,is_ktable KT>
  void make_ij_join_ixs(const T& t, const KT& kt, vixs_t& at_ixs, vixs_t& join_ixs)
  {
    using ktable_t = KT;
    using ks_t = typename ktable_t::ks_t;
    using krow_t = typename KT::krow_t;

    const std::size_t n = t.nrows();

    at_ixs.reserve(n);
    at_ixs.resize(0);

    join_ixs.reserve(n);

    const auto tk = t.template select<ks_t>();

    // For each row in t, find corresponding match in kt
    krow_t prev_lhs_row;
    std::size_t kix = null_ix;
    for ( std::size_t ix = 0 ; ix < n ; ++ix)
    {
      const krow_t lhs_row = tk.irow(ix);
      if ( ix > 0 && lhs_row == prev_lhs_row ) // avoid search if row is same as previous row
      {
         if ( kix != null_ix )
         {
           at_ixs.push_back(ix);
           join_ixs.push_back(kix);
         }
      }
      else
      {
         kix = kt.find(lhs_row);
         if ( kix != null_ix )
         {
           at_ixs.push_back(ix);
           join_ixs.push_back(kix);
         }
         prev_lhs_row = lhs_row;
      }
    }
  }
} // namespace detail


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<is_ktable KT>
requires ( is_kjoinable<table<std::pair<Ts,Ps>...>,KT> )
inline
auto
table<std::pair<Ts,Ps>...>::ij(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& at_ixs = workspace_;
  vixs_t join_ixs;

  detail::make_ij_join_ixs(*this,kt,at_ixs,join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;
  return at(at_ixs).addu( kt.t().template remove<ks_t>().at(join_ixs) ).template select<js_t>();
}

// ==================================== ijf =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<is_ktable KT>
requires ( is_kjoinable<table<std::pair<Ts,Ps>...>,KT> )
inline
auto
table<std::pair<Ts,Ps>...>::ijf(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& at_ixs = workspace_;
  vixs_t join_ixs;

  detail::make_ij_join_ixs(*this,kt,at_ixs,join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using nts_t = tuple_ext::remove_t<ts_t,kts_t>;                          // new-columns
  using its_t = tuple_ext::remove_t<ks_t,tuple_ext::inter_t<ts_t,kts_t>>; // common-columns
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;      // joint columns

  const auto r0 = at(at_ixs);

  // update common-columns, filling from lhs when rhs is null
  const auto r1 = r0.updatef( kt.t().template select<its_t>().at(join_ixs) );

  // add new columns
  const auto r2 = r1.template add( kt.t().template select<nts_t>().at(join_ixs) );

  return r2.template select<js_t>();
}

// ==================================== uj =================================

namespace detail
{
  // ==================================== resize =================================

  template<typename... Ts,is_primitive_type... Ps>
  static void resize(mtuple<std::pair<Ts,column<Ps>>...>& mt, const std::size_t n)
  {
    ( mt.template _get<Ts>()._v().resize(n), ... );
  }

  // ==================================== copy_cols =================================
  //
  template<is_table T0,is_table T1,tuple_ext::is_tuple Ts>
  requires ( all_in<Ts,typename T0::ts_t> && all_in<Ts,typename T1::ts_t> )
  struct copy_cols;

  template<is_table T0,is_table T1,typename... Ts>
  struct copy_cols<T0,T1,std::tuple<Ts...>>
  {

    static void run(const T0& t0,T1& t1,const std::size_t istart)
    {
      assert( istart + t0.nrows() <= t1.nrows() );
      (
        std::copy(
          t0.template col<Ts>().v().begin(),
          t0.template col<Ts>().v().end(),
          &t1.template _col<Ts>()._v()[istart]
        ),
        ...
      );
    }
  };


  // ==================================== fill_nones =================================

  template<is_table T,tuple_ext::is_tuple Ts>
  requires ( all_in<Ts,typename T::ts_t> )
  struct fill_nones;

  template<is_table T,typename... Ts>
  struct fill_nones<T,std::tuple<Ts...>>
  {
    static void run(T& t,const std::size_t istart, std::size_t n)
    {
      assert( istart + n <= t.nrows() );
      (
        std::fill(
          &t.template _col<Ts>()._v()[istart],
          &t.template _col<Ts>()._v()[istart+n],
          prim_traits<typename T::p_of_t<Ts>>::none
	),
        ...
      );
    }
  };
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<is_table T>
requires ( is_joinable<table<std::pair<Ts,Ps>...>,T> )
inline
auto
table<std::pair<Ts,Ps>...>::uj(const T& t) const
{
  // If joining A:
  // c0 | c1
  //  a | 10
  //  b | 20

  // with B:
  // c1 | c2
  // 30 | 100
  // 40 | 200

  // We expect result table `rt`:
  // c0    | c1 |   c2
  //  a    | 10 | none
  //  b    | 20 | none
  //  none | 30 |  100
  //  none | 40 |  200

  using nts_t = tuple_ext::remove_t<ts_t,typename T::ts_t>; // new columns (c2 in above example)
  using nps_t = typename T::ps_of_ts_t<nts_t>;              // primitive-type of new columns

  using rts_t = tuple_ext::concat_t<ts_t,nts_t>; // return columns (c0,c1,c2 in above example)
  using rps_t = tuple_ext::concat_t<ps_t,nps_t>; // return primitive-types

  using rtable_t = to_table_t<tuple_ext::zip_t<rts_t,rps_t>>; // return table type
  using cols_t = rtable_t::cols_t;

  const std::size_t n0 = nrows();
  const std::size_t n1 = t.nrows();

  cols_t cols;
  detail::resize(cols,n0+n1);

  rtable_t rt(cols);

  // Copy this table to rt
  // `rt` after this step
  // c0    | c1 |   c2
  //  a    | 10 |    -
  //  b    | 20 |    -
  //  -    |  - |    -
  //  -    |  - |    -
  detail::copy_cols<table_t,rtable_t,ts_t>::run(*this,rt,0);

  // `rt` after this step
  // c0    | c1 |   c2
  //  a    | 10 |    -
  //  b    | 20 |    -
  //  none |  - |    -
  //  none |  - |    -
  detail::fill_nones<rtable_t,ts_t>::run(rt, n0,n1);

  // Result table `rt`:
  // c0    | c1 |   c2
  //  a    | 10 | none
  //  b    | 20 | none
  //  none |  - |    -
  //  none |  - |    -
  detail::fill_nones<rtable_t,nts_t>::run(rt, 0, n0);

  // Result table `rt`:
  // c0    | c1 |   c2
  //  a    | 10 | none
  //  b    | 20 | none
  //  none | 30 |  100
  //  none | 40 |  200
  detail::copy_cols<T,rtable_t,typename T::ts_t>::run(t,rt,n0);

  return rt;
}

// ==================================== aj =================================

namespace detail{

  template<is_table T,is_ktable KT,typename Tm>
  void make_aj_join_ixs(const T& t, const KT& kt, vixs_t& join_ixs)
  {
    using ktable_t = KT;
    using ks_t = typename ktable_t::ks_t;
    using krow_t = typename KT::krow_t;

    const std::size_t n = t.nrows();

    join_ixs.resize(n);

    const auto tk = t.template select<ks_t>();

    using tm_prim_t = typename T::p_of_t<Tm>;
    const column<tm_prim_t> tms = t.template col<Tm>();

    krow_t prev_lhs_row;
    for ( std::size_t ix = 0 ; ix < n ; ++ix)
    {
      const krow_t lhs_row = tk.irow(ix);
      const tm_prim_t tm = tms[ix];
      join_ixs[ix] = kt.template find_asof<Tm>(lhs_row, tm);
    }
  }
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename Tm,is_ktable KT>
requires (
  in<Tm,std::tuple<Ts...>> &&
  !in<Tm,typename KT::ks_t> &&
  is_kjoinable<table<std::pair<Ts,Ps>...>,KT>
)
inline
auto
table<std::pair<Ts,Ps>...>::aj(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& join_ixs = workspace_;
  detail::make_aj_join_ixs<table_t,KT,Tm>(*this, kt, join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using rs_t = tuple_ext::concat_t<std::tuple<Tm>,ks_t>;
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;
  return addu( kt.t().template remove<rs_t>().at(join_ixs) ).template select<js_t>();
}

// ==================================== aj0 =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename Tm,is_ktable KT>
requires (
  in<Tm,std::tuple<Ts...>> &&
  !in<Tm,typename KT::ks_t> &&
  is_kjoinable<table<std::pair<Ts,Ps>...>,KT>
)
inline
auto
table<std::pair<Ts,Ps>...>::aj0(const KT& kt) const
{
  using ktable_t = KT;
  using ks_t = typename ktable_t::ks_t;

  vixs_t& join_ixs = workspace_;
  detail::make_aj_join_ixs<table_t,KT,Tm>(*this, kt, join_ixs);

  using kts_t = typename ktable_t::table_t::ts_t;
  using js_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,kts_t>>;
  return addu( kt.t().template remove<ks_t>().at(join_ixs) ).template select<js_t>();
}

// ==================================== apply =================================

namespace detail
{

template<typename F,is_primitive_type P>
inline
column<P>
apply(F f, const column<P>& colIn)
{
  using prim_t = P;
  constexpr prim_t none = prim_traits<prim_t>::none;

  const std::size_t n = colIn.size();

  std::vector<prim_t> vout(n);
  for ( std::size_t i = 0 ; i < n ; ++i )
  {
    const prim_t v = colIn[i];
    const prim_t nv = f(v);
    vout[i] = prim_traits<prim_t>::is_none(v) ? none : nv;
  }
  return column<prim_t>(vout);
}

} // namespace detail

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss,typename... Fs>
requires (
  (sizeof...(Ss) == sizeof...(Fs)) &&
  all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
  ( is_prim_to_prim_f<Fs,snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss>> && ... )
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::apply(Fs... fs) const
{
  return update<Ss...>( detail::apply(fs, col<Ss>())... );
}

// ==================================== apply_at =================================

namespace detail
{
template<typename F,is_primitive_type P>
inline
column<P>
apply_at(const vixs_t& ixs, F f, const column<P>& colIn)
{
  using prim_t = P;
  constexpr prim_t none = prim_traits<prim_t>::none;

  std::vector<prim_t> vout(colIn.v());
  for ( const std::size_t ix : ixs )
  {
    const prim_t v = colIn[ix];
    const prim_t nv = f(v);
    vout[ix] = prim_traits<prim_t>::is_none(v) ? none : nv;
  }

  return column<prim_t>(vout);
}

} // namespace detail

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss,typename... Fs>
requires (
  (sizeof...(Ss) == sizeof...(Fs)) &&
  all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
  ( is_prim_to_prim_f<Fs,snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss>> && ... )
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::apply_at(const vixs_t& ixs, Fs... fs) const
{
  return update<Ss...>( detail::apply_at( ixs, fs, col<Ss>() )... );
}

// ==================================== select =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss>
requires ( ( in<Ss,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Ss...>> )
inline
constexpr auto
table<std::pair<Ts,Ps>...>::select() const
{
  using sis_t = std::tuple<Ss...>;
  using sps_t = std::tuple<snd_elem_at_t<ts_t,ps_t,Ss>...>;
  using zs_t = tuple_ext::zip_t<sis_t,sps_t>;
  return to_table_t<zs_t>(col<Ss>()...);
}

// ==================================== remove =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template <tuple_ext::is_tuple Rs>
requires all_in<Rs,std::tuple<Ts...>>
inline
constexpr auto
table<std::pair<Ts,Ps>...>::remove() const
{
  using xis_t = tuple_ext::remove_t<Rs,ts_t>;
  return select<xis_t>();
}


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template <typename... Rs>
requires ( ( in<Rs,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Rs...>> )
inline
constexpr auto
table<std::pair<Ts,Ps>...>::remove() const
{
  return remove<std::tuple<Rs...>>();
}

// ==================================== add (column) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TAs,is_primitive_type... PAs>
requires (
  (!in<TAs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::add(const column<PAs>&... cs) const
{
  using nis_t = tuple_ext::concat_t<ts_t,std::tuple<TAs...>>;
  using nps_t = tuple_ext::concat_t<ps_t,std::tuple<PAs...>>;
  using zs_t = tuple_ext::zip_t<nis_t,nps_t>;
  return to_table_t<zs_t>(col<Ts>()...,cs...);
}

// ==================================== add (table) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TAs,is_primitive_type... PAs>
requires (
  (!in<TAs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::add(
  const table<std::pair<TAs,PAs>...>& t) const
{
  using nis_t = tuple_ext::concat_t<ts_t,std::tuple<TAs...>>;
  using nps_t = tuple_ext::concat_t<ps_t,std::tuple<PAs...>>;
  using zs_t = tuple_ext::zip_t<nis_t,nps_t>;
  return to_table_t<zs_t>(col<Ts>()...,t.template col<TAs>()...);
}

// ==================================== addu (column) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TAs,is_primitive_type... PAs>
requires (
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::addu(const column<PAs>&... cs) const
{
  using as_t = std::tuple<TAs...>;
  using is_t = tuple_ext::inter_t<ts_t,as_t>;
  using rs_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,as_t>>;

  if constexpr ( std::tuple_size_v<is_t> == 0)
  {
    return add<TAs...>(cs...);
  }
  else
  {
    return remove<is_t>().template add<TAs...>(cs...).template select<rs_t>();
  }
}

// ==================================== addu (table) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TAs,is_primitive_type... PAs>
requires (
  is_tuple_unique<std::tuple<TAs...>> &&
  ( sizeof...(TAs) == sizeof...(PAs) )
)
inline
constexpr auto
table<std::pair<Ts,Ps>...>::addu(
  const table<std::pair<TAs,PAs>...>& t
) const
{
  using as_t = std::tuple<TAs...>;
  using is_t = tuple_ext::inter_t<ts_t,as_t>;
  using rs_t = tuple_ext::unique_t<tuple_ext::concat_t<ts_t,as_t>>;

  if constexpr ( std::tuple_size_v<is_t> == 0)
  {
    return add(t);
  }
  else
  {
    return remove<is_t>().add(t).template select<rs_t>();
  }
}

// ==================================== update (with constants) =================================

namespace detail
{
  template<is_primitive_type P>
  inline
  column<P> make_cte_col(const P cte, const std::size_t nrows)
  {
    column<P> c;
    c._v().resize(nrows);
    std::fill(c._v().data(), &c._v().data()[nrows], cte);
    return c;
  }
}// namespace detail

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  (in<TUs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TUs...>> &&
  ( sizeof...(TUs) == sizeof...(PUs) )
)
inline
auto
table<std::pair<Ts,Ps>...>::update(const PUs... ctes) const
{
  const std::size_t n = nrows();
  return update<TUs...>(detail::make_cte_col<PUs>(ctes,n)...);
}

// ==================================== update (with columns) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  (in<TUs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TUs...>> &&
  ( sizeof...(TUs) == sizeof...(PUs) )
)
inline
auto
table<std::pair<Ts,Ps>...>::update(const column<PUs>&... cs) const
{
  return remove<TUs...>().template add<TUs...>(cs...).template select<ts_t>();
}

// ==================================== update (with table) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  (in<TUs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TUs...>> &&
  ( sizeof...(TUs) == sizeof...(PUs) )
)
inline
auto
table<std::pair<Ts,Ps>...>::update(
  const table<std::pair<TUs,PUs>...>& t
) const
{
  return remove<TUs...>().template add<TUs...>(t.template col<TUs>()...).template select<ts_t>();
}

// ==================================== updatef (with columns) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  (in<TUs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TUs...>> &&
  ( sizeof...(TUs) == sizeof...(PUs) )
)
inline
auto
table<std::pair<Ts,Ps>...>::updatef(const column<PUs>&... cs) const
{
  return update<TUs...>( cs.fill_with( col<TUs>() )... );
}

// ==================================== updatef (with table) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  (in<TUs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TUs...>> &&
  ( sizeof...(TUs) == sizeof...(PUs) )
)
inline
auto
table<std::pair<Ts,Ps>...>::updatef(
  const table<std::pair<TUs,PUs>...>& t
) const
{
  return update<TUs...>( t.template col<TUs>().fill_with( col<TUs>() )... );
}

// ==================================== update_at (with constants) =================================

namespace detail
{
  template<is_primitive_type PU>
  inline
  column<PU> update_col_at(const vixs_t& ixs, const column<PU>& c, const PU cte, const std::size_t nrows)
  {
    column<PU> cn = c;
    for (std::size_t i = 0 ; i < ixs.size() ; ++i )
    {
       const std::size_t ix = ixs[i];
       assert( ix < nrows );
       cn._v()[ix] = cte;
    }
    return cn;
  }
} // namespace detail

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  ( (in<TUs,std::tuple<Ts...>>) && ...) &&
  ( sizeof...(TUs) == sizeof...(PUs) ) &&
  (
    std::is_same_v<                    // TUs/PUs matches same pair in ts_t,ps_t
      snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,TUs>,
      PUs
     > && ...
  )
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::update_at(const vixs_t& ixs, const PUs... ctes) const
{
  const std::size_t n = nrows();
  return update<TUs...>(detail::update_col_at<PUs>(ixs, col<TUs>(), ctes, n)...);
}

// ==================================== update_at (with columns) =================================

namespace detail {

  template<is_primitive_type PU>
  column<PU> update_col_at(const vixs_t& ixs, const column<PU>& c0, const column<PU>& c1, const std::size_t nrows)
  {
    column<PU> cn = c0;
    for (std::size_t i = 0 ; i < ixs.size() ; ++i )
    {
       const std::size_t ix = ixs[i];
       assert( ix < nrows );
       cn._v()[ix] = c1.v()[ix];
    }
    return cn;
  }
} // namespace detail


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TUs,is_primitive_type... PUs>
requires (
  ( (in<TUs,std::tuple<Ts...>>) && ...) &&
  ( sizeof...(TUs) == sizeof...(PUs) ) &&
  (
    std::is_same_v<                    // TUs/PUs matches same pair in ts_t,ps_t
      snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,TUs>,
      PUs
     > && ...
  )
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::update_at(const vixs_t& ixs, const column<PUs>&... cs) const
{
  const std::size_t n = nrows();
  return update<TUs...>(detail::update_col_at<PUs>(ixs, col<TUs>(), cs, n)...);
}

// ==================================== fupdate =================================


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss,typename... Fs>
requires (
  (sizeof...(Ss) == sizeof...(Fs)) &&
  all_in<std::tuple<Ss...>,std::tuple<Ts...>> &&
  ( is_col_to_col_f<Fs,column< snd_elem_at_t<std::tuple<Ts...>,std::tuple<Ps...>,Ss> > > && ... )
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::fupdate(const Fs... fs) const
{
  return update<Ss...>( fs(col<Ss>())... );
}

// ==================================== key_by =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ks>
requires ( ( in<Ks,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Ks...>> )
inline
ktable<table<std::pair<Ts,Ps>...>,std::tuple<Ks...>>
table<std::pair<Ts,Ps>...>::key_by() const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return ktable<table_t,std::tuple<Ks...>>(*this);
}

// ==================================== group_by =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Gs>
requires ( ( in<Gs,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Gs...>> )
inline
gtable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>
table<std::pair<Ts,Ps>...>::group_by() const
{
  return gtable<table_t,std::tuple<Gs...>>(*this);
}

// ==================================== fby =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Gs>
requires ( ( in<Gs,std::tuple<Ts...>> && ... ) && is_tuple_unique<std::tuple<Gs...>> )
inline
ftable<table<std::pair<Ts,Ps>...>,std::tuple<Gs...>>
table<std::pair<Ts,Ps>...>::fby() const
{
  return ftable<table_t,std::tuple<Gs...>>(*this);
}

// ==================================== indices_by =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... TIs>
requires (
  (in<TIs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TIs...>>
)
inline
auto
table<std::pair<Ts,Ps>...>::indices_by() const
{
  using tis_t = std::tuple<TIs...>;
  using pis_t = many_snd_elem_at_t<ts_t,ps_t,tis_t>;
  using zis_t = tuple_ext::zip_t<tis_t,pis_t>;
  using irow_t = to_row_t<zis_t>;
  std::map<irow_t,vixs_t> res;
  for ( std::size_t ix = 0; ix < nrows() ; ++ix )
  {
    const irow_t r (col<TIs>()[ix]...);
    res[r].push_back(ix);
  }
  return res;
}

// ==================================== except =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::except(const table_t& xt) const
{
  using table_t = table<std::pair<Ts,Ps>...>;

  const std::size_t n = nrows();

  vixs_t& ixs = workspace_;
  ixs.reserve(n);
  ixs.resize(0);

  ktable<table_t,typename table_t::ts_t> kxt = xt.key_by<ts_t>();

  for ( std::size_t iRow = 0; iRow < n ; ++iRow )
  {
    const row_t rowi = irow(iRow);
    const std::size_t ix = kxt.find(rowi);
    if ( ix == null_ix )
    {
      ixs.push_back(iRow);
    }
  }
  return at(ixs);
}

// ==================================== reverse =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
constexpr table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::reverse() const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t(col<Ts>().reverse()...);
}

// ==================================== sort =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::sort() const
{
  vixs_t& ixs = workspace_;
  impl::make_sorted_indices(select<Ts...>(), ixs);
  return at(ixs);
}

// ==================================== fills =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::fills() const
{
  return table(col<Ts>().fills()...);
}

// ==================================== fill_with =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::fill_with(const row_t& vals) const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t( col<Ts>().fill_with(vals.template col<Ts>())... );
}

// ==================================== zfill =================================

namespace detail
{
  template<is_primitive_type P>
  column<P> do_zfill(const column<P>& col)
  {
    return col;
  }

  template<is_arithmetic_type P>
  column<P> do_zfill(const column<P>& col)
  {
    return col.zfill();
  }
}


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::zfill() const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t( detail::do_zfill( col<Ts>() )... );
}

// ==================================== deltas =================================

namespace detail
{
  template<is_primitive_type P>
  inline
  column<P> do_deltas(const column<P>& col)
  {
    return col;
  }

  template<is_arithmetic_type P>
  inline
  column<P> do_deltas(const column<P>& col)
  {
    return col.deltas();
  }
}


template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::deltas() const
{
  using table_t = table<std::pair<Ts,Ps>...>;
  return table_t( detail::do_deltas( col<Ts>() )... );
}

// ==================================== differ =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
column<bool>
table<std::pair<Ts,Ps>...>::differ() const
{
  const std::size_t n = nrows();
  std::vector<bool> res(n);

  if ( n != 0 )
  {
    res[0] = true;
    for ( std::size_t i = 1; i < n ; ++i )
    {
      const row_t ri1 = irow(i-1);
      const row_t ri = irow(i);
      res[i] = (ri == ri1) ? false : true;
    }
  }
  return column<bool>(std::move(res));
}

// ==================================== distinct =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::distinct() const
{
  const std::size_t n = nrows();
  table_t res;
  (res._col<Ts>()._v().reserve(n), ...);

  std::set<row_t> s;

  for ( std::size_t i = 0; i < n ; ++i )
  {
    const row_t ri = irow(i);
    if ( s.find(ri) == s.end() )
    {
      res._append(ri);
      s.insert(ri);
    }
  }
  return res;
}

// ==================================== sort_by =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss>
requires (
  (in<Ss,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<Ss...>>
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::sort_by() const
{
  vixs_t& ixs = workspace_;
  impl::make_sorted_indices(select<Ss...>(), ixs);
  return at(ixs);
}

// ==================================== rsort_by =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<typename... Ss>
requires (
  (in<Ss,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<Ss...>>
)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::rsort_by() const
{
  vixs_t& ixs = workspace_;
  impl::make_rsorted_indices(select<Ss...>(), ixs);
  return at(ixs);
}

// ==================================== next / prev  =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::next() const
{
  return table_t( col<Ts>().next()... );
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::prev() const
{
  return table_t( col<Ts>().prev()... );
}

// ==================================== abs =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::abs() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply abs no non-arithmetic types"  );
  return table_t( col<Ts>().abs()... );
}

// ==================================== log / log2 / log10  =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::log() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply log no non-arithmetic types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),double>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().log()... );
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::log2() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply log2 no non-arithmetic types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),double>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().log2()... );
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::log10() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply log10 no non-arithmetic types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),double>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().log10()... );
}

// ==================================== exp =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::exp() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply exp no non-arithmetic types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),double>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().exp()... );
}

// ==================================== mod =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::mod(
  const table<std::pair<Ts,Ps>...>::row_t& vals
) const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply mod no non-arithmetic types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),double>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().mod(vals.template col<Ts>())... );
}

// ==================================== min/max =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::min() const
{
  const std::size_t n = nrows();

  row_t rmin;
  if ( n > 0 )
  {
    rmin = row_t( detail::znull(col<Ts>()[0])... );
    for ( std::size_t i = 1 ; i < n ; ++i )
    {
      const row_t ri = row_t( detail::znull(col<Ts>()[i])... );
      rmin = std::min(ri,rmin);
    }
  }
  return rmin;
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::max() const
{
  const std::size_t n = nrows();

  row_t rmax;
  if ( n > 0 )
  {
    rmax = row_t( detail::znull(col<Ts>()[0])... );
    for ( std::size_t i = 1 ; i < n ; ++i )
    {
      const row_t ri = row_t( detail::znull(col<Ts>()[i])... );
      rmax = std::max(ri,rmax);
    }
  }
  return rmax;
}
// ==================================== sum =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::sum() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply aggregate no non-arithmetic types"  );
  return row_t( col<Ts>().sum()... );
}

// ==================================== avg =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
table<std::pair<Ts,Ps>...>::row_t
table<std::pair<Ts,Ps>...>::avg() const
{
  static_assert( ( is_arithmetic_type<Ps> && ... ), "Cannot apply aggregate no non-arithmetic types"  );
  return row_t( col<Ts>().avg()... );
}

// ==================================== all =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::all() const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_row_t = as_row_t<ts_t,nps_t>;
  return r_row_t( (bool)col<Ts>().all()... );
}

// ==================================== any =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::any() const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_row_t = as_row_t<ts_t,nps_t>;
  return r_row_t( (bool)col<Ts>().any()... );
}

// ==================================== neg =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::neg() const
{
  static_assert( ( std::is_same_v<Ps,bool> && ... ), "Cannot apply neg no non-boolean types"  );
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().neg()... );
}
// ==================================== eq =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::eq(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().eq( rhs.col<Ts>() )... );
}

// ==================================== ne =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::ne(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().ne( rhs.col<Ts>() )... );
}

// ==================================== lt =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::lt(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().lt( rhs.col<Ts>() )... );
}

// ==================================== le =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::le(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().le( rhs.col<Ts>() )... );
}

// ==================================== gt =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::gt(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().gt( rhs.col<Ts>() )... );
}

// ==================================== ge =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
auto
table<std::pair<Ts,Ps>...>::ge(const table<std::pair<Ts,Ps>...>& rhs) const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_table_t = as_table_t<ts_t,nps_t>;
  return r_table_t( col<Ts>().ge( rhs.col<Ts>() )... );
}

// ==================================== // =================================

namespace detail
{
  template<is_table,tuple_ext::is_tuple> struct expand_s;

  template<is_table T, typename... Es>
  struct expand_s<T,std::tuple<Es...>>
  {
    using table_t = T;
    static const auto cols(const table_t& t)
    {
      return t.template cols<Es...>();
    }

    static const auto key_by(const table_t& t)
    {
      return t.template key_by<Es...>();
    }

    static const auto group_by(const table_t& t)
    {
      return t.template group_by<Es...>();
    }

    static const auto select(const table_t& t)
    {
      return t.template select<Es...>();
    }

    static const auto sort_by(const table_t& t)
    {
      return t.template sort_by<Es...>();
    }

    static const auto indices_by(const table_t& t)
    {
      return t.template indices_by<Es...>();
    }
  };
} // namespace detail


// ==================================== key_by (with tuple) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<tuple_ext::is_tuple Ks>
requires ( all_in<Ks,std::tuple<Ts...>> && is_tuple_unique<Ks> )
inline
ktable<table<std::pair<Ts,Ps>...>,Ks>
table<std::pair<Ts,Ps>...>::key_by() const
{
   return detail::expand_s<table_t, Ks>::key_by(*this);
}

// ==================================== group_by (with tuple) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<tuple_ext::is_tuple Gs>
requires ( all_in<Gs,std::tuple<Ts...>> && is_tuple_unique<Gs> )
inline
gtable<table<std::pair<Ts,Ps>...>,Gs>
table<std::pair<Ts,Ps>...>::group_by() const
{
   return detail::expand_s<table_t,Gs>::group_by(*this);
}

// ==================================== select (with tuple) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<tuple_ext::is_tuple Ss>
requires ( all_in<Ss,std::tuple<Ts...>> && is_tuple_unique<Ss> )
inline
auto
table<std::pair<Ts,Ps>...>::select() const
{
   return detail::expand_s<table_t,Ss>::select(*this);
}

// ==================================== sort_by (with tuple) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<tuple_ext::is_tuple Ss>
requires ( all_in<Ss,std::tuple<Ts...>> && is_tuple_unique<Ss>)
inline
table<std::pair<Ts,Ps>...>
table<std::pair<Ts,Ps>...>::sort_by() const
{
   return detail::expand_s<table_t,Ss>::sort_by(*this);
}

// ==================================== indices_by (with tuple) =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
template<tuple_ext::is_tuple Ss>
requires ( all_in<Ss,std::tuple<Ts...>> && is_tuple_unique<Ss>)
inline
auto
table<std::pair<Ts,Ps>...>::indices_by() const
{
   return detail::expand_s<table_t,Ss>::indices_by(*this);
}

// ==================================== workspace =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
vixs_t&
table<std::pair<Ts,Ps>...>::_workspace() const
{
  return workspace_;
}

// ==================================== operator<< =================================

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
std::ostream& operator<< (std::ostream& os, const table<std::pair<Ts,Ps>...>& t)
{
  using namespace impl::io;

  using ts_t = typename std::tuple<Ts...>;
  using ps_t = typename std::tuple<Ps...>;

  const std::size_t nrows = t.nrows();

  const auto css = make_many_column_string( t );

  header_display<ts_t,ps_t>::run(os, css);
  os << "\n";

  line_display<ts_t,ps_t>::run(os, css);
  os << "\n";

  for ( std::size_t i = 0 ; i < nrows ; ++i )
  {
    row_display<ts_t,ps_t>::run(os, i, css);
    os << "\n";
  }
  return os;
}

template <typename... Ts,is_primitive_type... Ps>
requires ( is_key_value_pairs<std::pair<Ts,Ps>...> )
inline
std::ostream& operator<< (std::ostream& os, const table<>& t)
{
  return os;
}

} // namespace tables
