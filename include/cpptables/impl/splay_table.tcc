// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include "cpptables/impl/sort_impl.hh"
#include <fstream>

namespace tables
{

namespace detail
{
  // Recursively iterate through directories /T0/T1/.../Tn adding directory names to columns
  // Example:
  //
  // If directory layout (2 columns i.e. depth 2) is:
  // 2020/Jan
  // 2020/Feb
  // 2020/Mar
  // 2021/Jan
  // 2021/Feb
  // 2021/Mar
  //
  // Returned table would be
  // 2020 Jan
  // 2020 Feb
  // 2020 Mar
  // 2021 Jan
  // 2021 Feb
  // 2021 Mar
  template<is_table table_t>
  inline table_t get_path_entries(const std::filesystem::path path)
  {
    using ts_t = typename table_t::ts_t;
    using T = typename tuple_ext::head_t<ts_t>;
    using P = typename table_t::p_of_t<T>;

    if constexpr (std::tuple_size_v<ts_t> == 1 )
    {
      std::vector<P> xs;
      for (auto const& dir_entry : std::filesystem::directory_iterator(path))
      {
        if ( dir_entry.is_directory() )
        {
          const std::string filename = dir_entry.path().filename().string();
          const P val = prim_traits<P>::from_string(filename);
          xs.push_back(val);
        }
      }
      return table_t(column<P>(std::move(xs)));
    }
    else
    {
      table_t t;
      for (auto const& dir_entry : std::filesystem::directory_iterator(path))
      {
        if ( dir_entry.is_directory() )
        {
          using tail_table_t = typename table_t::select_t< tuple_ext::tail_t<ts_t> >;
          const tail_table_t t1 = get_path_entries<tail_table_t>(dir_entry.path());

          const std::string filename = dir_entry.path().filename().string();
          const P val = prim_traits<P>::from_string(filename);
          std::vector<P> xs(t1.nrows(), val);
          t._append(t1.template add<T>( column<P>(std::move(xs)) ).template select<ts_t>());
        }
      }
      return t;
    }
  }

  // ==================================== make_row_path =================================

  template<is_row> 
  struct make_row_path;
  template <typename... Ts, is_primitive_type... Ps>
  struct make_row_path<row<std::pair<Ts,Ps>...>> 
  {
    static std::filesystem::path run(const std::filesystem::path path, const row<std::pair<Ts,Ps>...>& row)
    {
      std::filesystem::path full_path(path);
      full_path.append("");
      ( full_path.append(std::to_string(row.template col<Ts>())).append(""), ... );
      return full_path;
    }
  };

  // ==================================== load_column =================================

  template<typename T,is_primitive_type P>
  inline 
  column<P> load_column(const std::filesystem::path path)
  {
    const std::filesystem::path column_path(path / type_traits<T>::name);

    std::vector<P> xs;
    if ( std::filesystem::is_regular_file(column_path) )
    {
      const std::size_t n = std::filesystem::file_size(column_path);
      std::fstream ifs(column_path.string(), ifs.binary | ifs.in);

      xs.resize(n / sizeof(P));
      ifs.seekg(0);
      ifs.read(reinterpret_cast<char*>(xs.data()), n);
    }
    return column<P>(std::move(xs));
  }


  template<is_tuple_unique,tuple_ext::is_tuple>
  struct columns_loader;

  template<typename... Ts,is_primitive_type... Ps>
  struct columns_loader<std::tuple<Ts...>,std::tuple<Ps...>>
  {
    static table<std::pair<Ts,Ps>...> run(const std::filesystem::path path)
    {
       return table<std::pair<Ts,Ps>...>(load_column<Ts,Ps>(path)...);
    }
  };
  // ==================================== append_column =================================

  template<typename T,is_primitive_type P>
  void append_column(const std::filesystem::path path, const column<P>& col)
  {
    const std::filesystem::path file_path(path / type_traits<T>::name);
    std::ofstream ofs(file_path.string(), ofs.binary | ofs.out | ofs.app);
    assert( ofs.is_open() && ofs.rdstate() == std::ios::goodbit);

    ofs.write(reinterpret_cast<const char*>(col.v().data()),col.size()*sizeof (P));
    ofs.close();
  }

  // ==================================== update_column =================================

  template<typename T,is_primitive_type P>
  void update_column(const std::filesystem::path path, const column<P>& col)
  {
    const std::filesystem::path file_path(path / type_traits<T>::name);
    std::ofstream ofs(file_path.string(), ofs.binary | ofs.out | ofs.trunc);
    assert( ofs.is_open() && ofs.rdstate() == std::ios::goodbit);

    ofs.write(reinterpret_cast<const char*>(col.v().data()),col.size()*sizeof (P));
    ofs.close();
  }

 
  // ==================================== column_updater =================================

  template<is_tuple_unique,tuple_ext::is_tuple>
  struct column_updater;

  template<typename... Ts,is_primitive_type... Ps>
  struct column_updater<std::tuple<Ts...>,std::tuple<Ps...>>
  {
    template<is_table T>
    static void append(const std::filesystem::path path,const T&t)
    {
      (append_column<Ts,Ps>(path,t.template col<Ts>()), ...);
    }
    template<is_table T>
    static void update(const std::filesystem::path path,const T&t)
    {
      (update_column<Ts,Ps>(path,t.template col<Ts>()), ...);
    }
  };
} // detail namespace

// ==================================== constructor / assignment  =================================

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
inline 
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::splay_table(const std::filesystem::path path) 
  : path_(path)
{
  std::filesystem::create_directories(path);
}

// ==================================== tk =================================

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
inline 
constexpr
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::table_ds_t
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::td() const
{
  return detail::get_path_entries<table_ds_t>(path_);
}

// ==================================== get =================================

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
template<typename... Ss>
requires( 
    ( sizeof...(Ss) > 0 ) &&
    ( in<Ss,std::tuple<Ts...>> && ... ) &&
    ( not_in<Ss,std::tuple<Ds...>> && ... )
)
inline 
auto
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::get(const table_ds_t& tk) const
{
   using ss_t = std::tuple<Ss...>;
   using sps_t = typename table_t::ps_of_ts_t<ss_t>;
   using rs_t = tuple_ext::concat_t<ds_t,ss_t>;
   using r_table_t = typename table_t::select_t<rs_t>;
   using ss_table_t = typename table_t::select_t<ss_t>;

   const table_ds_t tk1 = tk.template sort_by<ds_t>();
   r_table_t res;
   for ( std::size_t iRow = 0 ; iRow < tk1.nrows() ; ++iRow )
   {
     using row_ds_t = typename table_ds_t::row_t;
     const row_ds_t row = tk1.irow(iRow);
     const std::filesystem::path row_path = detail::make_row_path<row_ds_t>::run(path_,row);
     const ss_table_t t = detail::columns_loader<ss_t,sps_t>::run(row_path);
     const std::size_t nrows = t.nrows();

     using cols_ds_t = typename table_ds_t::cols_t;
     cols_ds_t cols;
     detail::resize_cols_with_row(cols,nrows,row);
     res._append( t.add(table_ds_t(cols)).template select<rs_t>() );
   }
   return res;
}

// ==================================== update =================================

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
inline 
void
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::update(const table_t& t) const
{
   using npds_t = typename table_t::ps_of_ts_t<nds_t>;
   using row_ds_t = typename table_ds_t::row_t;

   const table_ds_t tks = t.template select<ds_t>().template sort_by<ds_t>();
   const auto tnks = t.template select<nds_t>();
   row_ds_t last_row;
   for ( std::size_t iRow = 0 ; iRow < tks.nrows() ; ++iRow )
   {
     const row_ds_t row = tks.irow(iRow);
     if ( iRow == 0 || row != last_row )
     {
       const std::string row_path = detail::make_row_path<row_ds_t>::run(path_,row);
       std::filesystem::create_directories(row_path);
       const vixs_t ixs = where( ((t.template col<Ds>() == row.template col<Ds>()) && ... ) );
       detail::column_updater<nds_t,npds_t>::update(row_path,tnks.at(ixs));
       last_row = row;
     }    
   }
}

// ==================================== upsert =================================

template<typename... Ts,is_primitive_type... Ps,typename... Ds>
inline 
void
splay_table<table<std::pair<Ts,Ps>...>,std::tuple<Ds...>>::upsert(const table_t& t) const
{
   using npds_t = typename table_t::ps_of_ts_t<nds_t>;
   using row_ds_t = typename table_ds_t::row_t;

   const table_ds_t tks = t.template select<ds_t>().template sort_by<ds_t>();
   const auto tnks = t.template select<nds_t>();
   row_ds_t last_row;
   for ( std::size_t iRow = 0 ; iRow < tks.nrows() ; ++iRow )
   {
     const row_ds_t row = tks.irow(iRow);
     if ( iRow == 0 || row != last_row )
     {
       const std::string row_path = detail::make_row_path<row_ds_t>::run(path_,row);
       std::filesystem::create_directories(row_path);
       const vixs_t ixs = where( ((t.template col<Ds>() == row.template col<Ds>()) && ... ) );
       detail::column_updater<nds_t,npds_t>::append(row_path,tnks.at(ixs));
       last_row = row;
     }    
   }
}

} // namespace tables
