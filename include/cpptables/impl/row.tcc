// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/row_fwd.hh"
#include "cpptables/impl/io/show_table.hh"
#include <tuple_ext/tuple_ext.h>

namespace tables
{

// ==================================== constructors  =================================

template <typename... Ts, is_primitive_type... Ps>
row<std::pair<Ts,Ps>...>::row()
{}

template <typename... Ts, is_primitive_type... Ps>
row<std::pair<Ts,Ps>...>::row(const Ps&... ps)
 : cols_(ps...)
{}

// Constructor with tuple-of-primitives
template <typename... Ts, is_primitive_type... Ps>
row<std::pair<Ts,Ps>...>::row(const std::tuple<Ps...>& ps)
 : cols_(ps)
{}

// Constructor with tuple-of-primitives (by rvalue)
template <typename... Ts, is_primitive_type... Ps>
row<std::pair<Ts,Ps>...>::row(std::tuple<Ps...>&& ps)
 : cols_(std::forward<std::tuple<Ps...>>(ps))
{}

// ==================================== col  =================================

template <typename... Ts, is_primitive_type... Ps>
template<typename T>
requires in<T,std::tuple<Ts...>>
auto
row<std::pair<Ts,Ps>...>::col() const
{
  return cols_.template get<T>();
}

// ==================================== add  =================================

template <typename... Ts, is_primitive_type... Ps>
template<typename... TAs,is_primitive_type... PAs>
requires (
  ( sizeof...(TAs) == sizeof...(PAs) ) &&
  (!in<TAs,std::tuple<Ts...>> && ... ) &&
  is_tuple_unique<std::tuple<TAs...>>
)
auto
row<std::pair<Ts,Ps>...>::add(const row<std::pair<TAs,PAs>...>& other) const
{
   using rrow_t = row<
     std::pair<Ts,Ps>...,
     std::pair<TAs,PAs>...
   >;
   return rrow_t(col<Ts>()...,other.template col<TAs>()...);
}

// ==================================== // =================================

namespace detail
{
  template<is_primitive_type P>
  inline
  bool is_true(const P v) {
    return (v != prim_traits<P>::zero && ! prim_traits<P>::is_none(v) );
  }

  template<>
  inline
  bool is_true<bool>(const bool v) {
    return v;
  }
} // namespace detail

// ==================================== all =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::all() const
{
  return ( detail::is_true(col<Ts>()) && ... );
}

// ==================================== any =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::any() const
{
  return ( detail::is_true(col<Ts>()) || ... );
}

// ==================================== neg =================================

template <typename... Ts, is_primitive_type... Ps>
inline
auto
row<std::pair<Ts,Ps>...>::neg() const
{
  using nps_t = tuple_ext::repeat_t<ncols(),bool>;
  using r_row_t = as_row_t<ts_t,nps_t>;
  return r_row_t( col<Ts>().neg()... );
}

// ==================================== eq =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::eq(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) == std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== ne =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::ne(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) != std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== lt =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::lt(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) < std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== le =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::le(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) <= std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== gt =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::gt(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) > std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== ge =================================

template <typename... Ts, is_primitive_type... Ps>
inline
bool
row<std::pair<Ts,Ps>...>::ge(const row<std::pair<Ts,Ps>...>& rhs) const
{
  return ( std::make_tuple( col<Ts>()...) >= std::make_tuple( rhs.col<Ts>()... ) );
}

// ==================================== operator<< =================================

template <is_row R>
std::ostream& operator<< (std::ostream& os, const R& row)
{
  using namespace impl::io;

  using row_t = R;
  using ts_t = typename row_t::ts_t;
  using ps_t = typename row_t::ps_t;

  const auto css = make_many_column_string( row );

  header_display<ts_t,ps_t>::run(os, css);
  os << "\n";

  line_display<ts_t,ps_t>::run(os, css);
  os << "\n";

  row_display<ts_t,ps_t>::run(os, 0, css);
  return os;
}

} // namespace tables
