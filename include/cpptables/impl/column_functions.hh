// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <type_traits>
#include <tuple>
#include <vector>
#include <ostream>
#include "cpptables/column.hh"
#include "cpptables/concepts.hh"
#include "cpptables/types.hh"
#include "cpptables/table.hh"
#include "cpptables/type_functions.hh"
#include <tuple_ext/tuple_ext.h>
#include "cpptables/impl/io/show_table.hh"
//#include "cpptables/impl/column_functions.hh"

namespace tables
{
namespace detail
{
  // ==================================== reserve_cols  =================================

  template<typename ...Ts,is_primitive_type ...Ps>
  inline void reserve_cols(mtuple<std::pair<Ts,column<Ps>>...>& cols,const std::size_t n)
  {
    (cols.template _get<Ts>()._v().reserve(n),...);
  }

  // ==================================== resize_cols =================================

  template<typename ...Ts,is_primitive_type ...Ps>
  inline void resize_cols(mtuple<std::pair<Ts,column<Ps>>...>& cols,const std::size_t n)
  {
    (cols.template _get<Ts>()._v().resize(n),...);
  }

  // ==================================== resize_cols_with_row =================================
  template<typename ...Ts,is_primitive_type ...Ps>
  inline void resize_cols_with_row(mtuple<std::pair<Ts,column<Ps>>...>& cols, const std::size_t n, const row<std::pair<Ts,Ps>...>& row)
  {
    (cols.template _get<Ts>()._v().resize(n, row.template col<Ts>()),...);
  }
} // namespace detail
} // namespace tables

