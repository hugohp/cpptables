// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <iomanip>
#include <sstream>
#include "cpptables/missing_ops.hh"
#include "cpptables/types.hh"
#include "cpptables/impl/string_buffer.hh"
#include "cpptables/impl/io/column_traits_io.hh"
#include "cpptables/column.hh"
#include "cpptables/table.hh"
#include "cpptables/row.hh"

namespace tables
{

namespace impl
{
namespace io
{

// ==================================== column_as_string  =================================

// Used when transforming a column<T> to a string.
template<typename T,is_primitive_type P>
struct column_as_string {
  using type = T;
  using prim_t = P;
  std::size_t width = 0;
  std::string_view header;
  string_buffer sbuff;
};

// ==================================== make_column_string  =================================

// Converts column<T> to column_as_string<T>
template<typename T,is_primitive_type P>
inline column_as_string<T,P> make_column_string(const tables::column<P>& col)
{
  using prim_t = P;

  const std::size_t n = col.size();
  column_as_string<T,prim_t> res;

  // Column-name to string
  res.header = std::string_view(type_traits<T>::name);
  res.width = res.header.size();

  if (col.size() == 0) return res;

  res.sbuff = column_traits_io<prim_t>::to_ostream(col);

  // Width is maximum width of all elements in string-buffer
  for ( std::size_t i = 0 ; i < (n-1) ; ++i )
  {
    res.width = std::max(res.width,res.sbuff.pos[i+1]-res.sbuff.pos[i]);
  }
  res.width = std::max(res.width,res.sbuff.buffer.length()-res.sbuff.pos[n-1]);
  return res;
}

// ==================================== make_many_column_string  =================================

template<typename... Ts,is_primitive_type... Ps>
inline auto make_many_column_string(const tables::table<std::pair<Ts,Ps>...>& t)
{
  return std::make_tuple(make_column_string<Ts,Ps>(t.template col<Ts>())...);
}

template<typename... Ts,is_primitive_type... Ps>
inline auto make_many_column_string(const tables::row<std::pair<Ts,Ps>...>& row)
{
  tables::table<std::pair<Ts,Ps>...> t;
  t._append(row);
  return make_many_column_string(t);
}

// ==================================== header_display  =================================

template<tuple_ext::is_tuple Ts,tuple_ext::is_tuple Ps> struct header_display;

template<typename T,typename... Ts,is_primitive_type P, is_primitive_type... Ps>
struct header_display<std::tuple<T,Ts...>,std::tuple<P,Ps...>>
{
  static void run(std::ostream& os, const std::tuple<column_as_string<T,P>,column_as_string<Ts,Ps>...>& css)
  {
    do_first_column(os,std::get<column_as_string<T,P>>(css));
    (do_next_columns(os, std::get<column_as_string<Ts,Ps>>(css)),...);
  }
private:
  template<typename T1,is_primitive_type P1>
  static void do_column(std::ostream& os, const column_as_string<T1,P1>& cs)
  {
    os.width(cs.width);
    os << cs.header;
  }

  template<typename T1,is_primitive_type P1>
  static void do_first_column(std::ostream& os, const column_as_string<T1,P1>& cs)
  {
    do_column(os,cs);
  }

  template<typename T1,is_primitive_type P1>
  static void do_next_columns(std::ostream& os, const column_as_string<T1,P1>& cs)
  {
    os << " ";
    do_column(os,cs);
  }
};


// ==================================== line_display  =================================

// Displays a line filled with dashes
template<tuple_ext::is_tuple Ts,tuple_ext::is_tuple Ps> struct line_display;

template<typename... Ts,is_primitive_type... Ps>
struct line_display<std::tuple<Ts...>,std::tuple<Ps...>>
{
  static void run(std::ostream& os, const std::tuple<column_as_string<Ts,Ps>...>& css)
  {
    const std::size_t total_w = ( std::get<column_as_string<Ts,Ps>>(css).width + ... ) + sizeof...(Ts) - 1;
    os << std::setw(total_w) << std::setfill('-') << '-' << std::setfill(' ');
  }
};

// ==================================== row_display =================================

template<tuple_ext::is_tuple Ts,tuple_ext::is_tuple Ps> struct row_display;

template<typename T,typename... Ts,is_primitive_type P, is_primitive_type... Ps>
struct row_display<std::tuple<T,Ts...>,std::tuple<P,Ps...>>
{
  static void run(
    std::ostream& os, 
    const std::size_t irow, 
    const std::tuple<column_as_string<T,P>,column_as_string<Ts,Ps>...>& css
  )
  {
    do_first_column(os,irow, std::get<column_as_string<T,P>>(css));
    (do_next_columns(os, irow, std::get<column_as_string<Ts,Ps>>(css)),...);
  }


private:
  template<typename T1,is_primitive_type P1>
  static void do_column(std::ostream& os, const std::size_t irow, const column_as_string<T1,P1>& cs)
  {
    os.width(cs.width);
    os << cs.sbuff.at(irow);
  }

  template<typename T1,is_primitive_type P1>
  static void do_first_column(std::ostream& os, const std::size_t irow, const column_as_string<T1,P1>& cs)
  {
    do_column(os,irow, cs);
  }

  template<typename T1,is_primitive_type P1>
  static void do_next_columns(std::ostream& os, const std::size_t irow, const column_as_string<T1,P1>& cs)
  {
    os << " ";
    do_column(os,irow, cs);
  }
};

} // namespace io
} // namespace impl
} // namespace tables
