// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <sstream>
#include <chrono>
#include <string.h>
#include "cpptables/types.hh"
#include "cpptables/impl/string_buffer.hh"
#include "cpptables/column.hh"

namespace tables
{
namespace impl
{
namespace io
{

template<is_primitive_type U>
struct column_traits_io
{
  // Converts column to string-buffer
  static string_buffer to_ostream(const column<U>& col)
  {
    using prim_t = U;
    const std::vector<prim_t>& v = col.v();

    const std::size_t n = col.size();

    string_buffer sbuff;
    sbuff.pos.resize(n);

    // Column-elements to string
    std::stringstream ss;
    std::size_t p = 0;
    for ( std::size_t i = 0 ; i < n ; ++i )
    {
      const std::size_t prev_p = p;

      const prim_t d = v[i];
      if ( prim_traits<prim_t>::is_none(d) )
      {
        ss << "none";
      }
      else 
      {
        if constexpr ( is_time_point<prim_t> )
        {
          const std::time_t ti = std::chrono::system_clock::to_time_t(d);
          ss << std::put_time( std::gmtime(&ti), "%FT%T");
  
          const auto duration = d.time_since_epoch();
          const auto nanoseconds = (std::chrono::duration_cast<std::chrono::nanoseconds>(duration) % std::chrono::nanoseconds(1000000000)).count();
          if ( nanoseconds != 0 )
          { 
             ss << " " << nanoseconds;
          }
        }
        else
        {
          ss << d;
        }
      }
      sbuff.pos[i] = prev_p;
      p = ss.tellp();
    }
    sbuff.buffer = ss.str();
    return sbuff;
  }
};

} // namespace io
} // namespace impl
} // namespace tables
