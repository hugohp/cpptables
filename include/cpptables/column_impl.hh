// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace tables
{

template<is_primitive_type P>
class column_impl_base
{
public:
  
  using prim_t = P;
  using vector_t = std::vector<prim_t>;
  using column_impl_t = column_impl<prim_t>;
  constexpr static auto none = prim_traits<prim_t>::none;

public:

  // ==================================== constructor / assignment  =================================

  explicit column_impl_base();

  column_impl_base(std::initializer_list<prim_t> l);

  column_impl_base(const vector_t& v);

  column_impl_base(vector_t&& v);

  column_impl_base(const column_impl_base<prim_t>& other) = default;

  column_impl_base& operator=(const column_impl_base<prim_t>& other) = default;

  column_impl_base(column_impl_base<prim_t>&& other) = default;

  column_impl_base& operator=(column_impl_base<prim_t>&& other) = default;

  // ==================================== size / count  =================================

  std::size_t size() const;

  std::size_t count() const;

  // ==================================== v / _v =================================

  const vector_t& v() const;

  vector_t& _v();

  // ==================================== n_nones =================================

  std::size_t n_nones() const;

  // ==================================== at =================================

  std::shared_ptr<column_impl_t> at(const vixs_t& ixs) const;

  // ==================================== except =================================

  std::shared_ptr<column_impl_t> except(const column_impl_t& xs) const;
 
  // ==================================== append (scalar) =================================

  std::shared_ptr<column_impl_t> append(const prim_t& val) const;

  // ==================================== _append (scalar) =================================

  void _append(const prim_t val);

  // ==================================== _append (scalar) =================================

  void _append(const std::size_t count, const prim_t val);

  // ==================================== _append (column) =================================

  void _append(const column_impl<prim_t>& other);

  // ==================================== append (column) =================================

  std::shared_ptr<column_impl_t> append(const column_impl<prim_t>& other) const;

  // ==================================== take/drop =================================

  std::shared_ptr<column_impl_t> take(const std::size_t n) const;

  std::shared_ptr<column_impl_t> drop(const std::size_t n) const;

  // ==================================== rtake/rdrop =================================

  std::shared_ptr<column_impl_t> rtake(const std::size_t n) const;

  std::shared_ptr<column_impl_t> rdrop(const std::size_t n) const;

  // ==================================== apply =================================

  template<typename F>
  auto apply(F f) const;

  // ==================================== cast_to =================================

  template<is_primitive_type P1>
  auto cast_to() const;

  // ==================================== reverse =================================

  std::shared_ptr<column_impl_t> reverse() const;

  // ==================================== fills =================================

  std::shared_ptr<column_impl_t> fills() const;

  // ==================================== fill_with (scalar) =================================

  std::shared_ptr<column_impl_t> fill_with(const prim_t val) const;

  // ==================================== fill_with (column) =================================

  std::shared_ptr<column_impl_t> fill_with(const column_impl_t c) const;
 
  // ==================================== zfill =================================

  std::shared_ptr<column_impl_t> zfill() const;

  // ==================================== replace =================================

  std::shared_ptr<column_impl_t> replace(const column_impl_t& c) const;


  // ==================================== differ =================================

  std::shared_ptr<column_impl<bool>> differ() const;

  // ==================================== is_none =================================

  std::shared_ptr<column_impl<bool>> is_none() const;

  // ==================================== distinct =================================

  std::shared_ptr<column_impl_t> distinct() const;

  // ==================================== sort =================================

  std::shared_ptr<column_impl_t> sort() const;

  // ==================================== rsort =================================
 
  std::shared_ptr<column_impl_t> rsort() const;
 
  // ==================================== mins/maxs =================================

  std::shared_ptr<column_impl_t> mins() const;
 
  std::shared_ptr<column_impl_t> maxs() const;
 
  // ==================================== mmin/mmax =================================

  std::shared_ptr<column_impl_t> mmin(const std::size_t m) const;
 
  std::shared_ptr<column_impl_t> mmax(const std::size_t m) const;

  // ==================================== next/prev =================================

  std::shared_ptr<column_impl_t> next() const;
 
  std::shared_ptr<column_impl_t> prev() const;

  // ==================================== max/min =================================

  prim_t max() const;

  prim_t min() const;


  // ==================================== all =================================

  bool all() const;

  // ==================================== any =================================

  bool any() const;

  // ==================================== cross =================================

  template<is_primitive_type P1>
  std::pair<shared_ptr<column_impl<prim_t>>,shared_ptr<column_impl<P1>>> 
  cross(const column_impl<P1>& other) const;

  // ==================================== eq =================================

  inline
  std::shared_ptr<column_impl<bool>>
  eq(const column_impl_t& rhs) const;
  
  inline
  std::shared_ptr<column_impl<bool>>
  eq(const prim_t cte) const;

  // ==================================== ne =================================
  
  std::shared_ptr<column_impl<bool>>
  ne(const column_impl_t& rhs) const;

  inline
  std::shared_ptr<column_impl<bool>>
  ne(const prim_t cte) const;

  // ==================================== lt =================================
  
  inline
  std::shared_ptr<column_impl<bool>>
  lt(const column_impl_t& rhs) const;

  inline
  std::shared_ptr<column_impl<bool>>
  lt(const prim_t cte) const;

  // ==================================== le =================================
  
  inline
  std::shared_ptr<column_impl<bool>>
  le(const column_impl_t& rhs) const;

  inline
  std::shared_ptr<column_impl<bool>>
  le(const prim_t cte) const;

  // ==================================== gt =================================
  
  inline
  std::shared_ptr<column_impl<bool>>
  gt(const column_impl_t& rhs) const;

  inline
  std::shared_ptr<column_impl<bool>>
  gt(const prim_t cte) const;

  // ==================================== ge =================================
  
  inline
  std::shared_ptr<column_impl<bool>>
  ge(const column_impl_t& rhs) const;

  inline
  std::shared_ptr<column_impl<bool>>
  ge(const prim_t cte) const;

  // ==================================== in =================================
  
  inline
  std::shared_ptr<column_impl<bool>>
  in(const column_impl_t& rhs) const;

protected:

  prim_t _zat(const std::size_t i) const;

  vector_t v_;
};

// ==================================== column_impl<is_arithmetic_type>  =================================

template<is_arithmetic_type P>
class column_impl<P> : public column_impl_base<P>
{
public:
  using prim_t = P;

  using column_impl_base_t = column_impl_base<prim_t>;
  using vector_t = typename column_impl_base_t::vector_t;
  using column_impl_t = column_impl<prim_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_impl() : 
   column_impl_base<prim_t>()
  {}

  column_impl(std::initializer_list<prim_t> l) : 
   column_impl_base<prim_t>(l)
  {}

  column_impl(const vector_t& v) : 
   column_impl_base<prim_t>(v)
  {}

  column_impl(vector_t&& v) :
    column_impl_base<prim_t>(std::forward<vector_t>(v))
  {}

  column_impl(const column_impl<prim_t>& other) : 
    column_impl_base<prim_t>(other)
  {}

  column_impl(column_impl<prim_t>&& other) :
    column_impl_base<prim_t>( std::forward<column_impl_base_t>(other) )
  {}

  inline column_impl& operator=(const column_impl<prim_t>& other) 
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=(other));
  }

  inline column_impl& operator=(column_impl<prim_t>&& other)
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=( std::forward<column_impl_base_t>(other) ));
  }

  // ==================================== deltas =================================

  std::shared_ptr<column_impl_t> deltas() const;

  // ==================================== xbar =================================

  std::shared_ptr<column_impl_t> xbar(const prim_t val) const;

  // ==================================== sums =================================
 
  std::shared_ptr<column_impl_t> sums() const;
 
  // ==================================== msum =================================
 
  std::shared_ptr<column_impl_t> msum(const std::size_t m) const;

  // ==================================== abs =================================

  std::shared_ptr<column_impl_t> abs() const;

  // ==================================== log / log2 / log10 =================================
 
  std::shared_ptr<column_impl<double>> log() const;

  std::shared_ptr<column_impl<double>> log2() const;

  std::shared_ptr<column_impl<double>> log10() const;

  // ==================================== exp =================================
 
  std::shared_ptr<column_impl<double>> exp() const;

  // ==================================== mod =================================
 
  std::shared_ptr<column_impl<double>> mod(const prim_t y) const;

  // ==================================== sum =================================

  prim_t sum() const;

  // ==================================== avg =================================

  prim_t avg() const;

  // ==================================== dot =================================

  template<is_arithmetic_type P1>
  auto dot(const column_impl<P1>& rhs) const;

  // ==================================== norm =================================

  double norm() const;
};

// ==================================== column_impl<std::string_view>  =================================

template<>
class column_impl<std::string_view> : public column_impl_base<std::string_view>
{
public:
  using prim_t = std::string_view;

  using column_impl_base_t = column_impl_base<prim_t>;
  using vector_t = typename column_impl_base_t::vector_t;
  using column_impl_t = column_impl<prim_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_impl() : 
   column_impl_base<prim_t>() 
  {}

  column_impl(std::initializer_list<prim_t> l) : 
    column_impl_base<prim_t>(l) 
  {}

  column_impl(const vector_t& v) : 
    column_impl_base<prim_t>(v) 
  {}

  column_impl(vector_t&& v) :
    column_impl_base<prim_t>(std::forward<vector_t>(v))
  {}

  column_impl(const column_impl<prim_t>& other) : 
    column_impl_base<prim_t>(other)
  {}

  column_impl(column_impl<prim_t>&& other) :
    column_impl_base<prim_t>( std::forward<column_impl_base_t>(other) )
  {}

  inline column_impl& operator=(const column_impl<prim_t>& other) 
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=(other));
  }

  inline column_impl& operator=(column_impl<prim_t>&& other)
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=( std::forward<column_impl_base_t>(other) ));
  }

  std::shared_ptr<column_impl<bool>> starts_with(const std::string_view sv) const;

  std::shared_ptr<column_impl<bool>> starts_with(const column_impl<std::string_view>& svs) const;

  std::shared_ptr<column_impl<bool>> ends_with(const std::string_view sv) const;

  std::shared_ptr<column_impl<bool>> ends_with(const column_impl<std::string_view>& svs) const;

  std::shared_ptr<column_impl_t> substr(const std::size_t pos = 0, const std::size_t count = std::string::npos) const;

  std::shared_ptr<column_impl_t> substr(const column_impl<std::size_t>& pos, const column_impl<std::size_t>& count) const;

  std::shared_ptr<column_impl_t> concat(const std::string_view sv) const;

  std::shared_ptr<column_impl_t> concat(const column_impl<std::string_view>& svs) const;

  std::string to_string(const std::string_view delim = "") const;
};

// ==================================== column_impl<bool>  =================================

template<>
class column_impl<bool> : public column_impl_base<bool>
{
public:
  using prim_t = bool;

  using column_impl_base_t = column_impl_base<prim_t>;
  using vector_t = typename column_impl_base_t::vector_t;
  using column_impl_t = column_impl<prim_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_impl() : 
   column_impl_base<prim_t>() 
  {}

  column_impl(std::initializer_list<prim_t> l) : 
    column_impl_base<prim_t>(l) 
  {}

  column_impl(const vector_t& v) : 
    column_impl_base<prim_t>(v) 
  {}

  column_impl(vector_t&& v) :
    column_impl_base<prim_t>(std::forward<vector_t>(v))
  {}

  column_impl(const column_impl<prim_t>& other) : 
    column_impl_base<prim_t>(other)
  {}

  column_impl(column_impl<prim_t>&& other) :
    column_impl_base<prim_t>( std::forward<column_impl_base_t>(other) )
  {}

  inline column_impl& operator=(const column_impl<prim_t>& other) 
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=(other));
  }

  inline column_impl& operator=(column_impl<prim_t>&& other)
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=( std::forward<column_impl_base_t>(other) ));
  }

  // ==================================== neg =================================

  std::shared_ptr<column_impl_t> neg() const;

  // ==================================== where =================================

  // ==================================== where =================================

  vixs_t where() const;
};

// ==================================== column_impl<is_time_point>  =================================

template<is_time_point P>
class column_impl<P> : public column_impl_base<P>
{
public:
  using prim_t = P;

  using column_impl_base_t = column_impl_base<prim_t>;
  using vector_t = typename column_impl_base_t::vector_t;
  using column_impl_t = column_impl<prim_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_impl() : 
   column_impl_base<prim_t>() 
  {}

  column_impl(std::initializer_list<prim_t> l) : 
    column_impl_base<prim_t>(l) 
  {}

  column_impl(const vector_t& v) : 
    column_impl_base<prim_t>(v) 
  {}

  column_impl(vector_t&& v) :
    column_impl_base<prim_t>(std::forward<vector_t>(v))
  {}

  column_impl(const column_impl<prim_t>& other) : 
    column_impl_base<prim_t>(other)
  {}

  column_impl(column_impl<prim_t>&& other) :
    column_impl_base<prim_t>( std::forward<column_impl_base_t>(other) )
  {}

  inline column_impl& operator=(const column_impl<prim_t>& other) 
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=(other));
  }

  inline column_impl& operator=(column_impl<prim_t>&& other)
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=( std::forward<column_impl_base_t>(other) ));
  }

  // ==================================== time_since_epoch  =================================

  std::shared_ptr<column_impl<typename P::duration>>
  time_since_epoch() const;

  // ==================================== time_point_cast  =================================

  template <
    is_duration ToDuration, 
    typename Clock = typename P::clock,
    typename Duration = typename P::duration
  >
  std::shared_ptr<column_impl<std::chrono::time_point<Clock,ToDuration>>>
  time_point_cast() const;

  // ==================================== to_time_t  =================================

  std::shared_ptr<column_impl<std::time_t>>
  to_time_t() const;

  // ==================================== operator+(duration)  =================================

  template<is_duration D>
  constexpr std::shared_ptr<
    column_impl<
      std::chrono::time_point<
        typename P::clock, 
        std::common_type_t<D,typename P::duration>
      >
    >
  >
  operator+(const column<D>& ds) const;

  // ==================================== operator-(duration)  =================================

  template<is_duration D>
  constexpr std::shared_ptr<
    column_impl<
      std::chrono::time_point<
        typename P::clock, 
        std::common_type_t<D,typename P::duration>
      >
    >
  >
  operator-(const column<D>& ds) const;
};

// ==================================== column_impl<is_duration>  =================================

template<is_duration P>
class column_impl<P> : public column_impl_base<P>
{
public:
  using prim_t = P;

  using column_impl_base_t = column_impl_base<prim_t>;
  using vector_t = typename column_impl_base_t::vector_t;
  using column_impl_t = column_impl<prim_t>;

  // ==================================== constructor / assignment  =================================

  explicit column_impl() : 
   column_impl_base<prim_t>() 
  {}

  column_impl(std::initializer_list<prim_t> l) : 
    column_impl_base<prim_t>(l) 
  {}

  column_impl(const vector_t& v) : 
    column_impl_base<prim_t>(v) 
  {}

  column_impl(vector_t&& v) :
    column_impl_base<prim_t>(std::forward<vector_t>(v))
  {}

  column_impl(const column_impl<prim_t>& other) : 
    column_impl_base<prim_t>(other)
  {}

  column_impl(column_impl<prim_t>&& other) :
    column_impl_base<prim_t>( std::forward<column_impl_base_t>(other) )
  {}

  inline column_impl& operator=(const column_impl<prim_t>& other) 
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=(other));
  }

  inline column_impl& operator=(column_impl<prim_t>&& other)
  {
    return static_cast<column_impl_t&>(column_impl_base<prim_t>::operator=( std::forward<column_impl_base_t>(other) ));
  }

  // ==================================== duration_cast  =================================

  template <is_duration ToDuration>
  std::shared_ptr<column_impl<ToDuration>>
  duration_cast() const;

  // ==================================== floor  =================================

  template <is_duration ToDuration>
  std::shared_ptr<column_impl<ToDuration>>
  floor() const;

  // ==================================== ceil  =================================

  template <is_duration ToDuration>
  std::shared_ptr<column_impl<ToDuration>>
  ceil() const;

  // ==================================== round  =================================

  template <is_duration ToDuration>
  std::shared_ptr<column_impl<ToDuration>>
  round() const;

  // ==================================== abs  =================================

  std::shared_ptr<column_impl<P>>
  abs() const;
};

} // namespace tables
