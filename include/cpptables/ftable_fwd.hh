// This file is part of cpptables.
//
// cpptables is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// cpptables is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with cpptables.  If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include "table_fwd.hh"
#include "concepts.hh"

namespace tables
{

template <is_table,is_tuple_unique Gs>
requires ( std::tuple_size_v<Gs> > 0 )
struct ftable;

// ==================================== is_ftable =================================

namespace detail
{

template<typename T>
struct is_ftable_s : std::false_type {};

template <is_table T,tuple_ext::is_tuple G>
requires (
  all_in<G,typename T::ts_t> && 
  (std::tuple_size_v<G> > 0)
)
struct is_ftable_s<ftable<T,G>> : std::true_type {};

} // namespace detail

template<typename T>
concept is_ftable = detail::is_ftable_s<T>::value;

} // namespace typename

